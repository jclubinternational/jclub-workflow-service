﻿$repository = "jclubworkflowservice"
$projectfolder = "JClub.WorkflowService.Api"


Write-Host "Huidige versies:"
az acr repository show-tags -n jclub --repository $repository --output tsv
$version = Read-Host -Prompt "Geef versienummer"

docker build -f "..\src\${projectfolder}\Dockerfile" "..\Src" -t jclub.azurecr.io/${repository}:$version -t jclub.azurecr.io/${repository}:latest -t ${repository}:$version -t ${repository}:latest



$title    = 'Push remote'
$question = 'Wil je direct pushen naar jclub.azurecr.io'
$choices  = '&Ja', '&Nee'

$decision = $Host.UI.PromptForChoice($title, $question, $choices, 1)
if ($decision -eq 0) {
    docker push jclub.azurecr.io/${repository}:$version
    docker push jclub.azurecr.io/${repository}:latest
}


Write-Host "Press enter to continue"
Read-Host
