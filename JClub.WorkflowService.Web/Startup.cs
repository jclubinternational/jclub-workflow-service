using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;
using JClub.WorkflowService.Connector;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using TanvirArjel.Blazor.DependencyInjection;

namespace JClub.WorkflowService.Web {
	public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {

			var corsOrigins = Configuration.GetSection("CorsOrigins").Get<string[]>();
			services.AddCors(options => options
				.AddDefaultPolicy(p => p
									   .AllowAnyHeader()
									   .AllowAnyMethod()
									   .AllowCredentials()
									   .WithOrigins(corsOrigins)
				)
			);

			services.AddHttpClient<IWorkflowServiceConnector, WorkflowServiceConnector>(c => {
				c.BaseAddress = new Uri("https://localhost:44339");
			});
            services.AddRazorPages();
            services.AddServerSideBlazor();

			services
				.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
				.AddJwtBearer(options => {
					var signingKey = Convert.FromBase64String(Configuration["Jwt:SigningSecret"]);
					options.TokenValidationParameters = new TokenValidationParameters {
						ValidateIssuer           = true,
						ValidateAudience         = true,
						ValidateIssuerSigningKey = true,
						IssuerSigningKey         = new SymmetricSecurityKey(signingKey),
						ValidIssuers             = new[] { Configuration["Jwt:Issuer"] },
						ValidAudiences           = new[] { Configuration["Jwt:Audience"] },
					};
					options.TokenValidationParameters.NameClaimType = "UserLogin";
					options.TokenValidationParameters.RoleClaimType = "UserRole";
					options.Events = new JwtBearerEvents {
						OnMessageReceived = context => {

							var accessToken = context.HttpContext.Request.Cookies["access_token"];

							if (!string.IsNullOrEmpty(accessToken)) {
								context.Token = accessToken;
							}
							return Task.CompletedTask;
						}
					};
				});

			services.AddComponents();
		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

			app.UseCors();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
