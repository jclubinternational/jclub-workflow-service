using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Connector;
using Microsoft.AspNetCore.Components;

namespace JClub.WorkflowService.Web.Pages {
	[Route(Route)]
	//[Authorize]
	public partial class Workflowconfiguraties : 
		ComponentBase {

		public const string Route = "/";

		private readonly IWorkflowServiceConnector _connector;

		public Workflowconfiguraties(
			IWorkflowServiceConnector Connector) {
			
			_connector = Connector;
		}

		private IReadOnlyCollection<WorkflowconfiguratieViewModel>? _workflowconfiguraties;

		protected override async Task OnInitializedAsync() {
			_workflowconfiguraties = await _connector.GetWorkflowconfiguraties();
		}
	}
}