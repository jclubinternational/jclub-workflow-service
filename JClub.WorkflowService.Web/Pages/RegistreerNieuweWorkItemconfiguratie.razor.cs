using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Connector;
using JClub.WorkflowService.Messages.Commands;
using Microsoft.AspNetCore.Components;

namespace JClub.WorkflowService.Web.Pages {

	//[Route(BaseRoute + Route)]
	//[Route(BaseRoute + RouteWithPrevious + Route)]
	public partial class RegistreerNieuweWorkItemconfiguratie :
		ComponentBase {

		//private const string BaseRoute = "/workflowconfiguratie/{workflowconfiguratieId:guid}";
		//public const string Route = "/" + nameof(RegistreerNieuweWorkItemconfiguratie);
		//public const string RouteWithPrevious = "/workitemconfiguratie/{voorgaandeWorkItemconfiguratieId:guid}";

		private readonly IWorkflowServiceConnector _connector;
		private readonly NavigationManager _nav;

		[Parameter]
		public Guid WorkflowconfiguratieId { get; set; }
		[Parameter]
		public Guid? VoorgaandeWorkItemconfiguratieId { get; set; }

		public RegistreerNieuweWorkItemconfiguratie(
			IWorkflowServiceConnector connector,
			NavigationManager nav) {
			_connector = connector;
			_nav  = nav;
		}

		private IReadOnlyCollection<GebeurtenisdefinitieViewModel> _gebeurtenissen { get; set; } = null!;
		private IReadOnlyCollection<OpdrachtdefinitieViewModel> _opdrachten { get; set; } = null!;

		private readonly RegistreerNieuweWorkItemconfiguratieCommand _command = new ();

		public static string MaakRegistreerEerstePad(Guid workflowconfiguratieId) {

			return $"/workflowconfiguratie/{workflowconfiguratieId}/registreerNieuweWorkItemconfiguration";
		}

		public static string MaakRegistreerVolgendePad(Guid workflowconfiguratieId,Guid voorgaandeWorkItemconfiguratieId) {
			return $"/workflowconfiguratie/{workflowconfiguratieId}/workitemconfiguratie/{voorgaandeWorkItemconfiguratieId}/registreerNieuweWorkItemconfiguration";
		}

		protected override async Task OnInitializedAsync() {

			_gebeurtenissen = await _connector.GetGebeurtenisdefinities();
			_opdrachten     = await _connector.GetOpdrachtdefinities();

			_command.WorkflowconfiguratieId = WorkflowconfiguratieId;

			if (VoorgaandeWorkItemconfiguratieId == null) {
				return;
			}

			_command.VorigeWorkItemconfiguratieId = VoorgaandeWorkItemconfiguratieId;

			var item = await _connector.GetWorkItemconfiguratieById(WorkflowconfiguratieId, VoorgaandeWorkItemconfiguratieId.Value);

			var opdracht = _opdrachten.Single(_ => _.Id == item.VoerOpdrachtUit);

			_command.ReageerOpGebeurtenis = opdracht.ResulterendeGebeurtenis;
		}

		public async Task OnSaveClicked() {
			await _connector.RegistreerNieuweWorkItemconfiguratie(_command);

			_nav.NavigateTo($"/workflowconfiguratie/{WorkflowconfiguratieId}");
		}
	}
}