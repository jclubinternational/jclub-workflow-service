using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Connector;
using Microsoft.AspNetCore.Components;
using static JClub.WorkflowService.Web.Pages.RegistreerNieuweWorkItemconfiguratie;

namespace JClub.WorkflowService.Web.Pages {

	[Route(Route)]
	public partial class WorkItemconfiguraties :
		ComponentBase {

		public const string Route = "/workflowconfiguratie/{workflowconfiguratieId:guid}/workitemconfiguratie";

		[Parameter]
		public Guid workflowconfiguratieId { get; set; }

		public WorkItemconfiguraties(
			IWorkflowServiceConnector connector,
			NavigationManager nav
			) {

			_connector              = connector;
			_nav = nav;
		}

		private readonly IWorkflowServiceConnector _connector;
		private readonly NavigationManager _nav;
		private IReadOnlyCollection<WorkItemconfiguratieViewModel>? _workItemconfiguraties { get; set; }
		private IDictionary<Guid, GebeurtenisdefinitieViewModel> _gebeurtenisdefinities { get; set; } = null!;
		private IDictionary<Guid, OpdrachtdefinitieViewModel> _opdrachtdefinities { get; set; } = null!;
		private IList<WorkItemconfiguratieViewModel>? _selectedItems { get; set; }

		protected override async Task OnInitializedAsync() {
			var gebeurtenisdefinities = await _connector.GetGebeurtenisdefinities();
			_gebeurtenisdefinities = gebeurtenisdefinities.ToDictionary(_ => _.Id);

			var opdrachtdefinities = await _connector.GetOpdrachtdefinities();
			_opdrachtdefinities = opdrachtdefinities.ToDictionary(_ => _.Id);

			var items = (await _connector.GetWorkItemconfiguraties(workflowconfiguratieId));
			//var workItemconfiguraties = new List<WorkItemconfiguratieViewModel>();

			//foreach (var item in items) {
			//	var multiple = Enumerable.Repeat(item, 1).Select(_ => new WorkItemconfiguratieViewModel() {
			//		Id = _.Id,
			//		Beschrijving = _.Beschrijving,
			//		ReageerOpGebeurtenis = _.ReageerOpGebeurtenis,
			//		Status = _.Status,
			//		Titel = _.Titel,
			//		VoerOpdrachtUit = _.VoerOpdrachtUit,
			//		VorigeWorkItemconfiguratieId = _.VorigeWorkItemconfiguratieId
			//	}).ToList();

			//	workItemconfiguraties.AddRange(multiple);
			//}

			_workItemconfiguraties = items;
		}

		private void NavigateTo(Actions action, WorkItemconfiguratieViewModel workItemconfiguratie) {

			if (action == Actions.Edit) {
				_nav.NavigateTo($"workflowconfiguratie/{workflowconfiguratieId}/workitemconfiguratie/{workItemconfiguratie.Id}");
				return;
			}

			if (action == Actions.InsertAfter) {
				_nav.NavigateTo(MaakRegistreerVolgendePad(workflowconfiguratieId, workItemconfiguratie.Id));
				return;
			}

			if (workItemconfiguratie.VorigeWorkItemconfiguratieId == null) {
				_nav.NavigateTo(MaakRegistreerEerstePad(workflowconfiguratieId));
				return;
			}

			_nav.NavigateTo(MaakRegistreerVolgendePad(workflowconfiguratieId, workItemconfiguratie.VorigeWorkItemconfiguratieId.Value));
		}

		private enum Actions {
			Edit,
			InsertBefore,
			InsertAfter
		}
	}
}