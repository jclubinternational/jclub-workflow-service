using System;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Connector;
using Microsoft.AspNetCore.Components;

namespace JClub.WorkflowService.Web.Pages {
	[Route(Route)]
	public partial class Workflowconfiguratie : ComponentBase {
		private readonly IWorkflowServiceConnector _connector;

		public const string Route = "/workflowconfiguratie/{WorkflowconfiguratieId:guid}";

		[Parameter]
		public Guid WorkflowconfiguratieId { get; set; }

		public Workflowconfiguratie(IWorkflowServiceConnector connector) {
			_connector = connector;
		}

		private WorkflowconfiguratieViewModel? _workflowconfiguratie { get; set; }

		protected override async Task OnInitializedAsync() {
			_workflowconfiguratie = await _connector.GetWorkflowconfiguratieById(WorkflowconfiguratieId);
		}
	}
}
