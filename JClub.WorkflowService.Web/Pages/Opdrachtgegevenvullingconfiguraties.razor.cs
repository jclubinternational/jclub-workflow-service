using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Connector;
using Microsoft.AspNetCore.Components;

namespace JClub.WorkflowService.Web.Pages {
	[Route(Route)]
	public partial class Opdrachtgegevenvullingconfiguraties : ComponentBase {

		public const string Route = "/workflowconfiguratie/{workflowconfiguratieId:guid}/workitemconfiguratie/{workItemconfiguratieId:guid}/opdrachtgegevenvullingconfiguratie";

		private readonly IWorkflowServiceConnector _connector;

		public Opdrachtgegevenvullingconfiguraties(
			IWorkflowServiceConnector connector) {
			_connector = connector;
		}

		[Parameter]
		public Guid workflowconfiguratieId { get; set; }
		[Parameter]
		public Guid workItemconfiguratieId { get; set; }

		private IReadOnlyCollection<OpdrachtgegevenvullingconfiguratieViewModel>? _opdrachtgegevenvullingconfiguraties { get; set; }
		private IDictionary<Guid, GebeurtenisgegevendefinitieViewModel>? _gebeurtenisdefinitieById { get; set; }

		protected override async Task OnInitializedAsync() {

			_gebeurtenisdefinitieById = (await _connector.GetGebeurtenisgegevendefinities(workItemconfiguratieId))
				.ToDictionary(_ => _.Id);

			_opdrachtgegevenvullingconfiguraties = await _connector.GetOpdrachtgegevenvulingconfiguraties(workItemconfiguratieId);
		}
	}
}