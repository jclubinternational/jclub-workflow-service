using System;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Connector;
using Microsoft.AspNetCore.Components;

namespace JClub.WorkflowService.Web.Pages {
	[Route(Route)]
	public partial class WorkItemconfiguratie : ComponentBase{

		public const string Route = "/workflowconfiguratie/{workflowconfiguratieId:guid}/workitemconfiguratie/{workItemconfiguratieId:guid}";

		private readonly IWorkflowServiceConnector _connector;

		public WorkItemconfiguratie(IWorkflowServiceConnector connector) {
			_connector = connector;
		}

		[Parameter]
		public Guid WorkflowconfiguratieId { get; set; }
		[Parameter]
		public Guid WorkItemconfiguratieId { get; set; }

		private WorkItemconfiguratieViewModel? _workItemconfiguratie { get; set; }

		private GebeurtenisdefinitieViewModel? _gebeurtenisdefinitie { get; set; }
		private OpdrachtdefinitieViewModel? _opdrachtdefinitie { get; set; }

		protected override async Task OnInitializedAsync() {
			var workItemconfiguratie = await _connector.GetWorkItemconfiguratieById(WorkflowconfiguratieId, WorkItemconfiguratieId);

			_gebeurtenisdefinitie = await _connector.GetGebeurtenisdefinitieById(workItemconfiguratie.ReageerOpGebeurtenis);
			_opdrachtdefinitie    = await _connector.GetOpdrachtdefinitieById(workItemconfiguratie.VoerOpdrachtUit);
			_workItemconfiguratie = workItemconfiguratie;
		}
	}
}