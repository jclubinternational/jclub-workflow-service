using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace JClub.WorkflowService.Framework.Api {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddWorkflowCommands(this IServiceCollection services, params Assembly[] assembliesToScan) {
			return services.AddSingleton<IGetWorkflowCommands>(new WorkflowApiService(assembliesToScan));
		}
	}
}