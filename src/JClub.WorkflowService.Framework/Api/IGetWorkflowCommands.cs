using System.Collections.Generic;

namespace JClub.WorkflowService.Framework.Api {
	public interface IGetWorkflowCommands {
		IReadOnlyList<IWorkFlowCommand> All();
	}
}