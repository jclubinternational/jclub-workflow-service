using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace JClub.WorkflowService.Framework.Api {

	[ApiController]
	public class WorkflowCommandController : ControllerBase {

		[HttpGet("workflowcommand")]
		public IWorkFlowCommand[] Get([FromServices] IGetWorkflowCommands getWorkflowCommands) {

			return getWorkflowCommands.All()
				.ToArray();
		}
	}
}