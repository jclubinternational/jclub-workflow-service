using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using JClub.WorkflowService.Framework.Messages;

namespace JClub.WorkflowService.Framework.Api {

	internal class WorkflowApiService : IGetWorkflowCommands {
		private readonly Assembly[] _assemblies;
		private IReadOnlyList<IWorkFlowCommand>? _workFlowCommands;

		internal WorkflowApiService(Assembly[] assemblies) {
			_assemblies = assemblies;
		}

		public IReadOnlyList<IWorkFlowCommand> All() {
			return _workFlowCommands ??= _assemblies.SelectMany(_ => _.GetTypes().Where(t => t.IsAssignableFrom(typeof(WorkflowCommandHandler<,>)))
					.Select(t => new WorkFlowCommand  {
						CommandName = t.GenericTypeArguments[0].Name,
						CommandInput = t.GenericTypeArguments[0].GetProperties(BindingFlags.Public).Select(p => new WorkflowValue {
							Name = p.Name,
							Type = $"{p.GetMethod.ReturnType}" 
						}).ToArray(),
						EventName = t.GenericTypeArguments[1].Name,
						EventOutput = t.GenericTypeArguments[1].GetProperties(BindingFlags.Public).Select(p => new WorkflowValue {
							Name = p.Name,
							Type = $"{p.GetMethod.ReturnType}"
						}).ToArray()
					}))
				.Cast<IWorkFlowCommand>()
				.ToList();
		}

		private class WorkFlowCommand : IWorkFlowCommand {
			public string CommandName { get; set; } = string.Empty;
			public IWorkflowValue[] CommandInput { get; set; } = Array.Empty<IWorkflowValue>();
			public string EventName { get; set; } = string.Empty;
			public IWorkflowValue[] EventOutput { get; set; } = Array.Empty<IWorkflowValue>();
		}

		private class WorkflowValue : IWorkflowValue {
			public string Name { get; set; } = string.Empty;
			public string Type { get; set; } = string.Empty;
		}
	}

	public interface IWorkFlowCommand {
		string CommandName { get; }
		IWorkflowValue[] CommandInput { get; }
		string EventName { get; }
		IWorkflowValue[] EventOutput { get; }
	}

	public interface IWorkflowValue {
		string Name { get; }
		string Type { get; }
	}
}