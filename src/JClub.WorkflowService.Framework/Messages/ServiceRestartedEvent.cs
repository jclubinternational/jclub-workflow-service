using NServiceBus;

namespace JClub.WorkflowService.Framework.Messages {
	public class ServiceRestartedEvent : IEvent {
		public string WorkflowCommandControllerAddress { get; set; } = string.Empty;
	}
}