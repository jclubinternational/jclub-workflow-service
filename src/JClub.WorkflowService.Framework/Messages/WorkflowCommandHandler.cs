using System.Threading.Tasks;
using NServiceBus;

namespace JClub.WorkflowService.Framework.Messages
{
    public abstract class WorkflowCommandHandler<TCommand, TEvent> : IHandleMessages<TCommand> 
		where TEvent : IWorkflowEvent {

		protected abstract Task<TEvent> HandleWorkItem(TCommand message, IMessageHandlerContext context);

		public async Task Handle(TCommand message, IMessageHandlerContext context) {
			var @event = await HandleWorkItem(message, context);
			await context.Publish(@event);
		}
	}
}
