using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts;
using JClub.WorkflowService.Business.Contracts.Commands;
using JClub.WorkflowService.Business.Domain.Aggregates;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowAggregate;
using JClub.WorkflowService.Business.Domain.ValueObjects;

namespace JClub.WorkflowService.Business.Services {

	public class WorkflowService {

		private readonly IGetWorkflowconfiguratie _getWorkflowconfiguratie;
		private readonly ISaveWorkflow _saveWorkflow;
		private readonly IGetWorkflow _getWorkflow;

		public WorkflowService(
			IGetWorkflowconfiguratie getWorkflowconfiguratie,
			ISaveWorkflow saveWorkflow,
			IGetWorkflow getWorkflow) {

			_getWorkflowconfiguratie = getWorkflowconfiguratie;
			_saveWorkflow = saveWorkflow;
			_getWorkflow = getWorkflow;
		}

		public async Task StartWorkflow(IWorkflowStart workflowStart, IVerstuurOpdracht verstuurOpdracht) {
			var workflowconfiguratie = await _getWorkflowconfiguratie.ById(workflowStart.WorkflowconfiguratieId);

			var workflow = Workflow.Maak(workflowStart.CorrelationId, workflowconfiguratie);

			if (workflow == null) {
				return;
			}

			var gebuertenis = MaakGebeurtenis(workflowStart.Gebeurtenisnaam, workflowStart.Gebeurtenisinformatie);
			await workflow.PakVolgendeWorkItemOp(gebuertenis, verstuurOpdracht.Verstuur);
			await _saveWorkflow.SaveNew(workflow);
		}

		public async Task VoortzettenWorkflow(IWorkflowVoortzetting workflowVoortzetting, IVerstuurOpdracht verstuurOpdracht) {

			var workflow = await _getWorkflow.ById(workflowVoortzetting.WorkflowId);

			var gebuertenis = MaakGebeurtenis(workflowVoortzetting.Gebeurtenisnaam, workflowVoortzetting.Gebeurtenisinformatie);
			await workflow.PakVolgendeWorkItemOp(gebuertenis, verstuurOpdracht.Verstuur);
		}

		private static Gebeurtenis MaakGebeurtenis(string gebeurtenisnaam, IDictionary<string, string> gebeurtenisinformatie) {
			var gebeurtenisgegevens = gebeurtenisinformatie
				.Select(_ => new Gebeurtenisgegeven(_.Key, _.Value))
				.ToArray();

			var gebuertenis = new Gebeurtenis(gebeurtenisnaam, gebeurtenisgegevens);
			return gebuertenis;
		}
	}
}