using System;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts;
using JClub.WorkflowService.Business.Contracts.Commands;
using JClub.WorkflowService.Business.Domain.Aggregates;

namespace JClub.WorkflowService.Business.Services {
	public class WorkflowconfiguratieService {
		private readonly ISaveWorkflowconfiguratie _saveWorkflowconfiguratie;
		private readonly IGetWorkflowconfiguratie _getWorkflowconfiguratie;
		private readonly IGetOpdrachtAndGebeurtenisdefinities _getOpdrachtAndGebeurtenisdefinities;

		public WorkflowconfiguratieService(
			ISaveWorkflowconfiguratie saveWorkflowconfiguratie,
			IGetWorkflowconfiguratie getWorkflowconfiguratie,
			IGetOpdrachtAndGebeurtenisdefinities getOpdrachtAndGebeurtenisdefinities) {

			_saveWorkflowconfiguratie = saveWorkflowconfiguratie;
			_getWorkflowconfiguratie = getWorkflowconfiguratie;
			_getOpdrachtAndGebeurtenisdefinities = getOpdrachtAndGebeurtenisdefinities;
		}

		public Task VoegNieuweWorkflowCongiratieToe(INieuweWorkflowconfiguratie nieuweWorkflowconfiguratie) {
			var workflowconfiguratie = new Workflowconfiguratie(
				nieuweWorkflowconfiguratie.Titel,
				nieuweWorkflowconfiguratie.Beschrijving,
				nieuweWorkflowconfiguratie.AangemaaktDoor);

			return _saveWorkflowconfiguratie.SaveNew(workflowconfiguratie);
		}

		public async Task PasWorkflowconfiguratieAan(IAangepasteWorkflowconfiguratie aangepasteWorkflowconfiguratie) {
			var workflowconfiguratie = await _getWorkflowconfiguratie.ById(aangepasteWorkflowconfiguratie.Id);

			workflowconfiguratie.PasAan(aangepasteWorkflowconfiguratie.Titel, aangepasteWorkflowconfiguratie.Beschrijving);
		}

		public async Task VoegWorkflowItemconfiguratieToe(INieuweWorkflowItemconfiguratie nieuweWorkflowItemconfiguratie) {

			var workflowconfiguratie = await _getWorkflowconfiguratie.ById(nieuweWorkflowItemconfiguratie.WorkflowconfiguratieId);

			var (opdrachtdefinitie, gebeurtenisdefinitie) = await _getOpdrachtAndGebeurtenisdefinities.GetByIds(nieuweWorkflowItemconfiguratie.VoerOpdrachtUit, nieuweWorkflowItemconfiguratie.ReageerOpGebeurtenis);

			workflowconfiguratie.VoegWorkItemconfiguratieToe(gebeurtenisdefinitie, opdrachtdefinitie);
		}

		public async Task RegistreerOpdrachtinformatieconfiguratie(INieuweOpdrachtgegevensvullingconfiguratie nieuweOpdrachtgegevensvullingconfiguratie) {
			var workflowconfiguratie = await _getWorkflowconfiguratie.ById(nieuweOpdrachtgegevensvullingconfiguratie.WorkflowconfiguratieId);
			workflowconfiguratie.SpecificeerOpdrachtgegevensconfiguratie(nieuweOpdrachtgegevensvullingconfiguratie);
		}

		public async Task ActiveerWorkflowconfiguratie(Guid workflowconfiguratieId) {
			var workflowconfiguratie = await _getWorkflowconfiguratie.ById(workflowconfiguratieId);
			workflowconfiguratie.Activeer();
		}
	}
}