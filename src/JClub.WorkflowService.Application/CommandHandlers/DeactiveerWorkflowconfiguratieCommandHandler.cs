using System.Threading.Tasks;
using JClub.Proxy.SignalR.GenericCommand;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class DeactiveerWorkflowconfiguratieCommandHandler : 
		IHandleMessages<DeactiveerWorkflowconfiguratieCommand>,
		IHandleMessages<DeactiveerWorkflowconfiguratieUiCommand> {
		private readonly IGetWorkflowconfiguraties _getWorkflowconfiguraties;
		private readonly IUnitOfWork _unitOfWork;

		public DeactiveerWorkflowconfiguratieCommandHandler(
			IGetWorkflowconfiguraties getWorkflowconfiguraties,
			IUnitOfWork unitOfWork) {
			_getWorkflowconfiguraties = getWorkflowconfiguraties;
			_unitOfWork = unitOfWork;
		}

		public async Task Handle(DeactiveerWorkflowconfiguratieCommand message, IMessageHandlerContext context) {
			var workflowconfiguratie = await _getWorkflowconfiguraties.ById(message.Id);

			if (workflowconfiguratie == null) {
				return;
			}

			workflowconfiguratie.Deactiveer(); 
			await _unitOfWork.SaveChanges();
		}

		public async Task Handle(
			DeactiveerWorkflowconfiguratieUiCommand message,
			IMessageHandlerContext                context) {

			await Handle(message.Data,context);
			await context.Publish(new GenericSignalrCommandCompletedEvent());
		}
	}
}