using System;
using System.Threading.Tasks;
using JClub.Proxy.SignalR.GenericCommand;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class RegistreerNieuweWorkItemconfiguratieCommandHandler : 
		IHandleMessages<RegistreerNieuweWorkItemconfiguratieCommand>,
		IHandleMessages<RegistreerNieuweWorkItemconfiguratieUiCommand> {

		private readonly IGetWorkflowconfiguraties _getWorkflowconfiguraties;
		private readonly IGetDefinities _getDefinities;
		private readonly IUnitOfWork _unitOfWork;

		public RegistreerNieuweWorkItemconfiguratieCommandHandler(
			IGetWorkflowconfiguraties getWorkflowconfiguraties,
			IGetDefinities getDefinities,
			IUnitOfWork unitOfWork) {
			_getWorkflowconfiguraties = getWorkflowconfiguraties;
			_getDefinities = getDefinities;
			_unitOfWork = unitOfWork;
		}

		public async Task Handle(
			RegistreerNieuweWorkItemconfiguratieCommand message,
			IMessageHandlerContext                      context) {

			var workflowconfiguratie = await _getWorkflowconfiguraties.ById(message.WorkflowconfiguratieId);

			if (workflowconfiguratie == null) {
				return;
			}

			var (opdrachtdefinitie, gebeurtenisdefinitie) = await _getDefinities.GetByIds(message.VoerOpdrachtUit, message.ReageerOpGebeurtenis);

			workflowconfiguratie.VoegWorkItemconfiguratieToe(message.VorigeWorkItemconfiguratieId, gebeurtenisdefinitie, opdrachtdefinitie);
			await _unitOfWork.SaveChanges();
		}

		public async Task Handle(
			RegistreerNieuweWorkItemconfiguratieUiCommand message,
			IMessageHandlerContext                        context) {

			await Handle(message.Data,context);

			await context.Publish(new GenericSignalrCommandCompletedEvent());
		}
	}
}