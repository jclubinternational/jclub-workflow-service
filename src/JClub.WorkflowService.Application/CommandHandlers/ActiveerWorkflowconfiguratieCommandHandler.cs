using System.Threading.Tasks;
using JClub.Proxy.SignalR.GenericCommand;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class ActiveerWorkflowconfiguratieCommandHandler : 
		IHandleMessages<ActiveerWorkflowconfiguratieCommand>,
		IHandleMessages<ActiveerWorkflowconfiguratieUiCommand> {
		private readonly IGetWorkflowconfiguraties _getWorkflowconfiguraties;
		private readonly IUnitOfWork _unitOfWork;

		public ActiveerWorkflowconfiguratieCommandHandler(
			IGetWorkflowconfiguraties getWorkflowconfiguraties,
			IUnitOfWork unitOfWork) {
			_getWorkflowconfiguraties = getWorkflowconfiguraties;
			_unitOfWork = unitOfWork;
		}

		public async Task Handle(ActiveerWorkflowconfiguratieCommand message, IMessageHandlerContext context) {
			var workflowconfiguratie = await _getWorkflowconfiguraties.ById(message.Id);

			if (workflowconfiguratie == null) {
				return;
			}

			workflowconfiguratie.Activeer(); 
			await _unitOfWork.SaveChanges();
		}

		public async Task Handle(
			ActiveerWorkflowconfiguratieUiCommand message,
			IMessageHandlerContext                context) {

			await Handle(message.Data,context);
			await context.Publish(new GenericSignalrCommandCompletedEvent());
		}
	}
}