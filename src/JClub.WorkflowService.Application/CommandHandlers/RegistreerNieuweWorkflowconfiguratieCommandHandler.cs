using System.Threading.Tasks;
using JClub.Proxy.SignalR.GenericCommand;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Domain.Aggregates;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class RegistreerNieuweWorkflowconfiguratieCommandHandler :
		IHandleMessages<RegistreerNieuweWorkflowconfiguratieCommand>,
		IHandleMessages<RegistreerNieuweWorkflowconfiguratieUiCommand> {

		private readonly ISaveWorkflowconfiguratie _saveWorkflowconfiguratie;
		private readonly IUnitOfWork _unitOfWork;

		public RegistreerNieuweWorkflowconfiguratieCommandHandler(
			ISaveWorkflowconfiguratie saveWorkflowconfiguratie,
			IUnitOfWork unitOfWork) {
			_saveWorkflowconfiguratie = saveWorkflowconfiguratie;
			_unitOfWork = unitOfWork;
		}

		public async Task Handle(RegistreerNieuweWorkflowconfiguratieCommand message, IMessageHandlerContext context) {
			var workflowconfiguratie = new Workflowconfiguratie(
				message.Titel,
				message.Beschrijving,
				message.AangemaaktDoor);

			await _saveWorkflowconfiguratie.SaveNew(workflowconfiguratie);
			await _unitOfWork.SaveChanges();
		}

		public async Task Handle(
			RegistreerNieuweWorkflowconfiguratieUiCommand message,
			IMessageHandlerContext                        context) {

			await Handle(message.Data,context);

			await context.Publish(new GenericSignalrCommandCompletedEvent());
		}
	}
}