using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Domain.ValueObjects;
using JClub.WorkflowService.Messages.Commands;
using Newtonsoft.Json;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class VoortzettenWorkflowCommandHandler :
		IHandleMessages<VoortzettenWorkflowCommand> {

		private readonly IGetWorkflows _getWorkflows;
		private readonly ISendOpdrachtGenerator _sendOpdrachtGenerator;
		private readonly IUnitOfWork _unitOfWork;

		public VoortzettenWorkflowCommandHandler(
			IGetWorkflows getWorkflows,
			ISendOpdrachtGenerator sendOpdrachtGenerator,
			IUnitOfWork unitOfWork) {

			_getWorkflows = getWorkflows;
			_unitOfWork = unitOfWork;
			_sendOpdrachtGenerator = sendOpdrachtGenerator;
		}

		public async Task Handle(VoortzettenWorkflowCommand message, IMessageHandlerContext context) {

			var gebeurtenisinfo = JsonConvert.DeserializeObject<Dictionary<string, string>>(message.EventBody);

			if (gebeurtenisinfo == null) {
				return;
			}

			var verstuurOpdracht = _sendOpdrachtGenerator.Create(context);
			var workflow = await _getWorkflows.ById(message.WorkflowId);

			var gebuertenis = new Gebeurtenis(message.EventName, gebeurtenisinfo);
			await workflow.PakVolgendeWorkItemOp(gebuertenis, verstuurOpdracht.Send); 
			await _unitOfWork.SaveChanges();
		}
	}
}