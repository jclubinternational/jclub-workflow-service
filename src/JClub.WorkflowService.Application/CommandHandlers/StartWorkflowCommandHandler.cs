using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Contracts;
using JClub.WorkflowService.Business.Domain.Aggregates;
using JClub.WorkflowService.Business.Domain.ValueObjects;
using JClub.WorkflowService.Messages.Commands;
using Newtonsoft.Json;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class StartWorkflowCommandHandler : IHandleMessages<StartWorkflowCommand> {
		private readonly IGetWorkflowconfiguraties _getWorkflowconfiguraties;
		private readonly ISaveWorkflow _saveWorkflow;
		private readonly ISendOpdrachtGenerator _sendOpdrachtGenerator;
		private readonly IUnitOfWork _unitOfWork;

		public StartWorkflowCommandHandler(
			IGetWorkflowconfiguraties getWorkflowconfiguraties,
			ISaveWorkflow saveWorkflow,
			ISendOpdrachtGenerator sendOpdrachtGenerator,
			IUnitOfWork unitOfWork) {
			_getWorkflowconfiguraties = getWorkflowconfiguraties;
			_saveWorkflow = saveWorkflow;
			_unitOfWork = unitOfWork;
			_sendOpdrachtGenerator = sendOpdrachtGenerator;
		}

		public async Task Handle(StartWorkflowCommand message, IMessageHandlerContext context) {

			var gebeurtenisinfo = JsonConvert.DeserializeObject<Dictionary<string, string>>(message.EventBody);

			if (gebeurtenisinfo == null) {
				return;
			}
			
			var workflowconfiguratie = await _getWorkflowconfiguraties.ById(message.WorkflowconfiguratieId);

			if (workflowconfiguratie == null) {
				return;
			}

			await Workflow.Start(
				message.CorrelationId, 
				workflowconfiguratie,
				async workflow => {
					var gebuertenis = new Gebeurtenis(message.EventName, gebeurtenisinfo); 
					var verstuurOpdracht = _sendOpdrachtGenerator.Create(context);

					await workflow.PakVolgendeWorkItemOp(gebuertenis, verstuurOpdracht.Send);
					await _saveWorkflow.SaveNew(workflow);
					await _unitOfWork.SaveChanges();
				});
		}
	}
}