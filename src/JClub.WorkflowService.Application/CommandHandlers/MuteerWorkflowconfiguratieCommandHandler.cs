using System.Threading.Tasks;
using JClub.Proxy.SignalR.GenericCommand;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class MuteerWorkflowconfiguratieCommandHandler :
		IHandleMessages<MuteerWorkflowconfiguratieCommand>,
		IHandleMessages<MuteerWorkflowconfiguratieUiCommand> {
		private readonly IGetWorkflowconfiguraties _getWorkflowconfiguraties;
		private readonly IUnitOfWork _unitOfWork;

		public MuteerWorkflowconfiguratieCommandHandler(
			IGetWorkflowconfiguraties getWorkflowconfiguraties,
			IUnitOfWork               unitOfWork) {
			_getWorkflowconfiguraties = getWorkflowconfiguraties;
			_unitOfWork               = unitOfWork;
		}

		public async Task Handle(
			MuteerWorkflowconfiguratieCommand message,
			IMessageHandlerContext            context) {

			var workflowconfiguratie = await _getWorkflowconfiguraties.ById(message.Id);

			if (workflowconfiguratie == null) {
				return;
			}

			workflowconfiguratie.PasAan(message.Titel,message.Beschrijving);
			await _unitOfWork.SaveChanges();
		}

		public async Task Handle(
			MuteerWorkflowconfiguratieUiCommand message,
			IMessageHandlerContext              context) {

			await Handle(message.Data,context);
			await context.Publish(new GenericSignalrCommandCompletedEvent());
		}
	}
}