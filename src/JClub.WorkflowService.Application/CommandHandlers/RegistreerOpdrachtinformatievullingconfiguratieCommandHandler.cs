using System.Threading.Tasks;
using JClub.Proxy.SignalR.GenericCommand;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class RegistreerOpdrachtinformatievullingconfiguratieCommandHandler : 
		IHandleMessages<RegistreerNieuweOpdrachtgegevensvullingconfiguratieCommand>,
		IHandleMessages<RegistreerNieuweOpdrachtgegevensvullingconfiguratieUiCommand> {

		private readonly IGetWorkflowconfiguraties _getWorkflowconfiguraties;
		private readonly IUnitOfWork _unitOfWork;

		public RegistreerOpdrachtinformatievullingconfiguratieCommandHandler(
			IGetWorkflowconfiguraties getWorkflowconfiguraties,
			IUnitOfWork               unitOfWork) {

			_getWorkflowconfiguraties = getWorkflowconfiguraties;
			_unitOfWork               = unitOfWork;
		}

		public async Task Handle(
			RegistreerNieuweOpdrachtgegevensvullingconfiguratieCommand message,
			IMessageHandlerContext                                     context) {

			var workflowconfiguratie = await _getWorkflowconfiguraties.ByWorkItemconfiguratieId(message.WorkItemconfiguratieId);

			if (workflowconfiguratie == null) {
				return;
			}

			workflowconfiguratie.SpecificeerOpdrachtgegevensconfiguratie(message.WorkItemconfiguratieId,message.Opdrachtgegevenvullingen);
			await _unitOfWork.SaveChanges();
		}

		public async Task Handle(
			RegistreerNieuweOpdrachtgegevensvullingconfiguratieUiCommand message,
			IMessageHandlerContext                                       context) {

			await Handle(message.Data,context);
			await context.Publish(new GenericSignalrCommandCompletedEvent());
		}
	}
}