using System;
using System.Threading.Tasks;
using JClub.Proxy.SignalR.GenericCommand;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class MuteerWorkItemconfiguratieCommandHandler :
		IHandleMessages<MuteerWorkItemconfiguratieCommand>,
		IHandleMessages<MuteerWorkItemconfiguratieUiCommand> {

		private readonly IGetWorkflowconfiguraties _getWorkflowconfiguraties;
		private readonly IGetDefinities _getDefinities;
		private readonly IUnitOfWork _unitOfWork;

		public MuteerWorkItemconfiguratieCommandHandler(
			IGetWorkflowconfiguraties getWorkflowconfiguraties,
			IGetDefinities getDefinities,
			IUnitOfWork unitOfWork) {

			_getWorkflowconfiguraties                 = getWorkflowconfiguraties;
			_getDefinities = getDefinities;
			_unitOfWork                               = unitOfWork;
		}

		public async Task Handle(
			MuteerWorkItemconfiguratieCommand message,
			IMessageHandlerContext            context) {

			var workflowconfiguratie = await _getWorkflowconfiguraties.ByWorkItemconfiguratieId(message.Id);

			if (workflowconfiguratie == null) {
				return;
			}

			if (!workflowconfiguratie.ZoekWorkItemconfiguratie(message.Id,out var workItemconfiguratie)) {
				return;
			}

			var test = (message.ReageerOpGebeurtenis,message.VoerOpdrachtUit);

			var (opdrachtdefinitie, gebeurtenisdefinitie) = test switch {
				(not null, not null) x => await _getDefinities.GetByIds(x.VoerOpdrachtUit.Value, x.ReageerOpGebeurtenis.Value),
				(null, not null) x     => (await _getDefinities.GetOpdrachtdefinitieById(x.VoerOpdrachtUit.Value), null),
				(not null, null) x     => (null, await _getDefinities.GetGebeurtenisdefinitieById(x.ReageerOpGebeurtenis.Value)),
				_                      => (null, null)
			};

			workflowconfiguratie.MuteerWorkItemconfiguratie(workItemconfiguratie, message.Beschrijving, gebeurtenisdefinitie, opdrachtdefinitie);
			await _unitOfWork.SaveChanges();
		}

		public async Task Handle(
			MuteerWorkItemconfiguratieUiCommand message,
			IMessageHandlerContext              context) {

			await Handle(message.Data,context);
			await context.Publish(new GenericSignalrCommandCompletedEvent());
		}
	}
}