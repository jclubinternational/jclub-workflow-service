using System.Threading.Tasks;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.Application.CommandHandlers {
	public class VerstuurOpdrachtCommandHandler : IHandleMessages<VerstuurOpdrachtCommand> {
		public Task Handle(VerstuurOpdrachtCommand message, IMessageHandlerContext context) {
			return Task.CompletedTask;
		}
	}
}