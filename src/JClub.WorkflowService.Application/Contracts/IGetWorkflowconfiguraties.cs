using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Domain.Aggregates;

namespace JClub.WorkflowService.Application.Contracts {
	public interface IGetWorkflowconfiguraties {
		Task<Workflowconfiguratie?> ById(Guid                      id);
		Task<Workflowconfiguratie?> ByWorkItemconfiguratieId(Guid id);

		Task<IReadOnlyCollection<Workflowconfiguratie>> ByGebeurtenisnaam(string naam);
	}
}