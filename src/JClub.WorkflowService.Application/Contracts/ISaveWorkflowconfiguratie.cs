using System.Threading.Tasks;
using JClub.WorkflowService.Business.Domain.Aggregates;

namespace JClub.WorkflowService.Application.Contracts {
	public interface ISaveWorkflowconfiguratie {
		public Task SaveNew(Workflowconfiguratie workflowconfiguratie);
	}
}