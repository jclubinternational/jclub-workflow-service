using System.Threading.Tasks;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowEntities;

namespace JClub.WorkflowService.Application.Contracts {
	public interface ISendOpdracht {
		Task Send(Opdracht opdracht);
	}
}