using System;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Domain.Aggregates;

namespace JClub.WorkflowService.Business.Contracts {
	public interface IGetWorkflow {
		Task<Workflow> ById(Guid id);
	}
}