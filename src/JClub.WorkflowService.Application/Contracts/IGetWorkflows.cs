using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Domain.Aggregates;

namespace JClub.WorkflowService.Application.Contracts {
	public interface IGetWorkflows {
		Task<Workflow> ById(Guid id);

		Task<IReadOnlyCollection<Workflow>> ByGebeurtenisnaam(Guid correlationId, string naam);
	}
}