using JClub.WorkflowService.Business.Contracts;
using NServiceBus;

namespace JClub.WorkflowService.Application.Contracts {
	public interface ISendOpdrachtGenerator {
		ISendOpdracht Create(IMessageHandlerContext context);
	}
}