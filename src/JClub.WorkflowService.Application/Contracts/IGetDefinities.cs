using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;

namespace JClub.WorkflowService.Application.Contracts {
	public interface IGetDefinities {

		Task<IReadOnlyCollection<Opdrachtdefinitie>> GetAllOpdrachtdefinities();

		Task<Opdrachtdefinitie?> GetOpdrachtdefinitieById(Guid id);

		Task<IReadOnlyCollection<Gebeurtenisdefinitie>> GetAllGebeurtenisdefinities();

		Task<Gebeurtenisdefinitie?> GetGebeurtenisdefinitieById(Guid id);

		Task<(Opdrachtdefinitie, Gebeurtenisdefinitie)> GetByIds(Guid opdrachtdefinitieId, Guid gebeurtenisdefinitieId);
	}
}