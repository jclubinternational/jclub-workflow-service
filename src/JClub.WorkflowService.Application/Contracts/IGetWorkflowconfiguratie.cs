using System;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Domain.Aggregates;

namespace JClub.WorkflowService.Business.Contracts {
	public interface IGetWorkflowconfiguratie {
		public Task<Workflowconfiguratie> ById(Guid id);
	}
}