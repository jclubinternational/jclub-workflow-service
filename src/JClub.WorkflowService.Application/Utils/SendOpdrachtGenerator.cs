using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowEntities;
using JClub.WorkflowService.Messages.Commands;
using Newtonsoft.Json;
using NServiceBus;

namespace JClub.WorkflowService.Application.Utils {
	public class SendOpdrachtGenerator :
		ISendOpdrachtGenerator,
		ISendOpdracht {

		private readonly IMessageHandlerContext? _messageHandlerContext;

		public SendOpdrachtGenerator() {
			
		}

		private SendOpdrachtGenerator(
			IMessageHandlerContext messageHandlerContext) {
			_messageHandlerContext = messageHandlerContext;
		}

		public async Task Send(Opdracht opdracht) {

			var options = new SendOptions();
			options.SetDestination(opdracht.Definitie.Adres);

			var gegevens = opdracht.Opdrachtgegevens.ToDictionary(_ => _.Naam, _ => _.Waarde);

			var command = new VerstuurOpdrachtCommand {
				Naam = opdracht.Definitie.Naam, 
				Adres = opdracht.Definitie.Adres, 
				Gegevens = JsonConvert.SerializeObject(gegevens)
			};

			await _messageHandlerContext!.Send(command, options);
		}

		public ISendOpdracht Create(IMessageHandlerContext context) {
			return new SendOpdrachtGenerator(context);
		}
	}
}