﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using JClub.WorkflowService.Api;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace JClub.WorkflowService.Tests {
	[TestFixture]
	internal class AutoMapperTests {

		[Test]
		public void AutoMapper_configuratie_is_geldig() {
			// Arrange
			var mapper = GetConfiguredAutoMapper();

			// Act
			mapper.ConfigurationProvider.AssertConfigurationIsValid();

			// Assert

		}

		public static IMapper GetConfiguredAutoMapper() {
			// Connectionstring override voor NServiceBus initialisatie
			var webhost = Program.CreateHostBuilder(new [] { "--ConnectionStrings:Database", "not empty"}).Build();
			var mapper = webhost.Services.GetRequiredService<IMapper>();
			return mapper;
		}

	}
}
