﻿using System.Threading.Tasks;
using FluentAssertions;
using JClub.WorkflowService.Tests.Utils;
using NUnit.Framework;

namespace JClub.WorkflowService.Tests.Api {
	[TestFixture]
	public class HealthCheckTest : BaseIntegrationTest {
		[TestCase("/health")]
		public async Task TestHealthCheck(string url) {
			// Arrange
			var client = Factory.CreateClient();

			// Act
			var response = await client.GetAsync(url);

			// Assert
			response.EnsureSuccessStatusCode(); // Status Code 200-299
			response.Content.Headers.ContentType?.ToString()
				.Should().Be("application/json");
		}
	}
}
