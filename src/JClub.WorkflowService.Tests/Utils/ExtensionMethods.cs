﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using FluentAssertions.Collections;
using NServiceBus;
using NServiceBus.Testing;

namespace JClub.WorkflowService.Tests.Utils {
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	internal static class ExtensionMethods {

		public static IEnumerable<T> Published<T>(this TestableMessageHandlerContext context) => context.PublishedMessages.Select(i => i.Message).OfType<T>();
		public static IEnumerable<T> Sent<T>(this TestableMessageHandlerContext context) => context.SentMessages.Where(i => !context.TimeoutMessages.Any(t => t.Message == i.Message)).Select(i => i.Message).OfType<T>();
		public static IEnumerable<T> Timeouts<T>(this TestableMessageHandlerContext context) => context.TimeoutMessages.Select(i => i.Message).OfType<T>();

		public static void ShouldBeEmpty(this TestableMessageHandlerContext context) {
			context.SentMessages.Should().HaveCount(0);
			context.PublishedMessages.Should().HaveCount(0);
			context.TimeoutMessages.Should().HaveCount(0);
		}

		public static X GetFirst<X>(this AndConstraint<GenericCollectionAssertions<X>> obj) => obj.And.Subject.First();
		public static X? GetFirstOrDefault<X>(this AndConstraint<GenericCollectionAssertions<X>> obj) => obj.And.Subject.FirstOrDefault();

		public static IEnumerable<T> Published<T>(this IEnumerable<IEvent> events) => events.OfType<T>();

	}
}
