﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.Shared.MessageBus;
using NServiceBus;

namespace JClub.WorkflowService.Tests.Utils {
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class MockMessageBus : IMessageBus {
		public List<object> Events { get; private set; } = new List<object>();
		public List<object> Commands { get; private set; } = new List<object>();

		public Task PublishEvent(IEvent @event, PublishOptions? options = null) {
			Events.Add(@event);
			return Task.CompletedTask;
		}

		public Task SendCommand(ICommand command, SendOptions? options = null) {
			Commands.Add(command);
			return Task.CompletedTask;
		}

		public Task SendCommandLocal(ICommand command) {
			Commands.Add(command);
			return Task.CompletedTask;
		}

		public Task SendMessage(IMessage message, SendOptions? options = null) {
			throw new NotImplementedException();
		}

		public Task<TResult> Request<TResult>(IMessage message, string destinationEndpointName) where TResult : IMessage {
			throw new NotImplementedException();
		}

		public IEnumerable<T> Published<T>() => Events.OfType<T>();
		public IEnumerable<T> Sent<T>() => Commands.OfType<T>();

	}
}
