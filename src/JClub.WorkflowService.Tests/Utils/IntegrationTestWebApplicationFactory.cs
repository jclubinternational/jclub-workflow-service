﻿using System;
using JClub.WorkflowService.Api;
using JClub.WorkflowService.Connector;
using JClub.WorkflowService.Data;
using JClub.Shared.MessageBus.IntegrationTesting;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using StructureMap;

namespace JClub.WorkflowService.Tests.Utils {
	public class IntegrationTestWebApplicationFactory : WebApplicationFactory<Startup> {
		protected override void ConfigureWebHost(IWebHostBuilder builder) {
			builder
				.ConfigureAppConfiguration(b => b.AddCommandLine(new [] { "--ConnectionStrings:Database", "not empty"}))
				.ConfigureServices(svc => {
					// Bereid de IntegratieTestFactory voor op de MessageBus en de DataContextAccessor
					svc.SetupMessageBusIntegrationTestingComponents<WorkflowServiceDataContext>("WorkflowService");
					// Voeg ook de connector toe
					svc.AddWorkflowServiceConnector(opts => opts.ApiUrl = "http://localhost");
				});
		}
	}

	public abstract class BaseIntegrationTest : IDisposable {
		private IntegrationTestWebApplicationFactory? _factory;

		[SetUp]
		public void Setup() {
			_factory = new IntegrationTestWebApplicationFactory();
			var db = _factory.Services.GetRequiredService<WorkflowServiceDataContext>();
			db.Database.OpenConnection();
			db.Database.EnsureCreated();
		}

		[TearDown]
		public void Teardown() {
			_factory?.Services?.GetService<IContainer>()?.Dispose();
			_factory?.Dispose();
		}

		protected IntegrationTestWebApplicationFactory Factory => _factory ?? throw new InvalidOperationException();
		
		protected virtual void Dispose(bool disposing) {
			if (disposing) {
				_factory?.Dispose();
			}
		}

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
