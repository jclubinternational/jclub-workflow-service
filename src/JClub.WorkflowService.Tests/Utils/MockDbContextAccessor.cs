﻿using System.Threading;
using System.Threading.Tasks;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JClub.WorkflowService.Tests.Utils {
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class MockDbContextAccessor<T> : DbContextAccessor<T> where T: BaseDbContext<T> {
		private readonly T _dbContext;
		public MockDbContextAccessor(T dbContext) {
			_dbContext = dbContext;
		}

		public override T DbContext => _dbContext;

		protected override Task<int> SaveChanges(CancellationToken cancellationToken = default) {
			return _dbContext.SaveChangesAsync(cancellationToken);
		}
	}
}
