namespace JClub.WorkflowService.Business.Domain.ValueObjects {
	public class Gebeurtenisconfiguratie {
		public string Name { get; private set; } = string.Empty;
		public string EndPoint { get; private set; } = string.Empty;
	}
}