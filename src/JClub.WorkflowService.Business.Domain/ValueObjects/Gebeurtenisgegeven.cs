namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowAggregate {
	public sealed class Gebeurtenisgegeven {
		internal Gebeurtenisgegeven(
			string naam,
			string waarde) {

			Naam   = naam;
			Waarde = waarde;
		}

		public string Naam { get; set; }
		public string Waarde { get; set; }
	}
}