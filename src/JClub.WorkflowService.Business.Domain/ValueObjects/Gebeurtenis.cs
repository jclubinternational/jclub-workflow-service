using System.Collections.Generic;
using System.Linq;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowAggregate;

namespace JClub.WorkflowService.Business.Domain.ValueObjects {
	public sealed class Gebeurtenis {

		public Gebeurtenis(
			string                     naam,
			IDictionary<string,string> gebeurtenisgegevens) {
			Naam = naam;
			Gebeurtenisgegevens = gebeurtenisgegevens.Select(_ => new Gebeurtenisgegeven(_.Key,_.Value))
													 .ToArray();
		}

		public string Naam { get; private set; }
		public IReadOnlyList<Gebeurtenisgegeven> Gebeurtenisgegevens { get; private set; }
	}
}