using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Business.Contracts.Containers;
using JClub.WorkflowService.Business.Contracts.Enums;
using JClub.WorkflowService.Business.Contracts.Events;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;

namespace JClub.WorkflowService.Business.Domain.Aggregates {
	public sealed class Workflowconfiguratie : 
		EntityContainingEvents {

		private readonly List<WorkItemconfiguratie> _workItemconfiguraties = new();

		private Workflowconfiguratie() { }

		public Workflowconfiguratie(
			string titel,
			string beschrijving,
			Guid   aangemaaktDoor) {

			Id             = Guid.NewGuid();
			Titel          = titel;
			Beschrijving   = beschrijving;
			AangemaaktDoor = aangemaaktDoor;

			AddEvent(new NieuweWorkflowconfiguratieGeregistreerdEvent(Id));
		}

		public Guid Id { get; private set; }
		public string Titel { get; private set; } = string.Empty;
		public string Beschrijving { get; private set; } = string.Empty;
		public Guid AangemaaktDoor { get; private set; }
		public WorkItemconfiguratie? StartWorkItemconfiguratie => _workItemconfiguraties.SingleOrDefault(x => x.VorigeWorkItemconfiguratieId == null);
		public Gebeurtenisdefinitie? ReageerOpGebeurtenis { get; private set; }
		public Workflowconfiguratie? VervangenDoor { get; private set; }
		public bool IsActief { get; private set; }

		public IReadOnlyList<WorkItemconfiguratie> WorkItemconfiguraties => _workItemconfiguraties;

		public IEnumerable<WorkItemconfiguratie> SortedWorkItemconfiguraties {
			get {

				if (StartWorkItemconfiguratie == null) {
					return _workItemconfiguraties;
				}

				var itemsById = _workItemconfiguraties.ToDictionary(_ => _.VorigeWorkItemconfiguratieId ?? Guid.Empty);

				var result = new Collection<WorkItemconfiguratie> {
					StartWorkItemconfiguratie
				};

				FindNext(StartWorkItemconfiguratie);

				void FindNext(WorkItemconfiguratie item) {
					if (itemsById.TryGetValue(item.Id, out var nextItem)) {
						result.Add(nextItem);
						FindNext(nextItem);
					}
				}

				return result;
			}
		}

		public void PasAan(
			string titel, 
			string beschrijving) {

			Titel        = titel;
			Beschrijving = beschrijving;

			AddEvent(new WorkflowconfiguratieAangepastEvent(Id));
		}

		public void MuteerWorkItemconfiguratie(
			WorkItemconfiguratie  workItemconfiguratie,
			string?               beschrijving,
			Gebeurtenisdefinitie? reageerOpGebeurtenis,
			Opdrachtdefinitie?    voerOpdrachtUit) {

			var isGemuteert = false;

			if (beschrijving != null &&
				workItemconfiguratie.Beschrijving != beschrijving) {

				workItemconfiguratie.Beschrijving = beschrijving;
				isGemuteert                       = true;
			}

			if (reageerOpGebeurtenis != null &&
				workItemconfiguratie.VorigeWorkItemconfiguratieId == null &&
				workItemconfiguratie.ReageerOpGebeurtenis != reageerOpGebeurtenis) {

				var gebeurtenisgegevendefinities = BepaalGebeurtenisgegevendefinitiesPrivate(workItemconfiguratie.Id, BepaalGebeurtenisgegevendefinitiesModi.SlaEersteOver);
				workItemconfiguratie.ReageerOpDezeGebeurtenis(reageerOpGebeurtenis, gebeurtenisgegevendefinities);
				ReageerOpGebeurtenis = reageerOpGebeurtenis;

				var volgendeWorkItemconfiguratie = WorkItemconfiguraties.SingleOrDefault(_ => _.VorigeWorkItemconfiguratieId == workItemconfiguratie.Id);
				while (volgendeWorkItemconfiguratie != null) {
					gebeurtenisgegevendefinities = BepaalGebeurtenisgegevendefinities(volgendeWorkItemconfiguratie.Id);
					volgendeWorkItemconfiguratie.HerevualeerOpdrachtgegevenvullingconfiguraties(gebeurtenisgegevendefinities);
					volgendeWorkItemconfiguratie = WorkItemconfiguraties.SingleOrDefault(_ => _.VorigeWorkItemconfiguratieId == volgendeWorkItemconfiguratie.Id);
				}

				isGemuteert = true;
			}

			if (voerOpdrachtUit != null &&
				workItemconfiguratie.VoerOpdrachtUit != voerOpdrachtUit) {

				workItemconfiguratie.VoerDezeOpdrachtUit(voerOpdrachtUit);
				var volgendeWorkItemconfiguratie = WorkItemconfiguraties.SingleOrDefault(_ => _.VorigeWorkItemconfiguratieId == workItemconfiguratie.Id);

				if (volgendeWorkItemconfiguratie != null) {
					var gebeurtenisgegevendefinities = BepaalGebeurtenisgegevendefinitiesPrivate(volgendeWorkItemconfiguratie.Id, BepaalGebeurtenisgegevendefinitiesModi.SlaEersteOver);
					volgendeWorkItemconfiguratie.ReageerOpDezeGebeurtenis(voerOpdrachtUit.ResulterendeGebeurtenis, gebeurtenisgegevendefinities);
					volgendeWorkItemconfiguratie = WorkItemconfiguraties.SingleOrDefault(_ => _.VorigeWorkItemconfiguratieId == volgendeWorkItemconfiguratie.Id);
				}

				while (volgendeWorkItemconfiguratie != null) {
					var gebeurtenisgegevendefinities = BepaalGebeurtenisgegevendefinities(volgendeWorkItemconfiguratie.Id);
					volgendeWorkItemconfiguratie.HerevualeerOpdrachtgegevenvullingconfiguraties(gebeurtenisgegevendefinities);
					volgendeWorkItemconfiguratie = WorkItemconfiguraties.SingleOrDefault(_ => _.VorigeWorkItemconfiguratieId == volgendeWorkItemconfiguratie.Id);
				}
				isGemuteert = true;
			}

			if (isGemuteert) {
				AddEvent(new WorkItemconfiguratieGemuteerdEvent(workItemconfiguratie.Id));
			}
		}

		public void VoegWorkItemconfiguratieToe(
			Guid?                vorigeWorkItemId,
			Gebeurtenisdefinitie reageerOpGebeurtenis,
			Opdrachtdefinitie    voerOpdrachtUit) {

			switch (vorigeWorkItemId, StartWorkItemconfiguratie) {
				case (not null, null):
				case (null,null): {
					var workItemconfiguratie = new WorkItemconfiguratie(reageerOpGebeurtenis,voerOpdrachtUit);
					ReageerOpGebeurtenis = reageerOpGebeurtenis;
					_workItemconfiguraties.Add(workItemconfiguratie);
					AddEvent(new NieuweWorkItemconfiguratieGeregistreerdEvent(workItemconfiguratie.Id));
					return;
				}
				case (null,not null): {
					var workItemconfiguratie = new WorkItemconfiguratie(reageerOpGebeurtenis,voerOpdrachtUit);
					StartWorkItemconfiguratie.SchikVoor(workItemconfiguratie);
					ReageerOpGebeurtenis = reageerOpGebeurtenis;
					_workItemconfiguraties.Add(workItemconfiguratie);
					AddEvent(new NieuweWorkItemconfiguratieGeregistreerdEvent(workItemconfiguratie.Id));
					return;
				}
				case (not null,not null) when ZoekWorkItemconfiguratie(vorigeWorkItemId.Value, out var vorigeItemconfiguratie): {
					var workItemconfiguratie = new WorkItemconfiguratie(vorigeItemconfiguratie, reageerOpGebeurtenis,voerOpdrachtUit);

					foreach (var volgendeWorkItemconfiguratie in _workItemconfiguraties.Where(_ => _.VorigeWorkItemconfiguratieId == vorigeItemconfiguratie.Id)) {
						volgendeWorkItemconfiguratie.SchikVoor(workItemconfiguratie);
					}

					_workItemconfiguraties.Add(workItemconfiguratie);
					AddEvent(new NieuweWorkItemconfiguratieGeregistreerdEvent(workItemconfiguratie.Id));
					return;
				}
				default:
					return;
			}
		}

		public bool ZoekWorkItemconfiguratie(
			Guid                                          workItemconfiguratieId,
			[NotNullWhen(true)] out WorkItemconfiguratie? workItemconfiguratie) {

			workItemconfiguratie = _workItemconfiguraties.SingleOrDefault(_ => _.Id == workItemconfiguratieId);

			return workItemconfiguratie != null;
		}

		public IReadOnlyCollection<Gebeurtenisgegevendefinitie> BepaalGebeurtenisgegevendefinities(Guid workItemconfiguratieId) {
			return BepaalGebeurtenisgegevendefinitiesPrivate(workItemconfiguratieId);
		}

		private IReadOnlyCollection<Gebeurtenisgegevendefinitie> BepaalGebeurtenisgegevendefinitiesPrivate(
			Guid workItemconfiguratieId,
			BepaalGebeurtenisgegevendefinitiesModi modus = BepaalGebeurtenisgegevendefinitiesModi.VanafEerste) {

			if (!ZoekWorkItemconfiguratie(workItemconfiguratieId, out var workItemconfiguratie)) {
				return Array.Empty<Gebeurtenisgegevendefinitie>();
			}

			var gebeurtenisgegevendefinities = new List<Gebeurtenisgegevendefinitie>();

			if (modus == BepaalGebeurtenisgegevendefinitiesModi.VanafEerste) {
				gebeurtenisgegevendefinities.AddRange(workItemconfiguratie.ReageerOpGebeurtenis.Gegevensdefinities);
			}

			voegVoorgaandeGebeurtenisgegevendefinitiesToe(workItemconfiguratie.VorigeWorkItemconfiguratieId);

			return gebeurtenisgegevendefinities.Distinct()
											   .ToArray();

			void voegVoorgaandeGebeurtenisgegevendefinitiesToe(Guid? vorigeItemId) {
				if (vorigeItemId == null) {
					return;
				}

				if (ZoekWorkItemconfiguratie(vorigeItemId.Value, out var vorigeWorkItemconfiguratie)) {
					gebeurtenisgegevendefinities.AddRange(vorigeWorkItemconfiguratie.ReageerOpGebeurtenis.Gegevensdefinities);
					voegVoorgaandeGebeurtenisgegevendefinitiesToe(vorigeWorkItemconfiguratie.VorigeWorkItemconfiguratieId);
				}
			}
		}

		public void SpecificeerOpdrachtgegevensconfiguratie(
			Guid                                         workItemconfiguratieId,
			IReadOnlyCollection<IOpdrachtgegevenvulling> opdrachtinformatievullingen) {

			if (!ZoekWorkItemconfiguratie(workItemconfiguratieId,out var workItemconfiguratie)) {
				return;
			}

			var opdrachtgegevendefinities = workItemconfiguratie.VoerOpdrachtUit.Gegevensdefinities;
			var gebeurtenisgegevendefinities = BepaalGebeurtenisgegevendefinities(workItemconfiguratieId);

			var opdrachtinformatievullingconfiguraties = new List<Opdrachtgegevenvullingconfiguratie>();
			foreach (var vulling in opdrachtinformatievullingen) {

				var opdrachtdefinitie = opdrachtgegevendefinities.Single(_ => _.Id == vulling.OpdrachtgegevendefinitieId);

				var input = (GebeurtenisinformatiedefinitieId: vulling.GebeurtenisgegevendefinitieId,vulling.IngevuldeWaarde);
				object? informatie = input switch {
					(not null,_) => gebeurtenisgegevendefinities.Single(_ => _.Id == vulling.GebeurtenisgegevendefinitieId),
					(_,not null) => vulling.IngevuldeWaarde!,
					_            => default
				};

				var opdrachtinformatievullingconfiguratie = informatie switch {
					Gebeurtenisgegevendefinitie g => new Opdrachtgegevenvullingconfiguratie(opdrachtdefinitie,g),
					string ingevuldeWaarde        => new Opdrachtgegevenvullingconfiguratie(opdrachtdefinitie,ingevuldeWaarde),
					_                             => new Opdrachtgegevenvullingconfiguratie(opdrachtdefinitie)
				};

				opdrachtinformatievullingconfiguraties.Add(opdrachtinformatievullingconfiguratie);
			}

			workItemconfiguratie.SpecificeerOpdrachtgegevensconfiguraties(opdrachtinformatievullingconfiguraties);

			AddEvent(new OpdrachtinformatievullingconfiguratieGeregistreerdEvent());
		}

		public void Activeer() {

			if (IsActief) {
				return;
			}

			IsActief = ZijnAlleWorkItemsKlaarOmUitTeVoeren();

			if (IsActief) {
				AddEvent(new WorkflowconfiguratieGeactiveerdEvent(Id));
			} else {
				AddEvent(new WorkflowconfiguratieActivatieGefaaldEvent(Id));
			}
		}

		public IWorkflowconfiguratieViewModel ToViewModel() =>
			new WorkflowconfiguratieViewModel {
				Id                    = Id,
				Titel                 = Titel,
				Beschrijving          = Beschrijving,
				AangemaaktDoor        = AangemaaktDoor,
				ReageertOpGebeurtenis = StartWorkItemconfiguratie?.ReageerOpGebeurtenis.Naam,
				VervangenDoor         = VervangenDoor?.Titel,
				IsActief              = IsActief
			};

		private bool ZijnAlleWorkItemsKlaarOmUitTeVoeren() {

			return _workItemconfiguraties.All(_ => _.Status == WorkItemconfiguratieStatus.KlaarOmUitgevoerdTeWorden);
		}

		private enum BepaalGebeurtenisgegevendefinitiesModi {
			VanafEerste,
			SlaEersteOver
		}

		public void Deactiveer() {
			if (!IsActief) {
				return;
			}

			IsActief = false;

			AddEvent(new WorkflowconfiguratieGedeactiveerdEvent(Id));
		}
	}
}