using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Business.Contracts.Enums;
using JClub.WorkflowService.Business.Contracts.Events;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowEntities;
using JClub.WorkflowService.Business.Domain.ValueObjects;

namespace JClub.WorkflowService.Business.Domain.Aggregates {
	public sealed class Workflow : 
		EntityContainingEvents {

		private readonly List<WorkItem> _workItems = new();
		private Workflow() { }

		public Guid Id { get; private set; }
		public Guid WorkflowconfiguratieId { get; private set; }
		public Guid CorrelationId { get; private set; }
		public string Titel { get; private set; } = string.Empty;
		public DateTime GestartOp { get; private set; }
		public DateTime? GestoptOp { get; private set; }
		public WorkflowStatus Status { get; private set; }
		public Gebeurtenisdefinitie ReageerOpGebeurtenis { get; private set; } = null!;

		public WorkItem HuidigeWorkItem => _workItems.Single(_ => _.Id == HuidigeWorkItemId);
		
		public Guid HuidigeWorkItemId { get; private set; }

		public async Task PakVolgendeWorkItemOp(
			Gebeurtenis         gebeurtenis,
			Func<Opdracht,Task> stuur) {

			if (ReageerOpGebeurtenis.Naam != gebeurtenis.Naam) {
				return;
			}

			var huidigeWorkItem = HuidigeWorkItem;

			var vorigeWorkItemId = huidigeWorkItem.VorigeWorkItemId;

			if (huidigeWorkItem.Status == WorkItemStatus.Opgepakt) {
				huidigeWorkItem.Voltooid();
				vorigeWorkItemId = huidigeWorkItem.Id;
			}

			var workItem = GeefWorkItem(gebeurtenis, vorigeWorkItemId);

			if (workItem == null) {
				Status    = WorkflowStatus.Voltooid;
				GestoptOp = DateTime.UtcNow;
				AddEvent(new WorkflowVoltooidEvent(Id));
				return;
			}

			var opdracht = workItem.Opdracht;
			opdracht.AanvullenMetGebeurtenis(gebeurtenis);

			ReageerOpGebeurtenis = opdracht.Definitie.ResulterendeGebeurtenis;
			HuidigeWorkItemId    = workItem.Id;

			if (opdracht.ZijnGegevensCompleet != true) {
				return;
			}

			AddEvent(new WorkflowVoortgezetEvent(Id));
			workItem.Opgepakt();

			await stuur(opdracht);
		}

		private WorkItem? GeefWorkItem(
			Gebeurtenis gebeurtenis, Guid? vorigeWorkItemId) {

			return _workItems.Where(_ => _.Status == WorkItemStatus.TeDoen)
							 .SingleOrDefault(_ => _.ReageerOpGebeurtenis.Naam == gebeurtenis.Naam &&
												   _.VorigeWorkItemId == vorigeWorkItemId);
		}

		public IWorkflowViewModel ToViewModel() =>
			new WorkflowViewModel {
				Titel           = Titel,
				GestartOp       = GestartOp,
				GestoptOp       = GestoptOp,
				Status          = Status,
				HuidigeWorkItem = HuidigeWorkItem.Titel
			};

		public static Task Start(
			Guid                 correlationId,
			Workflowconfiguratie configuratie,
			Func<Workflow,Task>  gemaakt) {

			if (configuratie.StartWorkItemconfiguratie == null) {
				return Task.CompletedTask;
			}

			var workflow = new Workflow {
				Id                     = Guid.NewGuid(),
				WorkflowconfiguratieId = configuratie.Id,
				CorrelationId          = correlationId,
				Titel                  = configuratie.Titel,
				Status                 = WorkflowStatus.Gestart,
				GestartOp              = DateTime.UtcNow,
			};

			var startWorkItem = new WorkItem(workflow,configuratie.StartWorkItemconfiguratie);
			workflow.ReageerOpGebeurtenis = startWorkItem.ReageerOpGebeurtenis;
			workflow.HuidigeWorkItemId    = startWorkItem.Id;

			workflow._workItems.Add(startWorkItem);
			
			MaakFlow(configuratie.StartWorkItemconfiguratie, startWorkItem);

			void MaakFlow(
				WorkItemconfiguratie vorigeWorkItemconfiguratie,
				WorkItem             vorigeWorkItem) {

				var workItemconfiguratie = configuratie.WorkItemconfiguraties
													   .SingleOrDefault(_ => _.VorigeWorkItemconfiguratieId == vorigeWorkItemconfiguratie.Id);

				if (workItemconfiguratie == null) {
					return;
				}

				var workItem = new WorkItem(workflow, workItemconfiguratie,vorigeWorkItem);
				workflow._workItems.Add(workItem);
				MaakFlow(workItemconfiguratie, workItem);
			}

			workflow.AddEvent(new WorkflowGestartEvent(workflow.Id));

			return gemaakt(workflow);
		}
	}
}