using System;
using System.Collections.Generic;
using System.Linq;
using JClub.WorkflowService.Business.Contracts.ViewModels;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities {
	public sealed class Opdrachtdefinitie {

		private List<Opdrachtgegevendefinitie> _gegevensdefinities = new();

		private Opdrachtdefinitie() {}

		public Opdrachtdefinitie(
			string               naam,
			string               adres,
			Gebeurtenisdefinitie resulterendeGebeurtenis) {

			Id                      = Guid.NewGuid();
			Naam                    = naam;
			Adres                   = adres;
			ResulterendeGebeurtenis = resulterendeGebeurtenis;
		}

		public Guid Id { get; private set; }
		public string Naam { get; private set; } = string.Empty;
		public string Adres { get; private set; } = string.Empty;

		public void VoegGegevensdefinitiesToe(params Opdrachtgegevendefinitie[] definities) {
			_gegevensdefinities.AddRange(definities);
		}

		public IReadOnlyCollection<Opdrachtgegevendefinitie> Gegevensdefinities => _gegevensdefinities;
		public Gebeurtenisdefinitie ResulterendeGebeurtenis { get; private set; } = null!;

		public IEnumerable<IOpdrachtgegevendefinitieViewModel> GegevensdefinitieViewModels =>
			_gegevensdefinities.Select(_ => _.AlsViewModel());

		public IOpdrachtdefinitieViewModel AlsViewModel() {
			return new OpdrachtdefinitieViewModel {
				Id = Id,
				Naam = Naam,
				ResulterendeGebeurtenis = ResulterendeGebeurtenis.Id
			};
		}
	}
}