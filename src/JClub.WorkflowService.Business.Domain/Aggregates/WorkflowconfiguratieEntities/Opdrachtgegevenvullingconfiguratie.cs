using System;
using JClub.WorkflowService.Business.Contracts.ViewModels;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities {
	public sealed class Opdrachtgegevenvullingconfiguratie {

		private Opdrachtgegevenvullingconfiguratie() {}

		internal Opdrachtgegevenvullingconfiguratie(
			Opdrachtgegevendefinitie    opdrachtgegevendefinitie,
			Gebeurtenisgegevendefinitie gebeurtenisgegevendefinitie) : this(opdrachtgegevendefinitie) {

			Gebeurtenisgegevendefinitie = gebeurtenisgegevendefinitie;
		}

		internal Opdrachtgegevenvullingconfiguratie(
			Opdrachtgegevendefinitie opdrachtgegevendefinitie,
			string                   ingevuldeWaarde) : this(opdrachtgegevendefinitie) {

			IngevuldeWaarde = ingevuldeWaarde;
		}

		internal Opdrachtgegevenvullingconfiguratie(
			Opdrachtgegevendefinitie opdrachtgegevendefinitie) {

			Id                       = Guid.NewGuid();
			Opdrachtgegevendefinitie = opdrachtgegevendefinitie;
		}

		public Guid Id { get; private set; }
		public Opdrachtgegevendefinitie Opdrachtgegevendefinitie { get; private set; } = null!;

		public bool IsGevuld => Gebeurtenisgegevendefinitie != null ||
								IngevuldeWaarde != null;

		public Gebeurtenisgegevendefinitie? Gebeurtenisgegevendefinitie { get; private set; }
		public string? IngevuldeWaarde { get; private set; }

		public void VerwijderGebeurtenisgegevendefinitie() {
			Gebeurtenisgegevendefinitie = null;
		}

		public void RegistreerNieuweIngevuldeWaarde(string? nieuweIngevuldeWaarde) {

			if (nieuweIngevuldeWaarde != null) {
				VerwijderGebeurtenisgegevendefinitie();
			}

			IngevuldeWaarde = nieuweIngevuldeWaarde;
		}

		internal IOpdrachtgegevenvullingconfiguratieViewModel AlsViewModel() {
			return new OpdrachtgegevenvullingconfiguratieViewModel {
				OpdrachtgegevendefinitieId    = Opdrachtgegevendefinitie.Id,
				Opdrachtgegevennaam           = Opdrachtgegevendefinitie.Gegevennaam,
				Opdrachtgegevenwaardetype     = Opdrachtgegevendefinitie.Gegevenwaardetype,
				GebeurtenisgegevendefinitieId = Gebeurtenisgegevendefinitie?.Id,
				IngevuldeWaarde               = IngevuldeWaarde,
				IsVerplicht                   = Opdrachtgegevendefinitie.IsVerplicht
			};
		}
	}
}