using System;
using JClub.WorkflowService.Business.Contracts.ViewModels;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities {
	public sealed class Gebeurtenisgegevendefinitie {
		public Guid Id { get; set; }
		public Guid GebeurtenisdefinitieId { get; set; }
		public string Gegevennaam { get; set; } = string.Empty;
		public string Gegevenwaardetype { get; set; } = string.Empty;
		public string BronApiAdres { get; set; } = string.Empty;
		public string Weergaveformaat { get; set; } = string.Empty;

		public IGebeurtenisgegevendefinitieViewModel AlsViewModel() {
			return new GebeurtenisgegevendefinitieViewModel {
				Id = Id,
				Naam = Gegevennaam,
				Soort = Gegevenwaardetype
			};
		}
	}
}