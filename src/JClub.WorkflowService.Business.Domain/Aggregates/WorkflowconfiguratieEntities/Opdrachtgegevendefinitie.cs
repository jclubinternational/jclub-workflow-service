using System;
using JClub.WorkflowService.Business.Contracts.ViewModels;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities {
	public sealed class Opdrachtgegevendefinitie {
		public Guid Id { get; set; }
		public Guid OpdrachtdefinitieId { get; set; }
		public string Gegevennaam { get; set; } = string.Empty;
		public string Gegevenwaardetype { get; set; } = string.Empty;
		public bool IsVerplicht { get; set; }

		public IOpdrachtgegevendefinitieViewModel AlsViewModel() {
			return new OpdrachtgegevendefinitieViewModel {
				Id = Id,
				Naam = Gegevennaam,
				Soort = Gegevenwaardetype
			};
		}
	}
}