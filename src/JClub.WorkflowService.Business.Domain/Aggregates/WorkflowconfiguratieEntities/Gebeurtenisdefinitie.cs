using System;
using System.Collections.Generic;
using System.Linq;
using JClub.WorkflowService.Business.Contracts.ViewModels;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities {
	public sealed class Gebeurtenisdefinitie {

		private List<Gebeurtenisgegevendefinitie> _gegevensdefinities = new();

		private Gebeurtenisdefinitie() { }

		public Gebeurtenisdefinitie(
			string naam) {

			Id   = Guid.NewGuid();
			Naam = naam;
		}

		public Guid Id { get; private set; }
		public string Naam { get; private set; } = string.Empty;

		public void VoegGegevensdefinitiesToe(params Gebeurtenisgegevendefinitie[] definities) {
			_gegevensdefinities.AddRange(definities);
		}

		public IReadOnlyCollection<Gebeurtenisgegevendefinitie> Gegevensdefinities => _gegevensdefinities;

		public IGebeurtenisdefinitieViewModel AlsViewModel() {
			return new GebeurtenisdefinitieViewModel {
				Id = Id,Naam = Naam
			};
		}

		public IEnumerable<IGebeurtenisgegevendefinitieViewModel> GegevensdefinitieViewModels => 
			_gegevensdefinities.Select(_ => _.AlsViewModel());
	}
}