using System;
using System.Collections.Generic;
using System.Linq;
using JClub.WorkflowService.Business.Contracts.Enums;
using JClub.WorkflowService.Business.Contracts.ViewModels;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities {
	public sealed class WorkItemconfiguratie {

		private WorkItemconfiguratie() { }

		private readonly List<Opdrachtgegevenvullingconfiguratie> _opdrachtinformatievullingconfiguraties = new();

		internal WorkItemconfiguratie(
			WorkItemconfiguratie workItemconfiguratie,
			Gebeurtenisdefinitie reageerOpGebeurtenis,
			Opdrachtdefinitie    voerOpdrachtUit) :
			this(reageerOpGebeurtenis,voerOpdrachtUit) {

			VorigeWorkItemconfiguratieId = workItemconfiguratie.Id;
		}

		internal WorkItemconfiguratie(
			Gebeurtenisdefinitie reageerOpGebeurtenis,
			Opdrachtdefinitie    voerOpdrachtUit) {

			Id                   = Guid.NewGuid();
			Titel                = BepaaldWorkItemtitel(reageerOpGebeurtenis, voerOpdrachtUit);
			ReageerOpGebeurtenis = reageerOpGebeurtenis;
			VoerOpdrachtUit      = voerOpdrachtUit;
			Status               = WorkItemconfiguratieStatus.NietAlleOpdrachtgegevensZijnGevuld;

			StelOpdrachtgegevenconfiguratieOpnieuwIn();
		}

		private static string BepaaldWorkItemtitel(
			Gebeurtenisdefinitie reageerOpGebeurtenis,
			Opdrachtdefinitie    voerOpdrachtUit) {

			return $"Als {reageerOpGebeurtenis.Naam.ToLowerInvariant()} dan {voerOpdrachtUit.Naam.ToLowerInvariant()}";
		}

		public Guid Id { get; private set; }
		public Guid? VorigeWorkItemconfiguratieId { get; private set; }
		public string Titel { get; private set; } = string.Empty;
		public string Beschrijving { get; internal set; } = string.Empty;
		public Gebeurtenisdefinitie ReageerOpGebeurtenis { get; private set; } = null!;
		public Opdrachtdefinitie VoerOpdrachtUit { get; private set; } = null!;
		public IReadOnlyList<Opdrachtgegevenvullingconfiguratie> Opdrachtinformatievullingconfiguraties => _opdrachtinformatievullingconfiguraties;
		public WorkItemconfiguratieStatus Status { get; private set; }
		public IEnumerable<IOpdrachtgegevenvullingconfiguratieViewModel> OpdrachtinformatievullingconfiguratieViewModels => Opdrachtinformatievullingconfiguraties.Select(_ => _.AlsViewModel());

		internal void ReageerOpDezeGebeurtenis(
			Gebeurtenisdefinitie                             gebeurtenisdefinitie,
			IReadOnlyCollection<Gebeurtenisgegevendefinitie> beschikbareGebeurtenisgegevendefinities) {
			ReageerOpGebeurtenis = gebeurtenisdefinitie;
			Titel                = BepaaldWorkItemtitel(gebeurtenisdefinitie,VoerOpdrachtUit);

			HerevualeerOpdrachtgegevenvullingconfiguraties(beschikbareGebeurtenisgegevendefinities.Concat(ReageerOpGebeurtenis.Gegevensdefinities).ToArray());
		}

		public void HerevualeerOpdrachtgegevenvullingconfiguraties(IReadOnlyCollection<Gebeurtenisgegevendefinitie> beschikbareGebeurtenisgegevendefinities) {

			foreach (var opdrachtinformatievullingconfiguratie in Opdrachtinformatievullingconfiguraties) {

				if (!opdrachtinformatievullingconfiguratie.IsGevuld) {
					continue;
				}

				if (opdrachtinformatievullingconfiguratie.Gebeurtenisgegevendefinitie != null &&
					beschikbareGebeurtenisgegevendefinities
					.All(_ => _.Id != opdrachtinformatievullingconfiguratie.Gebeurtenisgegevendefinitie.Id)) {
					opdrachtinformatievullingconfiguratie.VerwijderGebeurtenisgegevendefinitie();
					Status = WorkItemconfiguratieStatus.NietAlleOpdrachtgegevensZijnGevuld;
					continue;
				}

				if (opdrachtinformatievullingconfiguratie.Gebeurtenisgegevendefinitie != null) {
					continue;
				}

				if (opdrachtinformatievullingconfiguratie.IngevuldeWaarde == null) {
					continue;
				}

				var placeholders = opdrachtinformatievullingconfiguratie.IngevuldeWaarde
																		.Split(new[] {
																			"$(",")"
																		},StringSplitOptions.RemoveEmptyEntries)
																		.Where(_ => _ == _.Trim());

				foreach (var placeholder in placeholders) {
					if (beschikbareGebeurtenisgegevendefinities.Any(_ => _.Gegevennaam == placeholder)) {
						continue;
					}

					var nieuweIngevuldeWaarde = opdrachtinformatievullingconfiguratie.IngevuldeWaarde.Replace($"$({placeholder})","?");
					opdrachtinformatievullingconfiguratie.RegistreerNieuweIngevuldeWaarde(nieuweIngevuldeWaarde);
					Status = WorkItemconfiguratieStatus.NietAlleOpdrachtgegevensZijnGevuld;
				}
			}
		}

		internal void VoerDezeOpdrachtUit(Opdrachtdefinitie opdrachtdefinitie) {
			VoerOpdrachtUit = opdrachtdefinitie;
			Titel           = BepaaldWorkItemtitel(ReageerOpGebeurtenis,VoerOpdrachtUit);

			StelOpdrachtgegevenconfiguratieOpnieuwIn();
		}

		private void StelOpdrachtgegevenconfiguratieOpnieuwIn() {
			var opdrachtgegevensconfiguraties = VoerOpdrachtUit.Gegevensdefinities
															   .Select(_ => new Opdrachtgegevenvullingconfiguratie(_))
															   .ToList();

			SpecificeerOpdrachtgegevensconfiguraties(opdrachtgegevensconfiguraties);
		}

		internal void SpecificeerOpdrachtgegevensconfiguraties(
			IReadOnlyList<Opdrachtgegevenvullingconfiguratie> opdrachtinformatievullingconfiguraties) {
			
			_opdrachtinformatievullingconfiguraties.RemoveAll(_ => true);
			_opdrachtinformatievullingconfiguraties.AddRange(opdrachtinformatievullingconfiguraties);

			if (_opdrachtinformatievullingconfiguraties.Any(_ => !_.IsGevuld && 
																 _.Opdrachtgegevendefinitie.IsVerplicht)) {
				Status = WorkItemconfiguratieStatus.NietAlleOpdrachtgegevensZijnGevuld;
				return;
			}

			var huidigeIds = new HashSet<Guid>(_opdrachtinformatievullingconfiguraties.Select(_ => _.Opdrachtgegevendefinitie.Id));

			Status = VoerOpdrachtUit.Gegevensdefinities.All(_ => !huidigeIds.Add(_.Id))
				? WorkItemconfiguratieStatus.KlaarOmUitgevoerdTeWorden
				: WorkItemconfiguratieStatus.NietAlleOpdrachtgegevensZijnGevuld;
		}

		internal void SchikVoor(
			WorkItemconfiguratie vorigeWorkItemconfiguratie) {

			VorigeWorkItemconfiguratieId = vorigeWorkItemconfiguratie.Id;
			ReageerOpGebeurtenis         = vorigeWorkItemconfiguratie.VoerOpdrachtUit.ResulterendeGebeurtenis;

			Titel = BepaaldWorkItemtitel(ReageerOpGebeurtenis,VoerOpdrachtUit);
		}

		public IWorkItemconfiguratieViewModel ToViewModel() =>
			new WorkItemconfiguratieViewModel {
				Id                           = Id,
				VorigeWorkItemconfiguratieId = VorigeWorkItemconfiguratieId,
				Titel                        = Titel,
				Beschrijving                 = Beschrijving,
				ReageerOpGebeurtenis         = ReageerOpGebeurtenis.Id,
				VoerOpdrachtUit              = VoerOpdrachtUit.Id,
				Status                       = Status
			};
	}
}