using System;
using System.Collections.Generic;
using System.Linq;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using JClub.WorkflowService.Business.Domain.ValueObjects;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowEntities {
	public sealed class Opdracht {

		private List<Opdrachtgegeven> _opdrachtgegevens = new();

		private Opdracht() { }

		internal Opdracht(
			WorkItemconfiguratie workItemconfiguratie) {

			Id        = Guid.NewGuid();
			Definitie = workItemconfiguratie.VoerOpdrachtUit;
			_opdrachtgegevens = workItemconfiguratie.Opdrachtinformatievullingconfiguraties
													.Select(_ =>
														_.Gebeurtenisgegevendefinitie == null
															? new Opdrachtgegeven(
																this,
																_.Opdrachtgegevendefinitie.Gegevennaam,
																_.Opdrachtgegevendefinitie.Gegevenwaardetype,
																_.Opdrachtgegevendefinitie.IsVerplicht,
																_.IngevuldeWaarde)
															: new Opdrachtgegeven(
																this,
																_.Opdrachtgegevendefinitie.Gegevennaam,
																_.Opdrachtgegevendefinitie.Gegevenwaardetype,
																_.Opdrachtgegevendefinitie.IsVerplicht,
																_.Gebeurtenisgegevendefinitie))
													.ToList();
		}

		public Guid Id { get; private set; }
		public Opdrachtdefinitie Definitie { get; private set; } = null!;
		public IReadOnlyCollection<Opdrachtgegeven> Opdrachtgegevens => _opdrachtgegevens;
		public bool ZijnGegevensCompleet { get; private set; }

		internal void AanvullenMetGebeurtenis(Gebeurtenis gebeurtenis) {

			if (ZijnGegevensCompleet) {
				return;
			}

			foreach (var opdrachtgegeven in Opdrachtgegevens) {
				opdrachtgegeven.UpdateWaarde(gebeurtenis);
			}

			ZijnGegevensCompleet = Opdrachtgegevens.Where(_ => _.IsVerplicht)
												   .All(_ => _.Waarde != null);
		}
	}
}