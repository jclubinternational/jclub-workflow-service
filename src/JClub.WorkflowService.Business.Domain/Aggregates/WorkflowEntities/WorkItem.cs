using System;
using JClub.WorkflowService.Business.Contracts.Enums;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowEntities {
	public class WorkItem {

		private WorkItem() { }

		public WorkItem(
			Workflow             workflow,
			WorkItemconfiguratie workItemconfiguratie,
			WorkItem             vorigeWorkItem) : 
			this(workflow, workItemconfiguratie) {
			VorigeWorkItemId = vorigeWorkItem.Id;
		}

		internal WorkItem(
			Workflow             workflow,
			WorkItemconfiguratie workItemconfiguratie) {

			Id                     = Guid.NewGuid();
			WorkItemconfiguratieId = workItemconfiguratie.Id;
			Titel                  = workItemconfiguratie.Titel;
			Status                 = WorkItemStatus.TeDoen;
			Workflow               = workflow;
			Opdracht               = new Opdracht(workItemconfiguratie);
			ReageerOpGebeurtenis   = workItemconfiguratie.ReageerOpGebeurtenis;
		}

		public Guid Id { get; private set; }
		public Guid WorkItemconfiguratieId { get; private set; }
		public Guid? VorigeWorkItemId { get; private set; }
		public string Titel { get; private set; } = string.Empty;
		public DateTime? OpgepaktOp { get; private set; }
		public DateTime? VoltooidOp { get; private set; }
		public WorkItemStatus Status { get; private set; }
		public Workflow Workflow { get; private set; } = null!;
		public Opdracht Opdracht { get; private set; } = null!;
		public Gebeurtenisdefinitie ReageerOpGebeurtenis { get; private set; } = null!;

		internal void Opgepakt() {
			Status     = WorkItemStatus.Opgepakt;
			OpgepaktOp = DateTime.UtcNow;
		}

		internal void Voltooid() {
			Status     = WorkItemStatus.Voltooid;
			VoltooidOp = DateTime.UtcNow;
		}
	}
}