using System;
using System.Linq;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using JClub.WorkflowService.Business.Domain.ValueObjects;

namespace JClub.WorkflowService.Business.Domain.Aggregates.WorkflowEntities {
	public sealed class Opdrachtgegeven {

		private Opdrachtgegeven() { }

		internal Opdrachtgegeven(
			Opdracht opdracht,
			string   naam,
			string   type,
			bool     isVerplicht,
			string?  waarde) {

			Id          = Guid.NewGuid();
			Naam        = naam;
			Type        = type;
			IsVerplicht = isVerplicht;
			Waarde      = waarde;
			Opdracht    = opdracht;
		}

		internal Opdrachtgegeven(
			Opdracht opdracht,
			string naam,
			string type,
			bool isVerplicht,
			Gebeurtenisgegevendefinitie? definitie) {

			Id                          = Guid.NewGuid();
			Naam                        = naam;
			Type                        = type;
			IsVerplicht                 = isVerplicht;
			Gebeurtenisgegevendefinitie = definitie;
			Opdracht                    = opdracht;
		}

		public Guid Id { get; private set; }
		public string Naam { get; private set; } = string.Empty;
		public string Type { get; private set; } = string.Empty;
		public bool IsVerplicht { get; private set; }
		public string? Waarde { get; private set; }
		public Gebeurtenisgegevendefinitie? Gebeurtenisgegevendefinitie { get; private set; }
		public Opdracht Opdracht { get; private set; } = null!;

		public void UpdateWaarde(Gebeurtenis gebeurtenis) {
			var gebeurtenisgegeven = gebeurtenis.Gebeurtenisgegevens
												.FirstOrDefault(_ => _.Naam == Gebeurtenisgegevendefinitie?.Gegevennaam);

			if (gebeurtenisgegeven != null) {
				Waarde = gebeurtenisgegeven.Waarde;
				return;
			}

			foreach (var gegeven in gebeurtenis.Gebeurtenisgegevens) {
				Waarde = Waarde?.Replace($"$({gegeven.Naam})", gegeven.Waarde);
			}
		}
	}
}