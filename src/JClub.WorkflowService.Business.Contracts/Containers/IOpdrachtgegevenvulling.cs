using System;

namespace JClub.WorkflowService.Business.Contracts.Containers {
	public interface IOpdrachtgegevenvulling {
		Guid OpdrachtgegevendefinitieId { get; }
		Guid? GebeurtenisgegevendefinitieId { get; }
		string? IngevuldeWaarde { get; }
	}
}