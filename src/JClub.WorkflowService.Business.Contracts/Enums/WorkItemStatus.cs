namespace JClub.WorkflowService.Business.Contracts.Enums {
	public enum WorkItemStatus {
		TeDoen,
		Opgepakt,
		Voltooid
	}
}