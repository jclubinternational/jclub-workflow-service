namespace JClub.WorkflowService.Business.Contracts.Enums {
	public enum WorkflowStatus {
		Gestart,
		Afgebroken,
		Verlopen,
		Voltooid
	}
}