namespace JClub.WorkflowService.Business.Contracts.Enums {
	public enum WorkItemconfiguratieStatus {
		NietAlleOpdrachtgegevensZijnGevuld,
		KlaarOmUitgevoerdTeWorden
	}
}