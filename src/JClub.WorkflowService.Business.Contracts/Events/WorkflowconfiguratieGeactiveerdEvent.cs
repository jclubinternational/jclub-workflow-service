using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class WorkflowconfiguratieGeactiveerdEvent : IEvent {

		private WorkflowconfiguratieGeactiveerdEvent() { }

		public WorkflowconfiguratieGeactiveerdEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}