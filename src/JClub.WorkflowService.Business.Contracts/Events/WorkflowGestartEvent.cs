using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class WorkflowGestartEvent : IEvent {

		private WorkflowGestartEvent() {
			
		}

		public WorkflowGestartEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}