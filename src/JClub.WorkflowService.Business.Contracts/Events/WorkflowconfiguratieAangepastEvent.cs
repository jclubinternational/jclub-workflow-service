using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class WorkflowconfiguratieAangepastEvent : IEvent {

		private WorkflowconfiguratieAangepastEvent() {}

		public WorkflowconfiguratieAangepastEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}