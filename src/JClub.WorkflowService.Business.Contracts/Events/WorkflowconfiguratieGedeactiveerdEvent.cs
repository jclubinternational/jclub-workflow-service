using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class WorkflowconfiguratieGedeactiveerdEvent : IEvent {

		private WorkflowconfiguratieGedeactiveerdEvent() { }

		public WorkflowconfiguratieGedeactiveerdEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}