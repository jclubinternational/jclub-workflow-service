using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class WorkflowconfiguratieActivatieGefaaldEvent : IEvent {
		private WorkflowconfiguratieActivatieGefaaldEvent() {
		
		}

		public WorkflowconfiguratieActivatieGefaaldEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}