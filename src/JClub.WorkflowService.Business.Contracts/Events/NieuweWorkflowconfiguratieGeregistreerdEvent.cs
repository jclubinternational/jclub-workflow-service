using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class NieuweWorkflowconfiguratieGeregistreerdEvent : IEvent {

		public NieuweWorkflowconfiguratieGeregistreerdEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}