using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class NieuweWorkItemconfiguratieGeregistreerdEvent : IEvent {
		
		public NieuweWorkItemconfiguratieGeregistreerdEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}