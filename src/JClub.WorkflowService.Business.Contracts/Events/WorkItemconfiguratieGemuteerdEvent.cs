using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class WorkItemconfiguratieGemuteerdEvent :
		IEvent {

		private WorkItemconfiguratieGemuteerdEvent() { }

		public WorkItemconfiguratieGemuteerdEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}