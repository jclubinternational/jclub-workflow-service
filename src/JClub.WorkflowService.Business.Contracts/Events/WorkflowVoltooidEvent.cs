using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class WorkflowVoltooidEvent : IEvent {

		private WorkflowVoltooidEvent() {
			
		}

		public WorkflowVoltooidEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}