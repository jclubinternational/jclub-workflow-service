using System;
using NServiceBus;

namespace JClub.WorkflowService.Business.Contracts.Events {
	public class WorkflowVoortgezetEvent : IEvent {

		private WorkflowVoortgezetEvent() {
			
		}

		public WorkflowVoortgezetEvent(Guid id) {
			Id = id;
		}

		public Guid Id { get; set; }
	}
}