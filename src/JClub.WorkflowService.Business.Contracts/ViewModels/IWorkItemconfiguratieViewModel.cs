using System;
using JClub.WorkflowService.Business.Contracts.Enums;

namespace JClub.WorkflowService.Business.Contracts.ViewModels {
	public interface IWorkItemconfiguratieViewModel {
		public Guid Id { get; }
		Guid? VorigeWorkItemconfiguratieId { get; }
		public string Titel { get; }
		public string Beschrijving { get; }
		public Guid ReageerOpGebeurtenis { get; }
		public Guid VoerOpdrachtUit { get; }
		public WorkItemconfiguratieStatus Status { get; }
	}

	public class WorkItemconfiguratieViewModel : IWorkItemconfiguratieViewModel {
		public Guid Id { get; set; }
		public Guid? VorigeWorkItemconfiguratieId { get; set; }
		public string Titel { get; set; } = string.Empty;
		public string Beschrijving { get; set; } = string.Empty;
		public Guid ReageerOpGebeurtenis { get; set; }
		public Guid VoerOpdrachtUit { get; set; }
		public WorkItemconfiguratieStatus Status { get; set; }
	}
}