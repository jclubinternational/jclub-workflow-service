using System;

namespace JClub.WorkflowService.Business.Contracts.ViewModels {
	public interface IOpdrachtdefinitieViewModel {
		Guid Id { get; }
		string Naam { get; }
		Guid ResulterendeGebeurtenis { get; }
	}

	public class OpdrachtdefinitieViewModel : IOpdrachtdefinitieViewModel {
		public Guid Id { get; set; }
		public string Naam { get; set; } = string.Empty;
		public Guid ResulterendeGebeurtenis { get; set; }
	}
}