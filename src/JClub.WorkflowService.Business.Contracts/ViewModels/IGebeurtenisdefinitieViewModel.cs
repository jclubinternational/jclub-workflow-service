using System;

namespace JClub.WorkflowService.Business.Contracts.ViewModels {
	public interface IGebeurtenisdefinitieViewModel {
		Guid Id { get; }
		string Naam { get; }
	}

	public class GebeurtenisdefinitieViewModel : IGebeurtenisdefinitieViewModel {
		public Guid Id { get; set; }
		public string Naam { get; set; } = string.Empty;
	}
}