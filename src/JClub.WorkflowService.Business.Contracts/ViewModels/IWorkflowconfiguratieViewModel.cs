using System;

namespace JClub.WorkflowService.Business.Contracts.ViewModels {
	public interface IWorkflowconfiguratieViewModel {
		public Guid Id { get; }
		public string Titel { get; }
		public string Beschrijving { get; }
		public Guid AangemaaktDoor { get; }
		public string? ReageertOpGebeurtenis { get; }
		public string? VervangenDoor { get; }
		public bool IsActief { get; }
	}

	public class WorkflowconfiguratieViewModel : IWorkflowconfiguratieViewModel {
		public Guid Id { get; set; }
		public string Titel { get; set; } = string.Empty;
		public string Beschrijving { get; set; } = string.Empty;
		public Guid AangemaaktDoor { get; set; }
		public string? ReageertOpGebeurtenis { get; set; }
		public string? VervangenDoor { get; set; }
		public bool IsActief { get; set; }
	}
}