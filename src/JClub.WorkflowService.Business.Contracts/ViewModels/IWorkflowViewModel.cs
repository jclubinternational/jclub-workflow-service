using System;
using JClub.WorkflowService.Business.Contracts.Enums;

namespace JClub.WorkflowService.Business.Contracts.ViewModels {
	public interface IWorkflowViewModel {
		public string Titel { get; }
		public DateTime GestartOp { get; }
		public DateTime? GestoptOp { get; }
		public WorkflowStatus Status { get; }
		public string HuidigeWorkItem { get; }
	}

	internal class WorkflowViewModel : IWorkflowViewModel {
		public string Titel { get; set; } = string.Empty;
		public DateTime GestartOp { get; set; }
		public DateTime? GestoptOp { get; set; }
		public WorkflowStatus Status { get; set; }
		public string HuidigeWorkItem { get; set; } = string.Empty;
	}
}