
using System;

namespace JClub.WorkflowService.Business.Contracts.ViewModels {
	public interface IOpdrachtgegevenvullingconfiguratieViewModel {

		Guid OpdrachtgegevendefinitieId { get; }
		string Opdrachtgegevennaam { get; }
		string Opdrachtgegevenwaardetype { get; }

		Guid? GebeurtenisgegevendefinitieId { get; }
		string? IngevuldeWaarde { get; }

		bool IsVerplicht { get; set; }
	}

	public class OpdrachtgegevenvullingconfiguratieViewModel : IOpdrachtgegevenvullingconfiguratieViewModel {
		public Guid OpdrachtgegevendefinitieId { get; set; }
		public string Opdrachtgegevennaam { get; set; } = string.Empty;
		public string Opdrachtgegevenwaardetype { get; set; } = string.Empty;

		public Guid? GebeurtenisgegevendefinitieId { get; set; }
		public string? IngevuldeWaarde { get; set; }

		public bool IsVerplicht { get; set; }
	}
}