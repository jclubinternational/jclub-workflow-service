using System;

namespace JClub.WorkflowService.Business.Contracts.ViewModels {
	public interface IGebeurtenisgegevendefinitieViewModel {
		Guid Id { get; }
		string Naam { get; }
		string Soort { get; }
	}

	public class GebeurtenisgegevendefinitieViewModel : IGebeurtenisgegevendefinitieViewModel {
		public Guid Id { get; set; }
		public string Naam { get; set; } = string.Empty;
		public string Soort { get; set; } = string.Empty;
	}
}