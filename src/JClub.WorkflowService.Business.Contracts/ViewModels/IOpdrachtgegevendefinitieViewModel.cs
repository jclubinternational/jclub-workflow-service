using System;

namespace JClub.WorkflowService.Business.Contracts.ViewModels {
	public interface IOpdrachtgegevendefinitieViewModel {
		Guid Id { get; }
		string Naam { get; }
		string Soort { get; }
	}

	internal class OpdrachtgegevendefinitieViewModel : IOpdrachtgegevendefinitieViewModel {
		public Guid Id { get; set; }
		public string Naam { get; set; } = string.Empty;
		public string Soort { get; set; } = string.Empty;
	}
}