using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("JClub.WorkflowService.Business.Domain"),
		   InternalsVisibleTo("JClub.WorkflowService.Connector"),
		   InternalsVisibleTo("JClub.WorkflowService.Requirements")]