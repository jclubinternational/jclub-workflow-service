namespace JClub.WorkflowService.MessageBus {
	public class MessageBusConstants {
		public static string EnclosedMessageTypes = "NServiceBus.EnclosedMessageTypes";
		public static string CorrelationId = "NServiceBus.CorrelationId";
	}
}