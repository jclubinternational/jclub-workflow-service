using NServiceBus;

namespace JClub.WorkflowService.MessageBus.Events {
	public class GenericEvent : IEvent {
		public string EventName { get; set; } = string.Empty;
		public string EventBody { get; set; } = string.Empty;
	}
}