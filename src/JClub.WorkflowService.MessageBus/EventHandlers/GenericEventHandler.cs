using System;
using System.Threading.Tasks;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Framework.Messages;
using JClub.WorkflowService.MessageBus.Events;
using JClub.WorkflowService.Messages.Commands;
using NServiceBus;

namespace JClub.WorkflowService.MessageBus.EventHandlers {

	public class GenericEventHandler :
		IHandleMessages<GenericEvent>,
		IHandleMessages<IWorkflowEvent> {
		private readonly IGetWorkflowconfiguraties _getWorkflowconfiguraties;
		private readonly IGetWorkflows _getWorkflows;

		public GenericEventHandler(
			IGetWorkflowconfiguraties getWorkflowconfiguraties,
			IGetWorkflows getWorkflows) {
			_getWorkflowconfiguraties = getWorkflowconfiguraties;
			_getWorkflows = getWorkflows;
		}

		public async Task Handle(GenericEvent message, IMessageHandlerContext context) {

			var correlationId = Guid.Parse(context.MessageHeaders[MessageBusConstants.CorrelationId]);

			var workflowconfiguraties = await _getWorkflowconfiguraties.ByGebeurtenisnaam(message.EventName);

			foreach (var workflowconfiguratie in workflowconfiguraties) {
				var command = new StartWorkflowCommand {
					CorrelationId = correlationId,
					WorkflowconfiguratieId = workflowconfiguratie.Id,
					EventName = message.EventName,
					EventBody = message.EventBody
				};

				try {
					await context.Send(command);
				} catch (Exception e) {
					Console.WriteLine(e);
					throw;
				}
			}

			var workflows = await _getWorkflows.ByGebeurtenisnaam(correlationId, message.EventName);

			foreach (var workflow in workflows) {
				var command = new VoortzettenWorkflowCommand {
					WorkflowId = workflow.Id,
					EventName = message.EventName,
					EventBody = message.EventBody
				};

				await context.SendLocal(command);
			}
		}

		public Task Handle(IWorkflowEvent message, IMessageHandlerContext context) {

			throw new NotImplementedException();
		}
	}
}