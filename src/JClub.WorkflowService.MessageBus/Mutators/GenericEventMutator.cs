using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JClub.WorkflowService.MessageBus.Events;
using Newtonsoft.Json;
using NServiceBus.MessageMutator;

namespace JClub.WorkflowService.MessageBus.Mutators {
	public class GenericEventMutator : IMutateIncomingTransportMessages {

		public Task MutateIncoming(MutateIncomingTransportMessageContext context) {

			if (!context.Headers.TryGetValue(MessageBusConstants.EnclosedMessageTypes, out var messageTypes) || !messageTypes.Contains("IWorkflowEvent")) {
				return Task.CompletedTask;
			}

			var originalBody = Encoding.UTF8.GetString(context.Body);
			var genericEvent = new GenericEvent {
				EventName = context.Headers[MessageBusConstants.EnclosedMessageTypes]
					.Split(";")[0]
					.Split(".").Last(),
				EventBody = originalBody
			};

			var json = JsonConvert.SerializeObject(genericEvent);
			var newBody = Encoding.UTF8.GetBytes(json);

			context.Headers[MessageBusConstants.EnclosedMessageTypes] = typeof(GenericEvent).AssemblyQualifiedName;
			context.Body = newBody;

			return Task.CompletedTask;
		}
	}
}