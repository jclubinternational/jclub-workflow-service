using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class VerstuurOpdrachtCommand : ICommand {
		public string Adres { get; set; } = string.Empty;
		public string Naam { get; set; } = string.Empty;
		public string Gegevens { get; set; } = string.Empty;
	}
}