using System;
using JClub.Shared.MessageBus.SignalR;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {

	public class RegistreerNieuweWorkflowconfiguratieCommand : 
		ICommand {
		public string Titel { get; set; } = string.Empty;
		public string Beschrijving { get; set; } = string.Empty;
		public Guid AangemaaktDoor { get; set; }
	}

	[SignalrBoundCommand(CommandName)]
	public class RegistreerNieuweWorkflowconfiguratieUiCommand : ICommand {
		public const string CommandName = "Workflow_RegistreerNieuweWorkflowconfiguratie";
		public RegistreerNieuweWorkflowconfiguratieCommand Data { get; set; } = null!;
	}
}