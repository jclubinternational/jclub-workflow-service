using System;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class VoortzettenWorkflowCommand :
		ICommand {
		public Guid WorkflowId { get; set; }
		public string EventName { get; set; } = string.Empty;
		public string EventBody { get; set; } = string.Empty;
	}
}