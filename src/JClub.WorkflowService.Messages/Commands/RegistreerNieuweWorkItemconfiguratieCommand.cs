using System;
using JClub.Shared.MessageBus.SignalR;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class RegistreerNieuweWorkItemconfiguratieCommand :
		ICommand {
		public Guid WorkflowconfiguratieId { get; set; }
		public Guid? VorigeWorkItemconfiguratieId { get; set; }
		public string Beschrijving { get; set; } = string.Empty;
		public Guid ReageerOpGebeurtenis { get; set; }
		public Guid VoerOpdrachtUit { get; set; }
	}

	[SignalrBoundCommand(CommandName)]
	public class RegistreerNieuweWorkItemconfiguratieUiCommand : ICommand {
		public const string CommandName = "Workflow_RegistreerNieuweWorkItemconfiguratie";
		public RegistreerNieuweWorkItemconfiguratieCommand Data { get; set; } = null!;
	}
}