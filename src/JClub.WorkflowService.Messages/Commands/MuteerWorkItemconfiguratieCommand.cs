using System;
using JClub.Shared.MessageBus.SignalR;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class MuteerWorkItemconfiguratieCommand : 
		ICommand {
		public Guid Id { get; set; }
		public string? Beschrijving { get; set; }
		public Guid? ReageerOpGebeurtenis { get; set; }
		public Guid? VoerOpdrachtUit { get; set; }
	}

	[SignalrBoundCommand(CommandName)]
	public class MuteerWorkItemconfiguratieUiCommand : 
		ICommand {
		
		public const string CommandName = "Workflow_MuteerWorkItemconfiguratie";

		public MuteerWorkItemconfiguratieCommand Data { get; set; } = null!;
	}
}