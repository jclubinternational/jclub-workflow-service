using System;
using JClub.Shared.MessageBus.SignalR;
using JClub.WorkflowService.Business.Contracts.Containers;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class RegistreerNieuweOpdrachtgegevensvullingconfiguratieCommand : 
		ICommand {
		public Guid WorkItemconfiguratieId { get; set; }
		public Opdrachtgegevenvulling[] Opdrachtgegevenvullingen { get; set; } = null!;
	}

	public class Opdrachtgegevenvulling : IOpdrachtgegevenvulling {
		public Guid OpdrachtgegevendefinitieId { get; set; }
		public Guid? GebeurtenisgegevendefinitieId { get; set; }
		public string? IngevuldeWaarde { get; set; }
	}

	[SignalrBoundCommand(CommandName)]
	public class RegistreerNieuweOpdrachtgegevensvullingconfiguratieUiCommand :
		ICommand {
		public const string CommandName = "Workflow_RegistreerNieuweOpdrachtgegevensvullingconfiguratie";

		public RegistreerNieuweOpdrachtgegevensvullingconfiguratieCommand Data { get; set; } = null!;
	}
}