using System;
using JClub.Shared.MessageBus.SignalR;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class DeactiveerWorkflowconfiguratieCommand :
		ICommand {

		public Guid Id { get; set; }
	}

	[SignalrBoundCommand(CommandName)]
	public class DeactiveerWorkflowconfiguratieUiCommand :
		ICommand {
		public const string CommandName = "Workflow_DeactiveerWorkflowconfiguratie";

		public DeactiveerWorkflowconfiguratieCommand Data { get; set; } = null!;
	}
}