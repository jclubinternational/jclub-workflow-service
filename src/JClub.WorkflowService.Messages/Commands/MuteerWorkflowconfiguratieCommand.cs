using System;
using JClub.Shared.MessageBus.SignalR;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class MuteerWorkflowconfiguratieCommand :
		ICommand {
		public Guid Id { get; set; }
		public string Titel { get; set; } = string.Empty;
		public string Beschrijving { get; set; } = string.Empty;
	}

	[SignalrBoundCommand(CommandName)]
	public class MuteerWorkflowconfiguratieUiCommand :
		ICommand {
		
		public const string CommandName = "Workflow_MuteerWorkflowconfiguratie";

		public MuteerWorkflowconfiguratieCommand Data { get; set; } = null!;

	}
}