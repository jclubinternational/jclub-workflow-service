using System;
using JClub.Shared.MessageBus.SignalR;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class ActiveerWorkflowconfiguratieCommand :
		ICommand {

		public Guid Id { get; set; }
	}

	[SignalrBoundCommand(CommandName)]
	public class ActiveerWorkflowconfiguratieUiCommand :
		ICommand {
		public const string CommandName = "Workflow_ActiveerWorkflowconfiguratie";

		public ActiveerWorkflowconfiguratieCommand Data { get; set; } = null!;
	}
}