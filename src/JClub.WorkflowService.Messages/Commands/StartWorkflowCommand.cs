using System;
using NServiceBus;

namespace JClub.WorkflowService.Messages.Commands {
	public class StartWorkflowCommand : 
		ICommand {

		public Guid CorrelationId { get; set; }
		public Guid WorkflowconfiguratieId { get; set; }
		public string EventName { get; set; } = string.Empty;
		public string EventBody { get; set; } = string.Empty;
	}
}