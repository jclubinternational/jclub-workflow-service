
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Messages.Commands;

namespace JClub.WorkflowService.Connector {
	public interface IWorkflowServiceConnector {

		Task<IReadOnlyCollection<WorkflowconfiguratieViewModel>> GetWorkflowconfiguraties();
		Task<WorkflowconfiguratieViewModel> GetWorkflowconfiguratieById(Guid id);

		Task<IReadOnlyCollection<WorkItemconfiguratieViewModel>> GetWorkItemconfiguraties(Guid workflowconfiguratieId);
		Task<WorkItemconfiguratieViewModel> GetWorkItemconfiguratieById(Guid workflowconfiguratieId,Guid id);

		Task<IWorkflowViewModel> GetWorkflowById(Guid id);

		Task<IReadOnlyCollection<GebeurtenisdefinitieViewModel>> GetGebeurtenisdefinities();
		Task<IReadOnlyCollection<GebeurtenisgegevendefinitieViewModel>> GetGebeurtenisgegevendefinities(Guid workItemconfiguratieId);
		Task<IReadOnlyCollection<OpdrachtdefinitieViewModel>> GetOpdrachtdefinities();

		Task<GebeurtenisdefinitieViewModel> GetGebeurtenisdefinitieById(Guid id);

		Task<OpdrachtdefinitieViewModel> GetOpdrachtdefinitieById(Guid id);

		Task<IReadOnlyCollection<OpdrachtgegevenvullingconfiguratieViewModel>> GetOpdrachtgegevenvulingconfiguraties(Guid workItemconfiguratieId);

		Task RegistreerNieuweWorkItemconfiguratie(RegistreerNieuweWorkItemconfiguratieCommand command);
	}
}
