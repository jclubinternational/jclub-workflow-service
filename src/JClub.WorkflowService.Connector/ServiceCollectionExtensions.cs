﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace JClub.WorkflowService.Connector {
	public static class ServiceCollectionExtensions {
		public static IServiceCollection AddWorkflowServiceConnector(this IServiceCollection services, Action<WorkflowServiceOptions> opts) {
			if (opts == null) throw new ArgumentNullException(nameof(opts));

			var options = new WorkflowServiceOptions();
			opts.Invoke(options);
			services.Configure(opts);

			services.AddHttpClient<IWorkflowServiceConnector, WorkflowServiceConnector>(client => { client.BaseAddress = new Uri(options.ApiUrl); });

			return services;
		}
	}
}
