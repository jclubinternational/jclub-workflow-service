using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using JClub.Shared.Utils;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Messages.Commands;

namespace JClub.WorkflowService.Connector {
	public class WorkflowServiceConnector : IWorkflowServiceConnector {
		private readonly HttpClient _httpClient;

		public WorkflowServiceConnector(HttpClient httpClient) {
			_httpClient = httpClient;
		}

		public async Task<IReadOnlyCollection<WorkflowconfiguratieViewModel>> GetWorkflowconfiguraties() {
			var result =  await _httpClient.GetJsonAsync<WorkflowconfiguratieViewModel[]>("Workflowconfiguratie");
			return result;
		}

		public async Task<WorkflowconfiguratieViewModel> GetWorkflowconfiguratieById(Guid id) {
			return await _httpClient.GetJsonAsync<WorkflowconfiguratieViewModel>($"Workflowconfiguratie/{id}");
		}

		public async Task<IReadOnlyCollection<WorkItemconfiguratieViewModel>> GetWorkItemconfiguraties(Guid workflowconfiguratieId) {
			var result = await _httpClient.GetJsonAsync<WorkItemconfiguratieViewModel[]>($"Workflowconfiguratie/{workflowconfiguratieId}/WorkItemconfiguratie");
			return result;
		}

		public async Task<WorkItemconfiguratieViewModel> GetWorkItemconfiguratieById(Guid workflowconfiguratieId, Guid id) {
			return await _httpClient.GetJsonAsync<WorkItemconfiguratieViewModel>($"Workflowconfiguratie/{workflowconfiguratieId}/WorkItemconfiguratie/{id}");
		}

		public async Task<IWorkflowViewModel> GetWorkflowById(Guid id) {
			return await _httpClient.GetJsonAsync<WorkflowViewModel>($"Workflow/{id}");
		}

		public async Task<IReadOnlyCollection<GebeurtenisdefinitieViewModel>> GetGebeurtenisdefinities() {
			var result = await _httpClient.GetJsonAsync<GebeurtenisdefinitieViewModel[]>("gebeurtenisdefinitie");
			return result;
		}

		public async Task<IReadOnlyCollection<GebeurtenisgegevendefinitieViewModel>> GetGebeurtenisgegevendefinities(Guid workItemconfiguratieId) {
			return await _httpClient.GetJsonAsync<GebeurtenisgegevendefinitieViewModel[]>($"WorkItemconfiguratie/{workItemconfiguratieId}/gebeurtenisdefinitiegegeven");
		}

		public async Task<IReadOnlyCollection<OpdrachtdefinitieViewModel>> GetOpdrachtdefinities() {
			var result = await _httpClient.GetJsonAsync<OpdrachtdefinitieViewModel[]>("opdrachtdefinitie");
			return result;
		}

		public async Task<GebeurtenisdefinitieViewModel> GetGebeurtenisdefinitieById(Guid id) {
			var result = await _httpClient.GetJsonAsync<GebeurtenisdefinitieViewModel>($"gebeurtenisdefinitie/{id}");
			return result;
		}

		public async Task<OpdrachtdefinitieViewModel> GetOpdrachtdefinitieById(Guid id) {
			var result = await _httpClient.GetJsonAsync<OpdrachtdefinitieViewModel>($"opdrachtdefinitie/{id}");
			return result;
		}

		public async Task<IReadOnlyCollection<OpdrachtgegevenvullingconfiguratieViewModel>> GetOpdrachtgegevenvulingconfiguraties(
			Guid workItemconfiguratieId) {
			var result = await _httpClient.GetJsonAsync<OpdrachtgegevenvullingconfiguratieViewModel[]>(
				$"WorkItemconfiguratie/{workItemconfiguratieId}/opdrachtgegevenvullingconfiguratie");

			return result;
		}

		public Task RegistreerNieuweWorkItemconfiguratie(RegistreerNieuweWorkItemconfiguratieCommand command) {
			return _httpClient.PostJsonAsync("/WorkItemconfiguratie/registreerNieuweWorkItemconfiguratiecommand", command);
		}
	}
}
