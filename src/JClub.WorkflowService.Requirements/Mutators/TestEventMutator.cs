using System;
using System.Text;
using System.Threading.Tasks;
using JClub.WorkflowService.MessageBus;
using Newtonsoft.Json;
using NServiceBus.MessageMutator;

namespace JClub.WorkflowService.Requirements.Mutators {
	public class TestEventMutator : IMutateOutgoingTransportMessages {
		public Task MutateOutgoing(MutateOutgoingTransportMessageContext context) {

			if (!context.OutgoingHeaders.TryGetValue(MessageBusConstants.EnclosedMessageTypes, out var messageTypes) || !messageTypes.Contains("TestEvent")) {
				return Task.CompletedTask;
			}

			var originalBody = Encoding.UTF8.GetString(context.OutgoingBody);

			try {
				var testEvent = JsonConvert.DeserializeObject<TestEvent>(originalBody);
				context.OutgoingHeaders["NServiceBus.CorrelationId"] = testEvent!.CorrelationId;
				context.OutgoingBody = Encoding.UTF8.GetBytes(testEvent.Body);
				context.OutgoingHeaders[MessageBusConstants.EnclosedMessageTypes] = $"Test.{testEvent.Name}; {messageTypes}";
			} catch (Exception e) {
				Console.WriteLine(e);
				throw;
			}


			return Task.CompletedTask;
		}
	}
}