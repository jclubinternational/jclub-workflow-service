using JClub.WorkflowService.Framework.Messages;

namespace JClub.WorkflowService.Requirements.Mutators {
	public class TestEvent : IWorkflowEvent {
		public string CorrelationId { get; set; } = string.Empty;
		public string Name { get; set; } = string.Empty;
		public string Body { get; set; } = string.Empty;
	}
}