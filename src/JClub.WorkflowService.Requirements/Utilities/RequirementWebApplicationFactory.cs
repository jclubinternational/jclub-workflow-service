using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Api;
using JClub.WorkflowService.Connector;
using JClub.WorkflowService.Data;
using JClub.WorkflowService.MessageBus.Mutators;
using JClub.WorkflowService.Messages.Commands;
using JClub.WorkflowService.Requirements.Handlers;
using JClub.WorkflowService.Requirements.Mutators;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using NServiceBus;
using NServiceBus.Extensions.IntegrationTesting;
using NServiceBus.MessageMutator;
using Serilog;
using Serilog.Events;

namespace JClub.WorkflowService.Requirements.Utilities {
	public class RequirementWebApplicationFactory : WebApplicationFactory<Startup>,
		IDbContextEventPublisher {

		private readonly SqliteConnection _connection = new("DataSource=:memory:");
		private IWorkflowServiceConnector? _connector;
		private event EventHandler<(ICommand, Exception?)>? _commandReceived;
		private event EventHandler<(IEvent, Exception?)>? _eventReceived;
		private bool _withTimeout = false;

		private readonly IDictionary<string, string> _receivedMessageBodies = new Dictionary<string, string>();
		public readonly string EndPointName = $"HostApplicationFactoryTests {Guid.NewGuid()}";

		public IWorkflowServiceConnector GetConnector() {
			return _connector ??= new WorkflowServiceConnector(CreateClient());
		}

		public TMessage? GetMessageOfType<TMessage>() {
			if(_receivedMessageBodies.TryGetValue(typeof(TMessage).Name, out var messageBody)) {
				return JsonConvert.DeserializeObject<TMessage>(messageBody);
			}

			return default;
		}

		public Task<TEvent> ListenToEvent<TEvent>() {
			_connector = GetConnector();
			var tcs = new TaskCompletionSource<TEvent>();

			void On_eventReceived(object? sender, (IEvent @event, Exception? exception) _) {

				if (_.exception != null) {
					tcs.SetException(_.exception);
					_eventReceived -= On_eventReceived;
					return;
				}

				if (_.@event is TEvent expectedEvent) {

					var body = JsonConvert.SerializeObject(_.@event);
					_receivedMessageBodies[typeof(TEvent).Name] = body;

					tcs.SetResult(expectedEvent);
					_eventReceived -= On_eventReceived;
				}
			}

			_eventReceived += On_eventReceived;

			return tcs.Task;
		}

		public Task<TEvent> SendEventAndAwaitEvent<T, TEvent>(T @event)
			where T : IEvent {
			_connector = GetConnector();
			var tcs = new TaskCompletionSource<TEvent>();

			void On_eventReceived(object? sender, (IEvent @event, Exception? exception) _) {

				if (_.exception != null) {
					tcs.SetException(_.exception);
					_eventReceived -= On_eventReceived;
					return;
				}
				
				if (_.@event is TEvent expectedEvent) {
					var body = JsonConvert.SerializeObject(_.@event);
					_receivedMessageBodies[typeof(TEvent).Name] = body;

					tcs.SetResult(expectedEvent);
					_eventReceived -= On_eventReceived;
				}
			}

			var _ = _withTimeout
				? Task.Delay(new TimeSpan(0, 0, 5)).ContinueWith(_ => tcs.SetResult(default!))
				: Task.CompletedTask;

			_eventReceived += On_eventReceived;

			var session = Services.GetRequiredService<IMessageSession>();
			var task = session.Publish(@event);

			return tcs.Task;
		}

		public async Task<TCommand> SendEventAndAwaitCommand<T, TCommand>(T @event)
			where T : IEvent {
			_connector = GetConnector();
			var tcs = new TaskCompletionSource<TCommand>();

			void On_commandReceived(object? s, (ICommand command, Exception? exception) _) {
				if (_.exception != null) {
					tcs.SetException(_.exception);
					_commandReceived -= On_commandReceived;
					return;
				}

				if (_.command is TCommand expectedCommand) {
					var body = JsonConvert.SerializeObject(_.command);
					_receivedMessageBodies[typeof(TCommand).Name] = body;

					tcs.SetResult(expectedCommand);
					_commandReceived -= On_commandReceived;
				}
			}

			var _ = _withTimeout
				? Task.Delay(new TimeSpan(0, 0, 5)).ContinueWith(_ => tcs.SetResult(default!))
				: Task.CompletedTask;

			_commandReceived += On_commandReceived;

			var session = Services.GetRequiredService<IMessageSession>();
			await session.Publish(@event);

			return await tcs.Task;
		}

		public async Task<TEvent> SendCommandAndAwaitEvent<T, TEvent>(T command)
			where T : ICommand {
			_connector = GetConnector();
			var tcs = new TaskCompletionSource<TEvent>();
			
			void On_eventReceived(object? s, (IEvent @event, Exception? exception) _) {

				if (_.exception != null) {
					tcs.SetException(_.exception);
					_eventReceived -= On_eventReceived;
					return;
				}

				if (_.@event is TEvent expectedEvent) {
					tcs.SetResult(expectedEvent);
					_eventReceived -= On_eventReceived;
				}
			}

			var _ = _withTimeout
				? Task.Delay(new TimeSpan(0, 0, 5)).ContinueWith(_ => {
					if (tcs.Task.Status != TaskStatus.RanToCompletion) {
						tcs.SetResult(default!);
					}
				})
				: Task.CompletedTask;

			_eventReceived += On_eventReceived;

			var session = Services.GetService<IMessageSession>();
			await session.Send(command);

			return await tcs.Task;
		}

		public async Task<IEvent> SendCommandAndAwaitEvents<T, TEvent1, TEvent2>(T command)
			where T : ICommand
			where TEvent1: IEvent
			where TEvent2 : IEvent
		{
			_connector = GetConnector();
			var tcs = new TaskCompletionSource<IEvent>();

			void On_eventReceived(object? s,(IEvent @event, Exception? exception) _) {

				if (_.exception != null) {
					tcs.SetException(_.exception);
					_eventReceived -= On_eventReceived;
					return;
				}

				if (_.@event is TEvent1 expectedEvent1) {
					tcs.SetResult(expectedEvent1);
					_eventReceived -= On_eventReceived;
				}

				if (_.@event is TEvent2 expectedEvent2) {
					tcs.SetResult(expectedEvent2);
					_eventReceived -= On_eventReceived;
				}
			}

			var _ = _withTimeout
				? Task.Delay(new TimeSpan(0, 0, 5)).ContinueWith(_ => {
					if (tcs.Task.Status != TaskStatus.RanToCompletion) {
						tcs.SetResult(default!);
					}
				})
				: Task.CompletedTask;

			_eventReceived += On_eventReceived;

			var session = Services.GetService<IMessageSession>();
			await session.Send(command);

			return await tcs.Task;
		}


		protected override void ConfigureWebHost(IWebHostBuilder builder) {

			Log.Logger = new LoggerConfiguration()
				.MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Verbose)
				.Enrich.FromLogContext()
				.WriteTo.Console()
				.CreateLogger();

			_connection.Open();

			builder.ConfigureServices(services => {

				services.AddSingleton<IDictionary<string, string>>(new Dictionary<string, string>());
				services.AddSingleton<Action<ICommand, IMessageHandlerContext>>((c, mhc) => _commandReceived?.Invoke(this, (c, null)));
				services.AddSingleton<Action<IEvent, IMessageHandlerContext>>((c, mhc) => _eventReceived?.Invoke(this, (c, null)));
				services.Remove(services.Single(x => x.ServiceType == typeof(IDbContextEventPublisher)));
				services.TryAddEnumerable(ServiceDescriptor.Singleton<IDbContextEventPublisher>(this));

				//Unregister existing database service(SQL Server).
				var descriptor = services.SingleOrDefault(
					d => d.ServiceType ==
						 typeof(DbContextOptions<WorkflowServiceDataContext>));

				if (descriptor != null) services.Remove(descriptor);

				//services.AddDbContext<WorkflowServiceDataContext>(options => options.UseSqlServer("Data Source=.,11433;Integrated Security=False;Initial Catalog=JCLUB_WORKFLOWSERVICE;User ID=sa;Password=saSA123!;MultipleActiveResultSets=True;").EnableSensitiveDataLogging());

				// Register new database service (SQLite In-Memory)
				services.AddDbContext<WorkflowServiceDataContext>(options => options.UseSqlite(_connection).EnableSensitiveDataLogging());

				var sp = services.BuildServiceProvider();

				using var scope = sp.CreateScope();

				var scopedServices = scope.ServiceProvider;

				// try to receive context with inmemory provider:
				var db = scopedServices.GetRequiredService<WorkflowServiceDataContext>();

				// Ensure the database is created.
				db.Database.EnsureCreated();
			});

			base.ConfigureWebHost(builder);
		}

		protected override IHostBuilder CreateHostBuilder() =>
			Host.CreateDefaultBuilder()
				.UseNServiceBus(ctxt => {
					
					var endpoint = new EndpointConfiguration(EndPointName);
					endpoint.Recoverability().CustomPolicy((config, context) => {
						_eventReceived?.Invoke(this, (null!, context.Exception));
						_commandReceived?.Invoke(this, (null!, context.Exception));

						return DefaultRecoverabilityPolicy.Invoke(config, context);
					});
					var serialization = endpoint.UseSerialization<NewtonsoftSerializer>();
					serialization.WriterCreator(_ => {
						var streamWriter = new StreamWriter(_, new UTF8Encoding(false));
						return new JsonTextWriter(streamWriter);
					});
					endpoint.RegisterMessageMutator(new GenericEventMutator());
					endpoint.RegisterMessageMutator(new TestEventMutator());

					// Set up NServiceBus with testing-friendly defaults
					endpoint.ConfigureTestEndpoint(t => {
						t.Routing().RouteToEndpoint(typeof(RegistreerNieuweWorkflowconfiguratieCommand).Assembly, EndPointName);
						//t.Routing().RouteToEndpoint(typeof(MuteerWorkflowconfiguratieCommand), EndPointName);
						//t.Routing().RouteToEndpoint(typeof(RegistreerNieuweWorkItemconfiguratieCommand), EndPointName);
						//t.Routing().RouteToEndpoint(typeof(RegistreerNieuweOpdrachtgegevensvullingconfiguratieCommand), EndPointName);
						//t.Routing().RouteToEndpoint(typeof(ActiveerWorkflowconfiguratieCommand), EndPointName);
						t.Routing().RouteToEndpoint(typeof(TestEvent), EndPointName);
						t.Routing().RouteToEndpoint(typeof(StartWorkflowCommand), EndPointName);
						t.Routing().RouteToEndpoint(typeof(CommandHandler).Assembly, EndPointName);
					});

					// Set up any of your other configuration here
					return endpoint;
				})
				.UseSerilog()
				.ConfigureWebHostDefaults(b => b.UseStartup<Startup>());

		protected override void Dispose(bool disposing) {
			_connection.Dispose();

			base.Dispose(disposing);
		}

		public Task PublishEvent(IEvent @event) {
			_eventReceived?.Invoke(this, (@event, null));
			return Task.CompletedTask;
		}
	}
}