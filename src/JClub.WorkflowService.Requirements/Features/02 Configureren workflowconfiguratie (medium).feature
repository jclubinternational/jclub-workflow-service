﻿Functionaliteit: 02 Configureren workflowconfiguratie (medium)

Achtergrond: Er zijn gebruikers, gebeurtenissen, opdrachten en er is een basis configuratie
Gegeven de gebruiker is 'Jantine Braas'
En ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Workflow voor als de order niet geleverd is                      |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
En de gebeurtenis 'Order niet geleverd' aanwezig is met de volgende gegevens:
| Gegeven     | Type   |
| Ordernummer | Nummer |
En de gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven | Type |
| Taak    | Taak |
En de opdracht 'Verstuur taak' met als resultaat gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven              | Type      |
| Taal                 | Taal      |
| Titel                | Tekst     |
| Korte omschrijving   | Tekst     |
| Lange omschrijving   | Tekst     |
| Toewijzing vestiging | Vestiging |
| Toewijzing rol       | Rol       |
En ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
En de volgende globale gegevens bestaan:
| Gegeven      | Type      |
| Nederlands   | Taal      |
| Hoofdkantoor | Vestiging |
| CM           | Rol       |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |


Scenario: Registreer nieuwe navolgende work-itemconfiguratie
Als ik een navolgende work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde         |
| Beschrijving           |                |
| Reageer op gebeurtenis | Taak verstuurd |
| Voer opdracht uit      | Verstuur taak  |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |
Dan heeft deze nieuwe work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                               |
| Titel                  | Als taak verstuurd dan verstuur taak |
| Beschrijving           |                                      |
| Reageer op gebeurtenis | Taak verstuurd                       |
| Voer opdracht uit      | Verstuur taak                        |
| Status                 | Klaar om uitgevoerd te worden        |

Scenario: Registreer nieuwe voorgaande work-itemconfiguratie
Als ik een voorgaande work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |
Dan heeft deze nieuwe work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                                    |
| Titel                  | Als order niet geleverd dan verstuur taak |
| Beschrijving           |                                           |
| Reageer op gebeurtenis | Order niet geleverd                       |
| Voer opdracht uit      | Verstuur taak                             |
| Status                 | Klaar om uitgevoerd te worden             |
Dan heeft de reeds bestaande work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                               |
| Titel                  | Als taak verstuurd dan verstuur taak |
| Beschrijving           |                                      |
| Reageer op gebeurtenis | Taak verstuurd                       |
| Voer opdracht uit      | Verstuur taak                        |
| Status                 | Klaar om uitgevoerd te worden        |