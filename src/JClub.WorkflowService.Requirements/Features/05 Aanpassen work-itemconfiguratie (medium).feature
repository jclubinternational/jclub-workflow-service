﻿Functionaliteit: 05 Aanpassen work-itemconfiguratie (medium)

Achtergrond: 
Gegeven de gebruiker is 'Jantine Braas'

En de gebeurtenis 'Order niet geleverd' aanwezig is met de volgende gegevens:
| Gegeven     | Type   |
| Ordernummer | Nummer |
En de gebeurtenis 'Ticket bij vervoerder aangemaakt' aanwezig is met de volgende gegevens:
| Gegeven          | Type  |
| Ticketreferentie | Tekst |
En de opdracht 'Maak ticket aan bij vervoerder' met als resultaat gebeurtenis 'Ticket bij vervoerder aangemaakt' aanwezig is met de volgende gegevens:
| Gegeven           | Type   |
| Ordernummer       | Nummer |
| BegeleidendeTekst | Tekst  |
En de gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven | Type |
| Taak    | Taak |
En de opdracht 'Verstuur taak' met als resultaat gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven              | Type      |
| Taal                 | Taal      |
| Titel                | Tekst     |
| Korte omschrijving   | Tekst     |
| Lange omschrijving   | Tekst     |
| Toewijzing vestiging | Vestiging |
| Toewijzing rol       | Rol       |
En de volgende globale gegevens bestaan:
| Gegeven      | Type      |
| Nederlands   | Taal      |
| Hoofdkantoor | Vestiging |
| CM           | Rol       |
En ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Order niet geleverd                                              |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
En ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |

Scenario: Aanpassen op welke gebeurtenis een volgende work-itemconfiguratie moet reageren
Gegeven ik een navolgende work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde                         |
| Beschrijving           |                                |
| Reageer op gebeurtenis | Taak verstuurd                 |
| Voer opdracht uit      | Maak ticket aan bij vervoerder |
En ik voor de opdracht 'Maak ticket aan bij vervoerder' de opdrachtgegevens invul op de volgende wijze:
| Gegeven           | Waarde                                                    | Bron                |
| Ordernummer       | Ordernummer                                               | Order niet geleverd |
| BegeleidendeTekst | Order met $(Ordernummer) niet geleverd, graag onderzoeken | Ingevuld            |
Als ik de opdracht dat de vorige work-itemconfiguratie moet uitvoeren verander naar 'Maak ticket aan bij vervoerder'
Dan heeft deze nieuwe work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                                                                  |
| Titel                  | Als ticket bij vervoerder aangemaakt dan maak ticket aan bij vervoerder |
| Beschrijving           |                                                                         |
| Reageer op gebeurtenis | Ticket bij vervoerder aangemaakt                                        |
| Voer opdracht uit      | Maak ticket aan bij vervoerder                                          |
| Status                 | Klaar om uitgevoerd te worden                                           |
En is de opdrachtgegevensconfiguratie als volgt ingevuld:
| Opdrachtgegevennaam | Opdrachtgegevenwaardetype | Gebeurtenisgegevendefinitie         | IngevuldeWaarde                                           | IsVerplicht |
| Ordernummer         | Nummer                    | [Order niet geleverd].[Ordernummer] | <geen>                                                    | Waar        |
| BegeleidendeTekst   | Tekst                     | <geen>                              | Order met $(Ordernummer) niet geleverd, graag onderzoeken | Waar        |

Scenario: Opdrachtgegevensconfiguraties aangepast als gebeurtenisgegevens niet meer beschikbaar zijn
Gegeven ik een navolgende work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde                           |
| Beschrijving           |                                  |
| Reageer op gebeurtenis | Taak verstuurd |
| Voer opdracht uit      | Maak ticket aan bij vervoerder   |
En ik voor de opdracht 'Maak ticket aan bij vervoerder' de opdrachtgegevens invul op de volgende wijze:
| Gegeven           | Waarde                                                    | Bron                |
| Ordernummer       | Ordernummer                                               | Order niet geleverd |
| BegeleidendeTekst | Order met $(Ordernummer) niet geleverd, graag onderzoeken | Ingevuld            |
Als ik de gebeurtenis waar de vorige work-itemconfiguratie op moet reageren verander naar 'Taak verstuurd'
Dan heeft deze nieuwe work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                                                |
| Titel                  | Als taak verstuurd dan maak ticket aan bij vervoerder |
| Beschrijving           |                                                       |
| Reageer op gebeurtenis | Taak verstuurd                                        |
| Voer opdracht uit      | Maak ticket aan bij vervoerder                        |
| Status                 | Niet alle opdrachtgegevens zijn gevuld                |
En is de opdrachtgegevensconfiguratie als volgt ingevuld:
| Opdrachtgegevennaam | Opdrachtgegevenwaardetype | Gebeurtenisgegevendefinitie | IngevuldeWaarde                              | IsVerplicht |
| Ordernummer         | Nummer                    | <geen>                      | <geen>                                       | Waar        |
| BegeleidendeTekst   | Tekst                     | <geen>                      | Order met ? niet geleverd, graag onderzoeken | Waar        |