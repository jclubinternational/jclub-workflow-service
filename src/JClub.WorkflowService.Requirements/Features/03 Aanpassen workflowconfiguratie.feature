﻿Functionaliteit: 03 Aanpassen workflowconfiguratie

Hier is beschreven hoe een workflowconfiguratie aangepast kan worden.

De achtergrond geeft aan hoe een basis workflowconfiguratie er uit kan zien.

Achtergrond: 
Gegeven de gebruiker is 'Jantine Braas'
En ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Order niet geleverd                                              |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
En de gebeurtenis 'Order niet geleverd' aanwezig is met de volgende gegevens:
| Gegeven     | Type   |
| Ordernummer | Nummer |
En de gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven | Type |
| Taak    | Taak |
En de opdracht 'Verstuur taak' met als resultaat gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven              | Type      |
| Taal                 | Taal      |
| Titel                | Tekst     |
| Korte omschrijving   | Tekst     |
| Lange omschrijving   | Tekst     |
| Toewijzing vestiging | Vestiging |
| Toewijzing rol       | Rol       |
En ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
En de volgende globale gegevens bestaan:
| Gegeven      | Type      |
| Nederlands   | Taal      |
| Hoofdkantoor | Vestiging |
| CM           | Rol       |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |

Scenario: Aanpassen van een titel en beschrijving van een niet actief workflowconfiguratie
Als ik de workflowconfiguratie alle bewerkbare gegevens aanpas op volgende wijze:
| Gegeven      | Waarde                                                                         |
| Titel        | Order niet geleverd aan MM                                                     |
| Beschrijving | Workflow voor als een MM via de portal aangeeft dat een order niet geleverd is |
Dan is de workflowconfiguratie bijgewerkt met de volgende gegevens:
| Gegeven                 | Waarde                                                                         |
| Titel                   | Order niet geleverd aan MM                                                     |
| Beschrijving            | Workflow voor als een MM via de portal aangeeft dat een order niet geleverd is |
| Aangemaakt door         | Jantine Braas                                                                  |
| Is actief               | Onwaar                                                                         |
| Reageert op gebeurtenis | Order niet geleverd                                                            |
| Vervangen door          | <geen>                                                                         |

Scenario: Activeren van een volledig geconfigureerde workflowconfiguratie
Als de workflowconfiguratie 'Order niet geleverd' geactiveerd wordt
Dan is de workflowconfiguratie bijgewerkt met de volgende gegevens:
| Gegeven                 | Waarde                                                           |
| Titel                   | Order niet geleverd                                              |
| Beschrijving            | Als een MM aangeeft via de portal dat een order niet geleverd is |
| Aangemaakt door         | Jantine Braas                                                    |
| Is actief               | Waar                                                             |
| Reageert op gebeurtenis | Order niet geleverd                                              |
| Vervangen door          | <geen>                                                           |

Scenario: Activeren van een niet volledig geconfigureerde workflowconfiguratie
Gegeven ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
#| Toewijzing rol       | CM                                                    | Ingevuld |
Als de workflowconfiguratie 'Order niet geleverd' geactiveerd wordt
Dan is de workflowconfiguratie bijgewerkt met de volgende gegevens:
| Gegeven                 | Waarde                                                           |
| Titel                   | Order niet geleverd                                              |
| Beschrijving            | Als een MM aangeeft via de portal dat een order niet geleverd is |
| Aangemaakt door         | Jantine Braas                                                    |
| Is actief               | Onwaar                                                           |
| Reageert op gebeurtenis | Order niet geleverd                                              |
| Vervangen door          | <geen>                                                           |

Scenario: Deactiveren van een workflowconfiguratie
Gegeven de workflowconfiguratie 'Order niet geleverd' geactiveerd is
Als de workflowconfiguratie 'Order niet geleverd' gedeactiveerd wordt
Dan is de workflowconfiguratie bijgewerkt met de volgende gegevens:
| Gegeven                 | Waarde                                                           |
| Titel                   | Order niet geleverd                                              |
| Beschrijving            | Als een MM aangeeft via de portal dat een order niet geleverd is |
| Aangemaakt door         | Jantine Braas                                                    |
| Is actief               | Onwaar                                                           |
| Reageert op gebeurtenis | Order niet geleverd                                              |
| Vervangen door          | <geen>                                                           |