﻿Functionaliteit: 06 Aftrappen workflow (basis)
	
Achtergrond: 
Gegeven de gebruiker is 'Jantine Braas'
En ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Workflow voor als de order niet geleverd is                      |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
En de gebeurtenis 'Order niet geleverd' aanwezig is met de volgende gegevens:
| Gegeven     | Type   |
| Ordernummer | Nummer |
En de gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven | Type |
| Taak    | Taak |
En de opdracht 'Verstuur taak' met als resultaat gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven              | Type      |
| Taal                 | Taal      |
| Titel                | Tekst     |
| Korte omschrijving   | Tekst     |
| Lange omschrijving   | Tekst     |
| Toewijzing vestiging | Vestiging |
| Toewijzing rol       | Rol       |
En ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
En de volgende globale gegevens bestaan:
| Gegeven      | Type      |
| Nederlands   | Taal      |
| Hoofdkantoor | Vestiging |
| CM           | Rol       |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |

Scenario: Gebeurtenis zorgt ervoor dat een workflow wordt gestart
Gegeven de workflowconfiguratie 'Workflow voor als de order niet geleverd is' geactiveerd is
Als er de eerste gebeurtenis 'Order niet geleverd' optreedt met de volgende gegevens:
| Gegeven     | Waarde |
| Ordernummer | 1      |
Dan is er de opdracht 'Verstuur taak' verstuurd met de volgende gegevens
| Gegeven              | Waarde                                   |
| Taal                 | Nederlands                               |
| Titel                | Order niet geleverd                      |
| Korte omschrijving   | Order niet geleverd                      |
| Lange omschrijving   | Order met ordernummer 1 is niet geleverd |
| Toewijzing vestiging | Hoofdkantoor                             |
| Toewijzing rol       | CM                                       |
En Workflow 'Order niet geleverd' heeft de volgende gegevens:
| Gegeven          | Waarde                                      |
| Titel            | Workflow voor als de order niet geleverd is |
| Gestart op       | <vandaag>                                   |
| Gestopt op       | <geen>                                      |
| Status           | Gestart                                     |
| Huidige WorkItem | Als order niet geleverd dan verstuur taak   |

Scenario: Gebeurtenis zorgt ervoor dat een workflow wordt voltooid
Gegeven de workflowconfiguratie 'Workflow voor als de order niet geleverd is' geactiveerd is
Als er de eerste gebeurtenis 'Order niet geleverd' optreedt met de volgende gegevens:
| Gegeven     | Waarde |
| Ordernummer | 1      |
En er de laatste gebeurtenis 'Taak verstuurd' optreedt met de volgende gegevens:
| Gegeven | Waarde              |
| Taak    | Order niet geleverd |
Dan Workflow 'Order niet geleverd' heeft de volgende gegevens:
| Gegeven          | Waarde                                      |
| Titel            | Workflow voor als de order niet geleverd is |
| Gestart op       | <vandaag>                                   |
| Gestopt op       | <vandaag>                                   |
| Status           | Voltooid                                    |
| Huidige WorkItem | Als order niet geleverd dan verstuur taak   |