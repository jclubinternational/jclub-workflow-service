﻿Functionaliteit: 01 Configureren workflowconfiguratie (basis)

Hier wordt beschreven hoe je een basis workflowconfiguratie kunt configureren

Workflowconfiguratie:
Dit is een blauwdruk van hoe de workflow kan verlopen. Hier geef je aan in welke volgorde work-items uitgevoerd dienen te worden.

Work-itemconfiguratie:
Dit bevat een definitie van:
- Een gebeurtenis waar het op reageerd
- Een opdracht die dan moet worden uitgevoerd en
- De resulterende gebeurtenis als de opdracht uitgevoerd is

Gebeurtenis:
In deze context zal er een lijst van voorgeprogrammeerde gebeurtenissen zijn waar men uit kan kiezen om een workflow te starten.

Opdracht:
In deze context zal er een lijst van voorgeprogrameerde opdrachten zijn die uitgevoerd kunnen worden aan de hand van een gebeurtenis.

Achtergrond: Er zijn gebruikers, gebeurtenissen, opdrachten en globale gegevens
Gegeven de gebruiker is 'Jantine Braas'
En de gebeurtenis 'Order niet geleverd' aanwezig is met de volgende gegevens:
| Gegeven     | Type   |
| Ordernummer | Nummer |
En de gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven | Type |
| Taak    | Taak |
En de opdracht 'Verstuur taak' met als resultaat gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven              | Type      |
| Taal                 | Taal      |
| Titel                | Tekst     |
| Korte omschrijving   | Tekst     |
| Lange omschrijving   | Tekst     |
| Toewijzing vestiging | Vestiging |
| Toewijzing rol       | Rol       |
En de volgende globale gegevens bestaan:
| Gegeven      | Type      |
| Nederlands   | Taal      |
| Hoofdkantoor | Vestiging |
| CM           | Rol       |

Scenario: Aanmaken van een nieuwe workflowconfiguratie
Gegeven er nog geen workflowconfiguraties zijn
Als ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Order niet geleverd                                              |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
Dan wordt de volgende workflowconfiguratie toegevoegd met de volgende gegevens:
| Gegeven                 | Waarde                                                           |
| Titel                   | Order niet geleverd                                              |
| Beschrijving            | Als een MM aangeeft via de portal dat een order niet geleverd is |
| Aangemaakt door         | Jantine Braas                                                    |
| Is actief               | Onwaar                                                           |
| Reageert op gebeurtenis | <geen>                                                           |
| Vervangen door          | <geen>                                                           |

Scenario: Registreren van een nieuwe en eerste work-itemconfiguratie
Gegeven ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Order niet geleverd                                              |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
Als ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
Dan heeft deze nieuwe work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                                    |
| Titel                  | Als order niet geleverd dan verstuur taak |
| Beschrijving           |                                           |
| Reageer op gebeurtenis | Order niet geleverd                       |
| Voer opdracht uit      | Verstuur taak                             |
| Status                 | Niet alle opdrachtgegevens zijn gevuld    |

Scenario: Registreren van alle opdrachtgegevens
Gegeven ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Order niet geleverd                                              |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
Als ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
Als ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |
Dan verandert de status van de work-itemconfiguratie met de titel 'Als order niet geleverd dan verstuur taak' naar 'Klaar om uitgevoerd te worden'