﻿Functionaliteit: 07 Aftrappen workflow (medium)

Achtergrond: 
Gegeven de gebruiker is 'Jantine Braas'
En de volgende globale gegevens bestaan:
| Gegeven      | Type      |
| Nederlands   | Taal      |
| Hoofdkantoor | Vestiging |
| CM           | Rol       |
En ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Workflow voor als de order niet geleverd is                      |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
En de gebeurtenis 'Order niet geleverd' aanwezig is met de volgende gegevens:
| Gegeven     | Type   |
| Ordernummer | Nummer |
En de gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven | Type |
| Taak    | Taak |
En de opdracht 'Verstuur taak' met als resultaat gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven              | Type      |
| Taal                 | Taal      |
| Titel                | Tekst     |
| Korte omschrijving   | Tekst     |
| Lange omschrijving   | Tekst     |
| Toewijzing vestiging | Vestiging |
| Toewijzing rol       | Rol       |
En ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |
En ik een navolgende work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde         |
| Beschrijving           |                |
| Reageer op gebeurtenis | Taak verstuurd |
| Voer opdracht uit      | Verstuur taak  |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |

Scenario: Meerdere gebeurtenissen zijn nodig om een workflow te voltooien
Gegeven de workflowconfiguratie 'Workflow voor als de order niet geleverd is' geactiveerd is
Als er de eerste gebeurtenis 'Order niet geleverd' optreedt met de volgende gegevens:
| Gegeven     | Waarde |
| Ordernummer | 1      |
En er de volgende gebeurtenis 'Taak verstuurd' optreedt met de volgende gegevens:
| Gegeven | Waarde              |
| Taak    | Order niet geleverd |
Dan Workflow 'Order niet geleverd' heeft de volgende gegevens:
| Gegeven          | Waarde                                      |
| Titel            | Workflow voor als de order niet geleverd is |
| Gestart op       | <vandaag>                                   |
| Gestopt op       | <geen>                                      |
| Status           | Gestart                                     |
| Huidige WorkItem | Als taak verstuurd dan verstuur taak        |
Als er de laatste gebeurtenis 'Taak verstuurd' optreedt met de volgende gegevens:
| Gegeven | Waarde              |
| Taak    | Order niet geleverd |
Dan Workflow 'Order niet geleverd' heeft de volgende gegevens:
| Gegeven          | Waarde                                      |
| Titel            | Workflow voor als de order niet geleverd is |
| Gestart op       | <vandaag>                                   |
| Gestopt op       | <vandaag>                                   |
| Status           | Voltooid                                    |
| Huidige WorkItem | Als taak verstuurd dan verstuur taak        |
