﻿Functionaliteit: 04 Aanpassen work-itemconfiguratie (basis)
	Simple calculator for adding two numbers

Achtergrond: 
Gegeven de gebruiker is 'Jantine Braas'
En ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:
| Gegeven      | Waarde                                                           |
| Titel        | Order niet geleverd                                              |
| Beschrijving | Als een MM aangeeft via de portal dat een order niet geleverd is |
En de gebeurtenis 'Order niet geleverd' aanwezig is met de volgende gegevens:
| Gegeven     | Type   |
| Ordernummer | Nummer |
En de gebeurtenis 'Ticket bij vervoerder aangemaakt' aanwezig is met de volgende gegevens:
| Gegeven          | Type  |
| Ticketreferentie | Tekst |
En de opdracht 'Maak ticket aan bij vervoerder' met als resultaat gebeurtenis 'Ticket bij vervoerder aangemaakt' aanwezig is met de volgende gegevens:
| Gegeven           | Type   |
| Ordernummer       | Nummer |
| BegeleidendeTekst | Tekst  |
En de gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven | Type |
| Taak    | Taak |
En de opdracht 'Verstuur taak' met als resultaat gebeurtenis 'Taak verstuurd' aanwezig is met de volgende gegevens:
| Gegeven              | Type      |
| Taal                 | Taal      |
| Titel                | Tekst     |
| Korte omschrijving   | Tekst     |
| Lange omschrijving   | Tekst     |
| Toewijzing vestiging | Vestiging |
| Toewijzing rol       | Rol       |
En de volgende globale gegevens bestaan:
| Gegeven      | Type      |
| Nederlands   | Taal      |
| Hoofdkantoor | Vestiging |
| CM           | Rol       |

Scenario: Aanpassen van een beschrijving van een work-itemconfiguratie
Gegeven ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
Als ik de beschrijving aanpas naar 'Een beschrijving voor deze work-item'
Dan heeft deze nieuwe work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                                    |
| Titel                  | Als order niet geleverd dan verstuur taak |
| Beschrijving           | Een beschrijving voor deze work-item      |
| Reageer op gebeurtenis | Order niet geleverd                       |
| Voer opdracht uit      | Verstuur taak                             |
| Status                 | Niet alle opdrachtgegevens zijn gevuld    |

Scenario: Aanpassen op welke gebeurtenis een eerste work-itemconfiguratie moet reageren
Gegeven ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |
Als ik de gebeurtenis waar de work-itemconfiguratie op moet reageren verander naar 'Taak verstuurd'
Dan heeft deze nieuwe work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                                 |
| Titel                  | Als taak verstuurd dan verstuur taak   |
| Beschrijving           |                                        |
| Reageer op gebeurtenis | Taak verstuurd                         |
| Voer opdracht uit      | Verstuur taak                          |
| Status                 | Niet alle opdrachtgegevens zijn gevuld |
En is de opdrachtgegevensconfiguratie als volgt ingevuld:
| Opdrachtgegevennaam  | Opdrachtgegevenwaardetype | Gebeurtenisdefinitiegegeven | IngevuldeWaarde                          | IsVerplicht |
| Taal                 | Taal                      | <geen>                      | Nederlands                               | Waar        |
| Titel                | Tekst                     | <geen>                      | Order niet geleverd                      | Waar        |
| Korte omschrijving   | Tekst                     | <geen>                      | Order niet geleverd                      | Waar        |
| Lange omschrijving   | Tekst                     | <geen>                      | Order met ordernummer ? is niet geleverd | Waar        |
| Toewijzing vestiging | Vestiging                 | <geen>                      | Hoofdkantoor                             | Waar        |
| Toewijzing rol       | Rol                       | <geen>                      | CM                                       | Waar        |

Scenario: Aanpassen welke opdracht uitgevoerd dient te worden voor de laatste work-itemconfiguratie
Gegeven ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
Als ik de opdracht dat de work-itemconfiguratie moet uitvoeren verander naar 'Maak ticket aan bij vervoerder'
Dan heeft deze nieuwe work-itemconfiguratie de volgende gegevens:
| Gegeven                | Waarde                                                     |
| Titel                  | Als order niet geleverd dan maak ticket aan bij vervoerder |
| Beschrijving           |                                                            |
| Reageer op gebeurtenis | Order niet geleverd                                        |
| Voer opdracht uit      | Maak ticket aan bij vervoerder                             |
| Status                 | Niet alle opdrachtgegevens zijn gevuld                     |

Scenario: Onvolledig invullen van opdrachtgegevens
Gegeven ik een eerste work-itemconfiguratie toevoeg met de volgende gegevens:
| Gegeven                | Waarde              |
| Beschrijving           |                     |
| Reageer op gebeurtenis | Order niet geleverd |
| Voer opdracht uit      | Verstuur taak       |
En ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
| Toewijzing rol       | CM                                                    | Ingevuld |
Als ik voor de opdracht 'Verstuur taak' de opdrachtgegevens invul op de volgende wijze:
| Gegeven              | Waarde                                                | Bron     |
| Taal                 | Nederlands                                            | Ingevuld |
| Titel                | Order niet geleverd                                   | Ingevuld |
| Korte omschrijving   | Order niet geleverd                                   | Ingevuld |
| Lange omschrijving   | Order met ordernummer $(Ordernummer) is niet geleverd | Ingevuld |
| Toewijzing vestiging | Hoofdkantoor                                          | Ingevuld |
#| Toewijzing rol       | CM                                                    | Ingevuld |
Dan verandert de status van de work-itemconfiguratie met de titel 'Als order niet geleverd dan verstuur taak' naar 'Niet alle opdrachtgegevens zijn gevuld'