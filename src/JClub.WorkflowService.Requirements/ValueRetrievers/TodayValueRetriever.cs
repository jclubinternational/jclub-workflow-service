using System;
using System.Collections.Generic;
using TechTalk.SpecFlow.Assist;

namespace JClub.WorkflowService.Requirements.ValueRetrievers {
	public class TodayValueRetriever : IValueRetriever {
		private readonly string _todayString;

		public TodayValueRetriever(string todayString) {
			_todayString = todayString;
		}


		public bool CanRetrieve(KeyValuePair<string, string> keyValuePair, Type targetType, Type propertyType) {
			var result = (propertyType == typeof(DateTime) ||
						  propertyType == typeof(DateTime?)) &&
			 string.Compare(keyValuePair.Value.Trim(), _todayString, StringComparison.InvariantCultureIgnoreCase) == 0;

			return result;
		}

		public object Retrieve(KeyValuePair<string, string> keyValuePair, Type targetType, Type propertyType) {
			return DateTime.UtcNow;
		}
	}
}