using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TechTalk.SpecFlow.Assist;

namespace JClub.WorkflowService.Requirements.ValueRetrievers {
	public class ValueToValueRetriever : IValueRetriever {
		private readonly string _sourceValue;
		private readonly object _outputValue;

		public ValueToValueRetriever(string sourceValue, object outputValue) {
			_sourceValue = sourceValue;
			_outputValue = outputValue;
		}

		public bool CanRetrieve(KeyValuePair<string, string> keyValuePair, Type targetType, Type propertyType) {
			var words = keyValuePair.Key.Split(' ');

			var property = targetType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
									 .SingleOrDefault(x => x.Name.StartsWith(words[0],StringComparison.InvariantCultureIgnoreCase));

			var result = keyValuePair.Value == _sourceValue && property?.GetMethod != null 
															&& (
																property.GetMethod.ReturnType == propertyType || 
																Nullable.GetUnderlyingType(property.GetMethod.ReturnType) == propertyType
																);

			return result;
		}

		public object Retrieve(KeyValuePair<string, string> keyValuePair, Type targetType, Type propertyType) {
			if (propertyType != _outputValue.GetType() &&
				Nullable.GetUnderlyingType(propertyType) != _outputValue.GetType()) {
				return keyValuePair.Value;
			}

			return _outputValue;
		}
	}
}