using TechTalk.SpecFlow.Assist.ValueRetrievers;

namespace JClub.WorkflowService.Requirements.ValueRetrievers {
	public class DutchBoolValueRetriever : BoolValueRetriever {
		protected override bool GetNonEmptyValue(string value) {
			return value == "Waar";
		}
	}
}