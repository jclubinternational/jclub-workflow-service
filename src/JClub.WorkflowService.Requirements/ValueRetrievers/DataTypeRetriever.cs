using System;
using System.Collections.Generic;
using TechTalk.SpecFlow.Assist;

namespace JClub.WorkflowService.Requirements.ValueRetrievers {
	public class DataTypeRetriever  : IValueRetriever {
		public bool CanRetrieve(KeyValuePair<string, string> keyValuePair, Type targetType, Type propertyType) {
			return keyValuePair.Key == "Type";
		}

		public object Retrieve(KeyValuePair<string, string> keyValuePair, Type targetType, Type propertyType) {
			return keyValuePair.Value switch {
				"Tekst"  => DataType.Text,
				"Nummer" => DataType.Number,
				_        => keyValuePair.Value
			};
		}

		public enum DataType {
			Text,
			Number
		}
	}
}