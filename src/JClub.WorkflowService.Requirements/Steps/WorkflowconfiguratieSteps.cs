using FluentAssertions;
using JClub.WorkflowService.Messages.Commands;
using JClub.WorkflowService.Requirements.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.Enums;
using JClub.WorkflowService.Business.Contracts.Events;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using Microsoft.Extensions.DependencyInjection;
using JClub.WorkflowService.Data;
using JClub.WorkflowService.Requirements.ValueRetrievers;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using TechTalk.SpecFlow.Assist.Attributes;

namespace JClub.WorkflowService.Requirements.Steps {
	[Binding]
    public class WorkflowconfiguratieSteps {

		private Guid _gebruikerId;
		private readonly RequirementWebApplicationFactory _requirementWebApplicationFactory;
		private Guid _workflowconfiguratieId;
		private Guid _workitemconfiguratieId;
		private Guid _vorigeWorkitemconfiguratieId;

		public WorkflowconfiguratieSteps(RequirementWebApplicationFactory requirementWebApplicationFactory) {
			_requirementWebApplicationFactory = requirementWebApplicationFactory;
		}

		[Given(@"de gebruiker is '(.*)'")]
		public void GegevenDeGebruikerIs(string gebruikersnaam) {
			_gebruikerId = new Guid("56F351FD-C282-46EA-B05A-E379BD0692F6");

			Service.Instance.ValueRetrievers.Register(new ValueToValueRetriever(gebruikersnaam, _gebruikerId));
		}

		[Given(@"er nog geen workflowconfiguraties zijn")]
		public void GegevenErNogGeenWorkflowconfiguratiesZijn() {
			//Hoeft niets te gebeuren
		}

		[Given(@"ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:")]
		[When(@"ik een nieuwe workflowconfiguratie toevoeg met de volgende gegevens:")]
		public async Task AlsIkEenNieuweWorkflowconfiguratieToevoegMetDeVolgendeGegevens(Table table) {
			var command = table.CreateInstance<RegistreerNieuweWorkflowconfiguratieCommand>();
			command.AangemaaktDoor = _gebruikerId;

			var result = await _requirementWebApplicationFactory.SendCommandAndAwaitEvent<RegistreerNieuweWorkflowconfiguratieCommand, NieuweWorkflowconfiguratieGeregistreerdEvent>(command);
			_workflowconfiguratieId = result.Id;
		}

		[Given(@"de gebeurtenis '(.*)' aanwezig is met de volgende gegevens:")]
		public void GegevenDeGebeurtenisAanwezigIsMetDeVolgendeGegevens(
			string gebeurtenisnaam,
			Table  table) {

			var gegevens = table.CreateSet<(string gegeven,string type)>();
			var eventDefinition = new Gebeurtenisdefinitie(gebeurtenisnaam);

			eventDefinition.VoegGegevensdefinitiesToe(gegevens.Select(_ => new Gebeurtenisgegevendefinitie {
				Id                = Guid.NewGuid(),
				Gegevennaam       = _.gegeven,
				Gegevenwaardetype = _.type,
				BronApiAdres      = _.type != "Tekst" && _.type != "Nummer" ? $"http://localhost/api/{_.type}/{{0}}" : ""
			}).ToArray());

			var dbContext = _requirementWebApplicationFactory.Services.GetRequiredService<WorkflowServiceDataContext>();
			dbContext.Gebeurtenisdefinities.Add(eventDefinition);
			dbContext.SaveChanges();

			Service.Instance.ValueRetrievers.Register(new ValueToValueRetriever(gebeurtenisnaam,eventDefinition.Id));

			foreach (var definitie in eventDefinition.Gegevensdefinities) {
				Service.Instance.ValueRetrievers.Register(new ValueToValueRetriever( $"[{gebeurtenisnaam}].[{definitie.Gegevennaam}]", definitie.Id));
			}
		}

		[Given(@"de opdracht '(.*)' met als resultaat gebeurtenis '(.*)' aanwezig is met de volgende gegevens:")]
		public void GegevenDeOpdrachtMetResultaatGebeurtenisAanwezigIsMetDeVolgendeGegevens(
			string opdrachtnaam,
			string gebeurtenisnaam,
			Table  table) {

			var gegevens = table.CreateSet<(string gegeven,string type)>();

			var dbContext = _requirementWebApplicationFactory.Services.GetRequiredService<WorkflowServiceDataContext>();
			var eventDefinition = dbContext.Gebeurtenisdefinities.Single(_ => _.Naam == gebeurtenisnaam);

			var commandDefinition = new Opdrachtdefinitie(opdrachtnaam,_requirementWebApplicationFactory.EndPointName,eventDefinition);

			commandDefinition.VoegGegevensdefinitiesToe(gegevens.Select(_ => new Opdrachtgegevendefinitie {
				Id                = Guid.NewGuid(),
				Gegevennaam       = _.gegeven,
				Gegevenwaardetype = _.type,
				IsVerplicht       = true
			}).ToArray());

			dbContext.Opdrachtdefinities.Add(commandDefinition);
			dbContext.SaveChanges();

			Service.Instance.ValueRetrievers.Register(new ValueToValueRetriever(opdrachtnaam,commandDefinition.Id));
		}

		public IDictionary<string, string> GlobaleGegevens = new Dictionary<string, string>();

		[Given(@"de volgende globale gegevens bestaan:")]
		public void GegevenDeVolgendeGlobaleGegevensBestaan(Table table) {

			var gegevens = table.CreateSet<(string gegeven, string Type)>();

			foreach (var (gegeven, type) in gegevens) {
				var id = $"{Guid.NewGuid()}";
				GlobaleGegevens[gegeven] = id;
				GlobaleGegevens[id]      = gegeven;
			}
		}

		[When(@"ik de workflowconfiguratie alle bewerkbare gegevens aanpas op volgende wijze:")]
		public async Task AlsIkDeWorkflowconfiguratieAlleBewerkbareGegevensAanpasOpVolgendeWijze(Table table) {
			var command = table.CreateInstance<MuteerWorkflowconfiguratieCommand>();
			command.Id = _workflowconfiguratieId;

			var result = await _requirementWebApplicationFactory.SendCommandAndAwaitEvent<MuteerWorkflowconfiguratieCommand, WorkflowconfiguratieAangepastEvent>(command);
			_workflowconfiguratieId = result.Id;
		}

		[Given(@"ik een (eerste|voorgaande|navolgende) work-itemconfiguratie toevoeg met de volgende gegevens:")]
		[When(@"ik een (eerste|voorgaande|navolgende) work-itemconfiguratie toevoeg met de volgende gegevens:")]
		public async Task AlsIkEenNieuweWork_ItemconfiguratieToevoegMetDeVolgendeGegevens(Volgorde volgorde, Table table) {
			var command = table.CreateInstance<RegistreerNieuweWorkItemconfiguratieCommand>();

			if (volgorde == Volgorde.Navolgende) {
				command.VorigeWorkItemconfiguratieId = _workitemconfiguratieId;
			}
			command.WorkflowconfiguratieId       = _workflowconfiguratieId;

			var result = await _requirementWebApplicationFactory.SendCommandAndAwaitEvent<RegistreerNieuweWorkItemconfiguratieCommand, NieuweWorkItemconfiguratieGeregistreerdEvent>(command);
			_vorigeWorkitemconfiguratieId = _workitemconfiguratieId;
			_workitemconfiguratieId       = result.Id;
		}

		[Given(@"ik voor de opdracht '(.*)' de opdrachtgegevens invul op de volgende wijze:")]
		[When(@"ik voor de opdracht '(.*)' de opdrachtgegevens invul op de volgende wijze:")]
		public async Task AlsIkVoorDeOpdrachtgegevensInvulOpDeVolgendeWijze(
			string opdrachtnaam, 
			Table table) {

			var dbContext = _requirementWebApplicationFactory.Services.GetRequiredService<WorkflowServiceDataContext>();
			var opdrachtdefinities = dbContext.Opdrachtdefinities.Single(_ => _.Naam == opdrachtnaam)
				.Gegevensdefinities.ToDictionary(_ => _.Gegevennaam);

			var configuraties = table.CreateSet<(string gegeven, string waarde, string bron)>()
				.Select(_ => new Opdrachtgegevenvulling {
					OpdrachtgegevendefinitieId = opdrachtdefinities[_.gegeven].Id,
					IngevuldeWaarde = _.bron == "Ingevuld" 
						? $"{(GlobaleGegevens.TryGetValue(_.waarde, out var value) ? value : _.waarde)}" 
						: null,
					GebeurtenisgegevendefinitieId = _.bron != "Ingevuld" 
						? dbContext.Gebeurtenisdefinities.Single(gd => gd.Naam == _.bron).Gegevensdefinities.Single(geg => geg.Gegevennaam == _.waarde).Id
						: null
				});

			var command = new RegistreerNieuweOpdrachtgegevensvullingconfiguratieCommand {
				WorkItemconfiguratieId = _workitemconfiguratieId, 
				Opdrachtgegevenvullingen = configuraties.ToArray()
			};

			var _ = await _requirementWebApplicationFactory.SendCommandAndAwaitEvent<RegistreerNieuweOpdrachtgegevensvullingconfiguratieCommand, OpdrachtinformatievullingconfiguratieGeregistreerdEvent>(command);
		}

		[Given(@"de workflowconfiguratie '(.*)' geactiveerd is")]
		[When(@"de workflowconfiguratie '(.*)' geactiveerd wordt")]
		public async Task AlsDeWorkflowconfiguratieGeactiveerdWordt(string workflowconfiguratietitel) {
			var command = new ActiveerWorkflowconfiguratieCommand {
				Id = _workflowconfiguratieId
			};

			await _requirementWebApplicationFactory.SendCommandAndAwaitEvents<ActiveerWorkflowconfiguratieCommand, WorkflowconfiguratieGeactiveerdEvent, WorkflowconfiguratieActivatieGefaaldEvent>(command);
		}

		[When(@"de workflowconfiguratie '(.*)' gedeactiveerd wordt")]
		public async Task AlsDeWorkflowconfiguratieGedeactiveerdWordt(string p0) {
			var command = new DeactiveerWorkflowconfiguratieCommand {
				Id = _workflowconfiguratieId
			};

			await _requirementWebApplicationFactory.SendCommandAndAwaitEvent<DeactiveerWorkflowconfiguratieCommand, WorkflowconfiguratieGedeactiveerdEvent>(command);
		}


		[When(@"ik de beschrijving aanpas naar '(.*)'")]
		public Task AlsIkDeBeschrijvingAanpasNaar(string beschrijving) {
			var command = new MuteerWorkItemconfiguratieCommand {
				Id = _workitemconfiguratieId,
				Beschrijving = beschrijving
			};

			return _requirementWebApplicationFactory.SendCommandAndAwaitEvent<MuteerWorkItemconfiguratieCommand,WorkItemconfiguratieGemuteerdEvent>(command);
		}

		[When(@"ik de gebeurtenis waar de work-itemconfiguratie op moet reageren verander naar '(.*)'")]
		public Task AlsIkDeGebeurtenisWaarDeWork_ItemconfiguratieOpMoetReagerenVeranderNaar(string nieuweGebeurtenis) {
			var dbContext = _requirementWebApplicationFactory.Services.GetRequiredService<WorkflowServiceDataContext>();

			var gebeurtenis = dbContext.Gebeurtenisdefinities.Single(_ => _.Naam == nieuweGebeurtenis);

			var command = new MuteerWorkItemconfiguratieCommand {
				Id           = _workitemconfiguratieId,
				ReageerOpGebeurtenis = gebeurtenis.Id
			};

			return _requirementWebApplicationFactory.SendCommandAndAwaitEvent<MuteerWorkItemconfiguratieCommand, WorkItemconfiguratieGemuteerdEvent>(command);
		}

		[When(@"ik de gebeurtenis waar de vorige work-itemconfiguratie op moet reageren verander naar '(.*)'")]
		public Task AlsIkDeGebeurtenisWaarDeVorigeWork_ItemconfiguratieOpMoetReagerenVeranderNaar(string nieuweGebeurtenis) {
			var dbContext = _requirementWebApplicationFactory.Services.GetRequiredService<WorkflowServiceDataContext>();

			var gebeurtenis = dbContext.Gebeurtenisdefinities.Single(_ => _.Naam == nieuweGebeurtenis);

			var command = new MuteerWorkItemconfiguratieCommand {
				Id                   = _vorigeWorkitemconfiguratieId,
				ReageerOpGebeurtenis = gebeurtenis.Id
			};

			return _requirementWebApplicationFactory.SendCommandAndAwaitEvent<MuteerWorkItemconfiguratieCommand, WorkItemconfiguratieGemuteerdEvent>(command);
		}


		[When(@"ik de opdracht dat de work-itemconfiguratie moet uitvoeren verander naar '(.*)'")]
		public Task AlsIkDeOpdrachtDatDeWork_ItemconfiguratieMoetUitvoerenVeranderNaar(string nieuweOpdracht) {
			var dbContext = _requirementWebApplicationFactory.Services.GetRequiredService<WorkflowServiceDataContext>();

			var opdracht = dbContext.Opdrachtdefinities.Single(_ => _.Naam == nieuweOpdracht);

			var command = new MuteerWorkItemconfiguratieCommand {
				Id                   = _workitemconfiguratieId,
				VoerOpdrachtUit = opdracht.Id
			};

			return _requirementWebApplicationFactory.SendCommandAndAwaitEvent<MuteerWorkItemconfiguratieCommand, WorkItemconfiguratieGemuteerdEvent>(command);
		}

		[When(@"ik de opdracht dat de vorige work-itemconfiguratie moet uitvoeren verander naar '(.*)'")]
		public Task AlsIkDeOpdrachtDatDeVorgieWork_ItemconfiguratieMoetUitvoerenVeranderNaar(string nieuweOpdracht) {
			var dbContext = _requirementWebApplicationFactory.Services.GetRequiredService<WorkflowServiceDataContext>();

			var opdracht = dbContext.Opdrachtdefinities.Single(_ => _.Naam == nieuweOpdracht);

			var command = new MuteerWorkItemconfiguratieCommand {
				Id              = _vorigeWorkitemconfiguratieId,
				VoerOpdrachtUit = opdracht.Id
			};

			return _requirementWebApplicationFactory.SendCommandAndAwaitEvent<MuteerWorkItemconfiguratieCommand, WorkItemconfiguratieGemuteerdEvent>(command);
		}


		[Then(@"is de workflowconfiguratie bijgewerkt met de volgende gegevens:")]
		[Then(@"wordt de volgende workflowconfiguratie toegevoegd met de volgende gegevens:")]
		public async Task DanWordtDeVolgendeWorkflowconfiguratieToegevoegdMetDeVolgendeGegevens(Table table) {
			var expectedViewModel = table.CreateInstance<WorkflowconfiguratieViewModel>();
			expectedViewModel.Id = _workflowconfiguratieId;

			var viewModel = await _requirementWebApplicationFactory.GetConnector().GetWorkflowconfiguratieById(_workflowconfiguratieId);
			viewModel.Should().BeEquivalentTo(expectedViewModel);
		}

		[Then(@"heeft deze nieuwe work-itemconfiguratie de volgende gegevens:")]
		public async Task DanHeeftDezeNieuweWork_ItemconfiguratieToegevoegdMetDeVolgendeGegevens(Table table) {
			var expectedViewModel = table.CreateInstance<WorkItemconfiguratieViewModel>();
			expectedViewModel.Id = _workitemconfiguratieId;

			var viewModel = await _requirementWebApplicationFactory.GetConnector().GetWorkItemconfiguratieById(_workflowconfiguratieId, _workitemconfiguratieId);
			expectedViewModel.VorigeWorkItemconfiguratieId = viewModel.VorigeWorkItemconfiguratieId;
			viewModel.Should().BeEquivalentTo(expectedViewModel);
		}

		[Then(@"heeft de reeds bestaande work-itemconfiguratie de volgende gegevens:")]
		public async Task DanHeeftDeReedsBestaandeWork_ItemconfiguratieToegevoegdMetDeVolgendeGegevens(Table table) {
			var expectedViewModel = table.CreateInstance<WorkItemconfiguratieViewModel>();
			expectedViewModel.Id = _vorigeWorkitemconfiguratieId;

			var viewModel = await _requirementWebApplicationFactory.GetConnector().GetWorkItemconfiguratieById(_workflowconfiguratieId, _vorigeWorkitemconfiguratieId);
			expectedViewModel.VorigeWorkItemconfiguratieId = viewModel.VorigeWorkItemconfiguratieId;
			viewModel.Should().BeEquivalentTo(expectedViewModel);
		}

		[Then(@"is de opdrachtgegevensconfiguratie als volgt ingevuld:")]
		public async Task DanIsDeOpdrachtgegevensconfiguratieAlsVolgtIngevuld(Table table) {
			var expectedViewModels = table.CreateSet<OpdrachtgegevenvullingconfiguratieViewModel>();

			var viewModels = await _requirementWebApplicationFactory.GetConnector().GetOpdrachtgegevenvulingconfiguraties(_workitemconfiguratieId);

			foreach (var viewModel in viewModels) {
				if (GlobaleGegevens.TryGetValue($"{viewModel.IngevuldeWaarde}",out var globaleGegeven)) {
					viewModel.IngevuldeWaarde = globaleGegeven;
				}
			}

			viewModels.Should().BeEquivalentTo(expectedViewModels, options => options.Excluding(x => x.OpdrachtgegevendefinitieId));
		}

		[Then(@"verandert de status van de work-itemconfiguratie met de titel '(.*)' naar '(.*)'")]
		public async Task DanVerandertDeStatusVanDeWork_ItemconfiguratieMetDeTitelNaar(
			string                     titel,
			WorkItemconfiguratieStatus status) {

			var viewModel = await _requirementWebApplicationFactory.GetConnector().GetWorkItemconfiguratieById(_workflowconfiguratieId,_workitemconfiguratieId);
			viewModel.Titel.Should().Be(titel);
			viewModel.Status.Should().Be(status);
		}
	}

	public class OpdrachtgegevenvullingconfiguratieViewModel : IOpdrachtgegevenvullingconfiguratieViewModel {
		public Guid OpdrachtgegevendefinitieId { get; set; }
		public string Opdrachtgegevennaam { get; set; } = string.Empty;
		public string Opdrachtgegevenwaardetype { get; set; } = string.Empty;
		[TableAliases("Gebeurtenisgegevendefinitie")]
		public Guid? GebeurtenisgegevendefinitieId { get; set; }
		public string? IngevuldeWaarde { get; set; }

		public bool IsVerplicht { get; set; }
	}

	public enum Volgorde {
		Eerste,
		Voorgaande,
		Navolgende
	}
}