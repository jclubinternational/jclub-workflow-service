using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using JClub.WorkflowService.Business.Contracts.Events;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Data;
using JClub.WorkflowService.Messages.Commands;
using JClub.WorkflowService.Requirements.Extensions;
using JClub.WorkflowService.Requirements.Mutators;
using JClub.WorkflowService.Requirements.Utilities;
using Newtonsoft.Json;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow.Assist;

namespace JClub.WorkflowService.Requirements.Steps {

	[Binding]
	public class WorkflowSteps {
		private readonly RequirementWebApplicationFactory _requirementWebApplicationFactory;
		private readonly WorkflowconfiguratieSteps _workflowconfiguratieSteps;
		private Guid? _correlationId;
		private Task? _awaitableEvent { get; set; }
		private Task<WorkflowGestartEvent>? _workflowGestartTask { get; set; }
		private Task<WorkflowVoortgezetEvent>? _workflowVoortgezetTask { get; set; }
		private Task<WorkflowVoltooidEvent>? _workflowVoltooidTask { get; set; }

		public WorkflowSteps(
			RequirementWebApplicationFactory requirementWebApplicationFactory,
			WorkflowconfiguratieSteps        workflowconfiguratieSteps) {
			_requirementWebApplicationFactory = requirementWebApplicationFactory;
			_workflowconfiguratieSteps        = workflowconfiguratieSteps;
		}

		[When(@"er de (eerste|volgende|laatste) gebeurtenis '(.*)' optreedt met de volgende gegevens:")]
		public async Task AlsErDeGebeurtenisOptreedtMetDeVolgendeGegevens(
			GebeurtenisVolgorde volgorde,
			string              gebeurtenisNaam,
			Table               table) {

			var instance = table.Rows.ToDictionary(_ => _[0].ToPropertyName(),_ => _[1]);
			var body = JsonConvert.SerializeObject(instance);
			_correlationId ??= Guid.NewGuid();

			var testEvent = new TestEvent {
				CorrelationId = $"{_correlationId}",Name = gebeurtenisNaam,Body = body
			};

			switch (volgorde) {
				case GebeurtenisVolgorde.Eerste:
					_awaitableEvent = _requirementWebApplicationFactory.ListenToEvent<WorkflowGestartEvent>();
					await _requirementWebApplicationFactory.SendEventAndAwaitCommand<TestEvent,VerstuurOpdrachtCommand>(testEvent);
					break;
				case GebeurtenisVolgorde.Volgende:
					_awaitableEvent = _requirementWebApplicationFactory.ListenToEvent<WorkflowVoortgezetEvent>();
					await _requirementWebApplicationFactory.SendEventAndAwaitCommand<TestEvent,VerstuurOpdrachtCommand>(testEvent);
					break;
				case GebeurtenisVolgorde.Laatste:
					_awaitableEvent = _requirementWebApplicationFactory.SendEventAndAwaitEvent<TestEvent,WorkflowVoltooidEvent>(testEvent);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(volgorde),volgorde,null);
			}
		}


		[Then(@"is er de opdracht '(.*)' verstuurd met de volgende gegevens")]
		public void DanIsErDeOpdrachtVerstuurdMetDeVolgendeGegevens(
			string naam,
			Table  table) {

			var command = _requirementWebApplicationFactory.GetMessageOfType<VerstuurOpdrachtCommand>();

			command.Should().NotBeNull();
			command!.Naam.Should().Be(naam);

			var dbContext = _requirementWebApplicationFactory.Services.GetRequiredService<WorkflowServiceDataContext>();
			var opdrachtdefinities = dbContext.Opdrachtdefinities.Single(_ => _.Naam == naam)
											  .Gegevensdefinities.ToDictionary(_ => _.Gegevennaam);

			var expectedGegevens = table.CreateSet<(string gegeven,string waarde)>()
										.ToDictionary(_ => opdrachtdefinities[_.gegeven].Gegevennaam,_ => $"{(_workflowconfiguratieSteps.GlobaleGegevens.TryGetValue(_.waarde,out var value) ? value : _.waarde)}");

			var gegevens = JsonConvert.DeserializeObject<Dictionary<string,string>>(command.Gegevens);

			foreach (var expectedGegeven in expectedGegevens) {
				gegevens!.TryGetValue(expectedGegeven.Key,out var gegeven);
				gegeven.Should().NotBeNull();
				gegeven!.Should().Be(expectedGegeven.Value);
			}
		}

		[Then(@"Workflow '(.*)' heeft de volgende gegevens:")]
		public async Task DanWorkflowHeeftDeVolgendeGegevens(
			string workflownaam,
			Table  table) {

			if (_awaitableEvent == null) {
				Assert.Fail("Er wordt niet naar gebeurtenissen geluisterd.");
				return;
			}

			var id = _awaitableEvent switch {
				Task<WorkflowGestartEvent> g    => (await g).Id,
				Task<WorkflowVoortgezetEvent> g => (await g).Id,
				Task<WorkflowVoltooidEvent> g   => (await g).Id,
				_                               => Guid.Empty
			};

			var connector = _requirementWebApplicationFactory.GetConnector();
			var workflow = await connector.GetWorkflowById(id);

			var expectedWorkflow = table.CreateInstance<WorkflowViewModel>();

			expectedWorkflow.GestartOp = expectedWorkflow.GestartOp.Date + workflow.GestartOp.TimeOfDay;
			expectedWorkflow.GestoptOp = expectedWorkflow.GestoptOp?.Date + (workflow.GestoptOp ?? DateTime.UtcNow).TimeOfDay;

			workflow.Should().BeEquivalentTo(expectedWorkflow);
		}
	}

	public enum GebeurtenisVolgorde {
		Eerste,
		Volgende,
		Laatste
	}
}