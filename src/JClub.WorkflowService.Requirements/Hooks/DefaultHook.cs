using BoDi;
using JClub.WorkflowService.Requirements.Utilities;
using JClub.WorkflowService.Requirements.ValueRetrievers;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using TechTalk.SpecFlow.Assist.ValueRetrievers;

namespace JClub.WorkflowService.Requirements.Hooks {
	[Binding]
	public class DefaultHook {
		private readonly IObjectContainer _container;

		[BeforeTestRun]
		public static void BeforeTestRun() {
			Service.Instance.ValueRetrievers.Register(new NullValueRetriever("<geen>"));
			Service.Instance.ValueRetrievers.Register(new DutchBoolValueRetriever());
			//Service.Instance.ValueRetrievers.Register(new DataTypeRetriever());
			Service.Instance.ValueRetrievers.Register(new TodayValueRetriever("<vandaag>"));
		}

		public DefaultHook(IObjectContainer container) {
			_container = container;
		}

		[BeforeScenario]
		public void Bootstrap() {
			_container.RegisterInstanceAs(new RequirementWebApplicationFactory());
		}
	}
}