using System.Linq;

namespace JClub.WorkflowService.Requirements.Extensions {
	public static class StringExtensions {
		public static string ToPropertyName(this string value) {
			var words = value.Split(" ");

			return string.Join("", words.Select(WithCapitalLetter));
		}

		private static string WithCapitalLetter(string value) {
			return char.ToUpperInvariant(value[0]) + string.Join("", value[1..]);
		}
	}
}