using System;
using System.Threading.Tasks;
using NServiceBus;

namespace JClub.WorkflowService.Requirements.Handlers {
	public class CommandHandler : IHandleMessages<ICommand> {
		private readonly Action<ICommand, IMessageHandlerContext> _commandReceived;

		public CommandHandler(Action<ICommand, IMessageHandlerContext> commandReceived) {
			_commandReceived = commandReceived;
		}

		public Task Handle(ICommand message, IMessageHandlerContext context) {
			_commandReceived(message, context);
			return Task.CompletedTask;
		}
	}
}