using System;
using System.Threading.Tasks;
using NServiceBus;

namespace JClub.WorkflowService.Requirements.Handlers {
	public class EventHandler : IHandleMessages<IEvent> {
		private readonly Action<IEvent, IMessageHandlerContext> _eventReceived;

		public EventHandler(Action<IEvent, IMessageHandlerContext> eventReceived) {
			_eventReceived = eventReceived;
		}

		public Task Handle(IEvent message, IMessageHandlerContext context) {
			_eventReceived(message, context);
			return Task.CompletedTask;
		}
	}
}