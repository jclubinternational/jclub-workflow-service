using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JClub.WorkflowService.Data.EntityConfigurations {
	public class OpdrachtdefinitieConfiguration :
		IEntityTypeConfiguration<Opdrachtdefinitie> {
		public void Configure(EntityTypeBuilder<Opdrachtdefinitie> builder) {

			builder.ToTable(nameof(Opdrachtdefinitie));
			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedNever();

			builder.HasMany(x => x.Gegevensdefinities)
				.WithOne()
				.HasForeignKey(x => x.OpdrachtdefinitieId);
			builder.Navigation(x => x.Gegevensdefinities).AutoInclude();

			builder.HasOne(x => x.ResulterendeGebeurtenis)
				.WithOne()
				.HasForeignKey<Opdrachtdefinitie>($"Resulterende{nameof(Gebeurtenisdefinitie)}Id");
			builder.Navigation(x => x.ResulterendeGebeurtenis).AutoInclude();
		}
	}
}