using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JClub.WorkflowService.Data.EntityConfigurations {
	public class GebeurtenisdefinitieConfiguration :
		IEntityTypeConfiguration<Gebeurtenisdefinitie> {
		public void Configure(EntityTypeBuilder<Gebeurtenisdefinitie> builder) {

			builder.ToTable(nameof(Gebeurtenisdefinitie));
			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedNever();

			builder.HasMany(x => x.Gegevensdefinities)
				.WithOne()
				.HasForeignKey(x => x.GebeurtenisdefinitieId);
			builder.Navigation(x => x.Gegevensdefinities).AutoInclude();

		}
	}
}