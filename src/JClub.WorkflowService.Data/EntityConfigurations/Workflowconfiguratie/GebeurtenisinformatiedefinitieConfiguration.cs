using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JClub.WorkflowService.Data.EntityConfigurations {
	public class GebeurtenisinformatiedefinitieConfiguration :
		IEntityTypeConfiguration<Gebeurtenisgegevendefinitie> {

		public void Configure(EntityTypeBuilder<Gebeurtenisgegevendefinitie> builder) {
			builder.ToTable(nameof(Gebeurtenisgegevendefinitie));

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedNever();
		}
	}
}