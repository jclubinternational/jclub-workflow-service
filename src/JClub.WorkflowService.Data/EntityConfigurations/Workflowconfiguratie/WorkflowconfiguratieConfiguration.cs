using JClub.WorkflowService.Business.Domain.Aggregates;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JClub.WorkflowService.Data.EntityConfigurations {
	public class WorkflowconfiguratieConfiguration :
		IEntityTypeConfiguration<Workflowconfiguratie> {
		public void Configure(EntityTypeBuilder<Workflowconfiguratie> builder) {
			builder.ToTable(nameof(Workflowconfiguratie));

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedNever();

			builder.HasOne(_ => _.ReageerOpGebeurtenis)
				.WithMany()
				.HasForeignKey($"{nameof(Gebeurtenisdefinitie)}Id")
				.OnDelete(DeleteBehavior.NoAction);

			builder.Navigation(_ => _.ReageerOpGebeurtenis).AutoInclude();

			builder.Ignore(x => x.StartWorkItemconfiguratie);
			builder.Ignore(x => x.SortedWorkItemconfiguraties);

			builder.OwnsMany(x => x.WorkItemconfiguraties, _ => {
				_.ToTable(nameof(WorkItemconfiguratie));

				_.HasKey(x => x.Id);
				_.Property(x => x.Id).ValueGeneratedNever();

				_.HasOne(x => x.ReageerOpGebeurtenis)
					.WithMany()
					.HasForeignKey($"{nameof(Gebeurtenisdefinitie)}Id")
					.OnDelete(DeleteBehavior.NoAction);
				_.Navigation(x => x.ReageerOpGebeurtenis).AutoInclude();

				_.HasOne(x => x.VoerOpdrachtUit)
					.WithMany()
					.HasForeignKey($"{nameof(Opdrachtdefinitie)}Id")
					.OnDelete(DeleteBehavior.NoAction);
				_.Navigation(x => x.VoerOpdrachtUit).AutoInclude();

				_.WithOwner()
					.HasForeignKey($"{nameof(Workflowconfiguratie)}Id")
					.HasPrincipalKey(x => x.Id);

				_.OwnsMany(x => x.Opdrachtinformatievullingconfiguraties, __ => {
					__.WithOwner();
					__.ToTable(nameof(Opdrachtgegevenvullingconfiguratie));

					__.HasKey(x => x.Id);
					__.Property(x => x.Id).ValueGeneratedNever();

					__.Ignore(x => x.IsGevuld);

					__.HasOne(x => x.Gebeurtenisgegevendefinitie)
						.WithMany()
						.HasForeignKey($"{nameof(Gebeurtenisgegevendefinitie)}Id")
						.OnDelete(DeleteBehavior.NoAction);

					__.HasOne(x => x.Opdrachtgegevendefinitie)
						.WithMany()
						.HasForeignKey($"{nameof(Opdrachtgegevendefinitie)}Id")
						.OnDelete(DeleteBehavior.NoAction);
				});
			});
		}
	}
}