using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JClub.WorkflowService.Data.EntityConfigurations {
	public class OpdrachtgegevendefinitieConfiguration :
		IEntityTypeConfiguration<Opdrachtgegevendefinitie> {
		public void Configure(EntityTypeBuilder<Opdrachtgegevendefinitie> builder) {
			builder.ToTable(nameof(Opdrachtgegevendefinitie));

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedNever();
		}
	}
}