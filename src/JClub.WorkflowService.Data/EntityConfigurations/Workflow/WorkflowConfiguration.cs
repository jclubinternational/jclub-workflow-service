using JClub.WorkflowService.Business.Domain.Aggregates;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JClub.WorkflowService.Data.EntityConfigurations {
	public class WorkflowConfiguration :
		IEntityTypeConfiguration<Workflow> {
		public void Configure(EntityTypeBuilder<Workflow> builder) {

			builder.ToTable(nameof(Workflow));

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedNever();

			builder.HasOne(x => x.ReageerOpGebeurtenis)
				.WithMany()
				.HasForeignKey($"{nameof(Workflow.ReageerOpGebeurtenis)}Id")
				.OnDelete(DeleteBehavior.NoAction);
			builder.Navigation(x => x.ReageerOpGebeurtenis).AutoInclude();

			builder.OwnsMany<WorkItem>("_workItems", _ => {
				_.WithOwner(x => x.Workflow);

				_.ToTable(nameof(WorkItem));

				_.HasKey(x => x.Id);
				_.Property(x => x.Id).ValueGeneratedNever();

				_.HasOne(x => x.ReageerOpGebeurtenis)
					.WithMany()
					.HasForeignKey($"{nameof(Gebeurtenisdefinitie)}Id")
					.OnDelete(DeleteBehavior.NoAction);
				_.Navigation(x => x.ReageerOpGebeurtenis).AutoInclude();

				_.OwnsOne(x => x.Opdracht, __ => {
					__.WithOwner();

					__.ToTable(nameof(Opdracht));
					__.HasKey(x => x.Id);
					__.Property(x => x.Id).ValueGeneratedNever();

					__.HasOne(x => x.Definitie)
						.WithMany()
						.HasForeignKey($"{nameof(Opdrachtdefinitie)}Id")
						.OnDelete(DeleteBehavior.NoAction);

					__.Navigation(x => x.Definitie).AutoInclude();

					__.OwnsMany(x => x.Opdrachtgegevens, ___ => {
						___.WithOwner(x => x.Opdracht);

						___.ToTable(nameof(Opdrachtgegeven));
						___.HasKey(x => x.Id);
						___.Property(x => x.Id).ValueGeneratedNever();

						___.HasOne(x => x.Gebeurtenisgegevendefinitie).WithMany().OnDelete(DeleteBehavior.NoAction);
						___.Navigation(x => x.Gebeurtenisgegevendefinitie).AutoInclude();
					});
				});
			});
		}
	}
}