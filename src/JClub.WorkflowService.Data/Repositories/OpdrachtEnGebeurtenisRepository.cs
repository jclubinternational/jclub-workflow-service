using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using Microsoft.EntityFrameworkCore;

namespace JClub.WorkflowService.Data.Repositories {
	public class OpdrachtEnGebeurtenisRepository :
		BaseRepository<Opdrachtdefinitie, WorkflowServiceDataContext>,
		IGetDefinities {

		public OpdrachtEnGebeurtenisRepository(DbContextAccessor<WorkflowServiceDataContext> accessor) : base(accessor) { }

		async Task<(Opdrachtdefinitie, Gebeurtenisdefinitie)> IGetDefinities.GetByIds(Guid opdrachtdefinitieId,Guid gebeurtenisdefinitieId) {
			var opdrachtdefinitie = await Accessor.DbContext.Opdrachtdefinities.FindAsync(opdrachtdefinitieId);
			var gebeurtenisdefinitie = await Accessor.DbContext.Gebeurtenisdefinities.FindAsync(gebeurtenisdefinitieId);

			return (opdrachtdefinitie, gebeurtenisdefinitie);
		}

		public async Task<Gebeurtenisdefinitie?> GetGebeurtenisdefinitieById(Guid id) {
			var gebeurtenisdefinitie = await Accessor.DbContext.Gebeurtenisdefinities.FindAsync(id);

			return gebeurtenisdefinitie;
		}

		public async Task<IReadOnlyCollection<Gebeurtenisdefinitie>> GetAllGebeurtenisdefinities() {
			var result = await Accessor.DbContext.Gebeurtenisdefinities.ToArrayAsync();

			return result;
		}

		public async Task<Opdrachtdefinitie?> GetOpdrachtdefinitieById(Guid id) {
			var opdrachtdefinitie = await Accessor.DbContext.Opdrachtdefinities.FindAsync(id);

			return opdrachtdefinitie;
		}

		public async Task<IReadOnlyCollection<Opdrachtdefinitie>> GetAllOpdrachtdefinities() {
			var result = await Accessor.DbContext.Opdrachtdefinities.ToArrayAsync();

			return result;
		}
	}
}