using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Business.Domain.Aggregates;
using JClub.WorkflowService.Application.Contracts;
using Microsoft.EntityFrameworkCore;

namespace JClub.WorkflowService.Data.Repositories {
	public class WorkflowconfiguratieRepository : 
		BaseRepository<Workflowconfiguratie, WorkflowServiceDataContext>,
		ISaveWorkflowconfiguratie,
		IGetWorkflowconfiguraties {

		public WorkflowconfiguratieRepository(DbContextAccessor<WorkflowServiceDataContext> accessor) : base(accessor) { }
		
		public async Task SaveNew(Workflowconfiguratie workflowconfiguratie) {
			await Accessor.DbContext.AddAsync(workflowconfiguratie);
		}

		public Task<Workflowconfiguratie?> ById(Guid id) {
			return FindAsync(x => x.Id == id)!;
		}

		public async Task<Workflowconfiguratie?> ByWorkItemconfiguratieId(Guid id) {
			return (await GetAllAsync(w => w.WorkItemconfiguraties.Any(wic => wic.Id == id)))
				.SingleOrDefault();
		}


		public async Task<IReadOnlyCollection<Workflowconfiguratie>> ByGebeurtenisnaam(string naam) {
			return await Set().Where(_ => _.IsActief &&
										  _.ReageerOpGebeurtenis!.Naam == naam)
							  .ToArrayAsync();
		}
	}
}