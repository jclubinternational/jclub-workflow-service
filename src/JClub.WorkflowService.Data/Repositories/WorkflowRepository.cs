using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Domain.Aggregates;
using Microsoft.EntityFrameworkCore;

namespace JClub.WorkflowService.Data.Repositories {
	public class WorkflowRepository :
		BaseRepository<Workflow, WorkflowServiceDataContext>,
		ISaveWorkflow,
		IGetWorkflows {
		public WorkflowRepository(DbContextAccessor<WorkflowServiceDataContext> accessor) : base(accessor) { }

		public async Task SaveNew(Workflow workflow) {
			await Accessor.DbContext.AddAsync(workflow);
		}

		public Task<Workflow> ById(Guid id) {
			return GetAsync(x => x.Id == id);
		}

		public async Task<IReadOnlyCollection<Workflow>> ByGebeurtenisnaam(Guid correlationId, string naam) {
			return await Set().Where(_ => _.CorrelationId == correlationId &&
										  _.ReageerOpGebeurtenis.Naam == naam)
				.ToArrayAsync();
		}
	}
}