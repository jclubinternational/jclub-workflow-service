﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JClub.WorkflowService.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Gebeurtenisdefinitie",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Naam = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    DateModified = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gebeurtenisdefinitie", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gebeurtenisgegevendefinitie",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    GebeurtenisdefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Gegevennaam = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Gegevenwaardetype = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BronApiAdres = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Weergaveformaat = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    DateModified = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gebeurtenisgegevendefinitie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Gebeurtenisgegevendefinitie_Gebeurtenisdefinitie_GebeurtenisdefinitieId",
                        column: x => x.GebeurtenisdefinitieId,
                        principalTable: "Gebeurtenisdefinitie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Opdrachtdefinitie",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Naam = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Adres = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ResulterendeGebeurtenisdefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    DateModified = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opdrachtdefinitie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Opdrachtdefinitie_Gebeurtenisdefinitie_ResulterendeGebeurtenisdefinitieId",
                        column: x => x.ResulterendeGebeurtenisdefinitieId,
                        principalTable: "Gebeurtenisdefinitie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Workflow",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WorkflowconfiguratieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CorrelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titel = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GestartOp = table.Column<DateTime>(type: "datetime2", nullable: false),
                    GestoptOp = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    ReageerOpGebeurtenisId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HuidigeWorkItemId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    DateModified = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workflow", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Workflow_Gebeurtenisdefinitie_ReageerOpGebeurtenisId",
                        column: x => x.ReageerOpGebeurtenisId,
                        principalTable: "Gebeurtenisdefinitie",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Workflowconfiguratie",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titel = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Beschrijving = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AangemaaktDoor = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    GebeurtenisdefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    VervangenDoorId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    IsActief = table.Column<bool>(type: "bit", nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    DateModified = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workflowconfiguratie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Workflowconfiguratie_Gebeurtenisdefinitie_GebeurtenisdefinitieId",
                        column: x => x.GebeurtenisdefinitieId,
                        principalTable: "Gebeurtenisdefinitie",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Workflowconfiguratie_Workflowconfiguratie_VervangenDoorId",
                        column: x => x.VervangenDoorId,
                        principalTable: "Workflowconfiguratie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Opdrachtgegevendefinitie",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OpdrachtdefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Gegevennaam = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Gegevenwaardetype = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsVerplicht = table.Column<bool>(type: "bit", nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    DateModified = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opdrachtgegevendefinitie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Opdrachtgegevendefinitie_Opdrachtdefinitie_OpdrachtdefinitieId",
                        column: x => x.OpdrachtdefinitieId,
                        principalTable: "Opdrachtdefinitie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkItem",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WorkItemconfiguratieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VorigeWorkItemId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Titel = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OpgepaktOp = table.Column<DateTime>(type: "datetime2", nullable: true),
                    VoltooidOp = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    WorkflowId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    GebeurtenisdefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkItem_Gebeurtenisdefinitie_GebeurtenisdefinitieId",
                        column: x => x.GebeurtenisdefinitieId,
                        principalTable: "Gebeurtenisdefinitie",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WorkItem_Workflow_WorkflowId",
                        column: x => x.WorkflowId,
                        principalTable: "Workflow",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkItemconfiguratie",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VorigeWorkItemconfiguratieId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Titel = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Beschrijving = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GebeurtenisdefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OpdrachtdefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    WorkflowconfiguratieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkItemconfiguratie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkItemconfiguratie_Gebeurtenisdefinitie_GebeurtenisdefinitieId",
                        column: x => x.GebeurtenisdefinitieId,
                        principalTable: "Gebeurtenisdefinitie",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WorkItemconfiguratie_Opdrachtdefinitie_OpdrachtdefinitieId",
                        column: x => x.OpdrachtdefinitieId,
                        principalTable: "Opdrachtdefinitie",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_WorkItemconfiguratie_Workflowconfiguratie_WorkflowconfiguratieId",
                        column: x => x.WorkflowconfiguratieId,
                        principalTable: "Workflowconfiguratie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Opdracht",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OpdrachtdefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ZijnGegevensCompleet = table.Column<bool>(type: "bit", nullable: false),
                    WorkItemId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opdracht", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Opdracht_Opdrachtdefinitie_OpdrachtdefinitieId",
                        column: x => x.OpdrachtdefinitieId,
                        principalTable: "Opdrachtdefinitie",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Opdracht_WorkItem_WorkItemId",
                        column: x => x.WorkItemId,
                        principalTable: "WorkItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Opdrachtgegevenvullingconfiguratie",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OpdrachtgegevendefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    GebeurtenisgegevendefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    IngevuldeWaarde = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WorkItemconfiguratieId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opdrachtgegevenvullingconfiguratie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Opdrachtgegevenvullingconfiguratie_Gebeurtenisgegevendefinitie_GebeurtenisgegevendefinitieId",
                        column: x => x.GebeurtenisgegevendefinitieId,
                        principalTable: "Gebeurtenisgegevendefinitie",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Opdrachtgegevenvullingconfiguratie_Opdrachtgegevendefinitie_OpdrachtgegevendefinitieId",
                        column: x => x.OpdrachtgegevendefinitieId,
                        principalTable: "Opdrachtgegevendefinitie",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Opdrachtgegevenvullingconfiguratie_WorkItemconfiguratie_WorkItemconfiguratieId",
                        column: x => x.WorkItemconfiguratieId,
                        principalTable: "WorkItemconfiguratie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Opdrachtgegeven",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Naam = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsVerplicht = table.Column<bool>(type: "bit", nullable: false),
                    Waarde = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GebeurtenisgegevendefinitieId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OpdrachtId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opdrachtgegeven", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Opdrachtgegeven_Gebeurtenisgegevendefinitie_GebeurtenisgegevendefinitieId",
                        column: x => x.GebeurtenisgegevendefinitieId,
                        principalTable: "Gebeurtenisgegevendefinitie",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Opdrachtgegeven_Opdracht_OpdrachtId",
                        column: x => x.OpdrachtId,
                        principalTable: "Opdracht",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Gebeurtenisgegevendefinitie_GebeurtenisdefinitieId",
                table: "Gebeurtenisgegevendefinitie",
                column: "GebeurtenisdefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_Opdracht_OpdrachtdefinitieId",
                table: "Opdracht",
                column: "OpdrachtdefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_Opdracht_WorkItemId",
                table: "Opdracht",
                column: "WorkItemId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Opdrachtdefinitie_ResulterendeGebeurtenisdefinitieId",
                table: "Opdrachtdefinitie",
                column: "ResulterendeGebeurtenisdefinitieId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Opdrachtgegeven_GebeurtenisgegevendefinitieId",
                table: "Opdrachtgegeven",
                column: "GebeurtenisgegevendefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_Opdrachtgegeven_OpdrachtId",
                table: "Opdrachtgegeven",
                column: "OpdrachtId");

            migrationBuilder.CreateIndex(
                name: "IX_Opdrachtgegevendefinitie_OpdrachtdefinitieId",
                table: "Opdrachtgegevendefinitie",
                column: "OpdrachtdefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_Opdrachtgegevenvullingconfiguratie_GebeurtenisgegevendefinitieId",
                table: "Opdrachtgegevenvullingconfiguratie",
                column: "GebeurtenisgegevendefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_Opdrachtgegevenvullingconfiguratie_OpdrachtgegevendefinitieId",
                table: "Opdrachtgegevenvullingconfiguratie",
                column: "OpdrachtgegevendefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_Opdrachtgegevenvullingconfiguratie_WorkItemconfiguratieId",
                table: "Opdrachtgegevenvullingconfiguratie",
                column: "WorkItemconfiguratieId");

            migrationBuilder.CreateIndex(
                name: "IX_Workflow_ReageerOpGebeurtenisId",
                table: "Workflow",
                column: "ReageerOpGebeurtenisId");

            migrationBuilder.CreateIndex(
                name: "IX_Workflowconfiguratie_GebeurtenisdefinitieId",
                table: "Workflowconfiguratie",
                column: "GebeurtenisdefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_Workflowconfiguratie_VervangenDoorId",
                table: "Workflowconfiguratie",
                column: "VervangenDoorId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkItem_GebeurtenisdefinitieId",
                table: "WorkItem",
                column: "GebeurtenisdefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkItem_WorkflowId",
                table: "WorkItem",
                column: "WorkflowId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkItemconfiguratie_GebeurtenisdefinitieId",
                table: "WorkItemconfiguratie",
                column: "GebeurtenisdefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkItemconfiguratie_OpdrachtdefinitieId",
                table: "WorkItemconfiguratie",
                column: "OpdrachtdefinitieId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkItemconfiguratie_WorkflowconfiguratieId",
                table: "WorkItemconfiguratie",
                column: "WorkflowconfiguratieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Opdrachtgegeven");

            migrationBuilder.DropTable(
                name: "Opdrachtgegevenvullingconfiguratie");

            migrationBuilder.DropTable(
                name: "Opdracht");

            migrationBuilder.DropTable(
                name: "Gebeurtenisgegevendefinitie");

            migrationBuilder.DropTable(
                name: "Opdrachtgegevendefinitie");

            migrationBuilder.DropTable(
                name: "WorkItemconfiguratie");

            migrationBuilder.DropTable(
                name: "WorkItem");

            migrationBuilder.DropTable(
                name: "Opdrachtdefinitie");

            migrationBuilder.DropTable(
                name: "Workflowconfiguratie");

            migrationBuilder.DropTable(
                name: "Workflow");

            migrationBuilder.DropTable(
                name: "Gebeurtenisdefinitie");
        }
    }
}
