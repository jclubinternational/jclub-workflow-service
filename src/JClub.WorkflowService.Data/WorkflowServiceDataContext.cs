using Microsoft.EntityFrameworkCore;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Business.Domain.Aggregates;
using JClub.WorkflowService.Business.Domain.Aggregates.WorkflowconfiguratieEntities;
using JClub.WorkflowService.Data.EntityConfigurations;

namespace JClub.WorkflowService.Data {
	public class WorkflowServiceDataContext : 
		BaseDbContext<WorkflowServiceDataContext> {
		public WorkflowServiceDataContext(DbContextOptions<WorkflowServiceDataContext> options) : base(options) { }

		public DbSet<Workflowconfiguratie> Workflowconfiguraties { get; set; } = null!;
		public DbSet<Opdrachtdefinitie> Opdrachtdefinities { get; set; } = null!;
		public DbSet<Gebeurtenisdefinitie> Gebeurtenisdefinities { get; set; } = null!;
		public DbSet<Workflow> Workflows { get; set; } = null!;
		
		protected override void OnModelCreating(ModelBuilder modelBuilder) {
			
			modelBuilder.ApplyConfiguration(new WorkflowconfiguratieConfiguration());

			modelBuilder.ApplyConfiguration(new OpdrachtdefinitieConfiguration());
			modelBuilder.ApplyConfiguration(new OpdrachtgegevendefinitieConfiguration());
			modelBuilder.ApplyConfiguration(new GebeurtenisdefinitieConfiguration());
			modelBuilder.ApplyConfiguration(new GebeurtenisinformatiedefinitieConfiguration());

			modelBuilder.ApplyConfiguration(new WorkflowConfiguration());

			base.OnModelCreating(modelBuilder);
		}
	}
}