using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using JClub.Shared.Initialization;
using JClub.Shared.MessageBus;
using Microsoft.Extensions.Hosting;
using Serilog;
using StructureMap;

namespace JClub.WorkflowService.Api {
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public static class Program {
		public static async Task Main(string[] args) {
			var webHost = CreateHostBuilder(args).Build();
			var provider = webHost.Services;

			// Perform initialization stuffz
			await provider.GetRequiredService<IApplicationInitializationTaskManager>().InitializeApplication().ConfigureAwait(false);
			
			// Start de applicatie
			await webHost.RunAsync().ConfigureAwait(false);
			var accessor = provider.GetRequiredService<IEndpointAccessor>();
			foreach (var endpoint in accessor.Endpoints) {
				await endpoint.Stop().ConfigureAwait(false);
			}
			await webHost.StopAsync().ConfigureAwait(false);
			
			var lifetime = webHost.Services.GetService<IHostLifetime>() as IDisposable;
			lifetime?.Dispose();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.UseServiceProviderFactory(ctx => new StructureMapServiceProviderFactory(new StructureMapRegistry(ctx.Configuration)))
				.UseSerilog((context, services, configuration) => configuration
					.ReadFrom.Configuration(context.Configuration)
					.ReadFrom.Services(services))
				.ConfigureWebHostDefaults(webBuilder => {
					webBuilder.UseStartup<Startup>();
				});

	}
}
