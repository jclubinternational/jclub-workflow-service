using System;
using JClub.Shared.MessageBus.SignalR;
using StructureMap;
using Microsoft.Extensions.Configuration;
using JClub.Shared.Initialization.NServiceBus;
using JClub.Shared.MessageBus;

namespace JClub.WorkflowService.Api {
	public class StructureMapRegistry : Registry {
		public StructureMapRegistry(IConfiguration configuration) {
			if (configuration == null) throw new ArgumentNullException(nameof(configuration));

			Scan(scanner => {
				scanner.AssembliesFromApplicationBaseDirectory(assembly => !assembly.IsDynamic);
				
				scanner.Convention<SignalrBoundCommandConvention>();
				scanner.WithDefaultConventions();
			});

			this.AddNServiceBusEndpointApplicationInitializationTask(new StartNServiceBusOptions(configuration), configuration["ServiceBus:EndpointName"], "");

		}
	}
}
