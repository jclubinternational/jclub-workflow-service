using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace JClub.WorkflowService.Api.Controllers {
	public class OpdrachtgegevenvullingconfiguratieController : ControllerBase {

		[HttpGet("workitemconfiguratie/{workItemconfiguratieId}/[controller]")]
		public async Task<ActionResult<IEnumerable<IOpdrachtgegevenvullingconfiguratieViewModel>>> GetById(
			[FromServices] IGetWorkflowconfiguraties repository,
			[FromRoute]    Guid                      workItemconfiguratieId) {

			var workflowconfiguratie = await repository.ByWorkItemconfiguratieId(workItemconfiguratieId);

			if (workflowconfiguratie == null) {
				return NotFound(workItemconfiguratieId);
			}

			if (!workflowconfiguratie.ZoekWorkItemconfiguratie(workItemconfiguratieId,out var workitemconfiguratie)) {
				return NotFound(workitemconfiguratie);
			}

			return Ok(workitemconfiguratie.OpdrachtinformatievullingconfiguratieViewModels);
		}
	}
}