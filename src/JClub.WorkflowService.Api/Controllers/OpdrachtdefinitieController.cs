using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace JClub.WorkflowService.Api.Controllers {
	public class OpdrachtdefinitieController : ControllerBase {

		[HttpGet("[controller]")]
		public async Task<IEnumerable<IOpdrachtdefinitieViewModel>> GetAll(
			[FromServices] IGetDefinities getGebeurtenisdefinities) {

			var opdrachtdefinities = await getGebeurtenisdefinities.GetAllOpdrachtdefinities();

			return opdrachtdefinities.Select(_ => _.AlsViewModel());
		}

		[HttpGet("[controller]/{id}")]
		public async Task<ActionResult<IOpdrachtdefinitieViewModel>> GetById(
			[FromServices] IGetDefinities getGebeurtenisdefinities,
			[FromRoute]    Guid                                 id) {

			var opdrachtdefinitie = await getGebeurtenisdefinities.GetOpdrachtdefinitieById(id);

			if (opdrachtdefinitie == null) {
				return NotFound(id);
			}

			return Ok(opdrachtdefinitie.AlsViewModel());
		}

		[HttpGet("[controller]/{id}/gegeven")]
		public async Task<ActionResult<IEnumerable<IOpdrachtgegevendefinitieViewModel>>> GetGegevensById(
			[FromServices] IGetDefinities getGebeurtenisdefinities,
			[FromRoute]    Guid                                 id) {

			var opdrachtdefinitie = await getGebeurtenisdefinities.GetOpdrachtdefinitieById(id);

			if (opdrachtdefinitie == null) {
				return NotFound(id);
			}

			return Ok(opdrachtdefinitie.GegevensdefinitieViewModels);
		}
	}
}