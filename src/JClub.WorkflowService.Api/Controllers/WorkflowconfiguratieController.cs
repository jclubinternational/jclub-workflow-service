using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Data.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace JClub.WorkflowService.Api.Controllers {
	public class WorkflowconfiguratieController : ControllerBase {
		
		[HttpGet("[controller]")]
		public async Task<IEnumerable<IWorkflowconfiguratieViewModel>> GetAll(
			[FromServices]WorkflowconfiguratieRepository repository) {

			return (await repository.GetAllAsync(w => true))
				.Select(_ => _.ToViewModel());
		}

		[HttpGet("[controller]/{id}")]
		public async Task<IWorkflowconfiguratieViewModel> GetById(
			[FromServices] WorkflowconfiguratieRepository repository,
			[FromRoute] Guid id) {

			var workflowconfiguratie = await repository.GetAsync(w => w.Id == id);
			return workflowconfiguratie.ToViewModel();
		}
	}
}