using System;
using System.Threading.Tasks;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Data.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace JClub.WorkflowService.Api.Controllers {
	public class WorkflowController : ControllerBase {
		
		[HttpGet("[controller]/{id}")]
		public async Task<IWorkflowViewModel> Get(
			[FromRoute]Guid id,
			[FromServices]WorkflowRepository repository) {

			var workflow = await repository.GetAsync(_ => _.Id == id);
			return workflow.ToViewModel();
		}
	}
}