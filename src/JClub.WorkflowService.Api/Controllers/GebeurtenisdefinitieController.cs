using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace JClub.WorkflowService.Api.Controllers {
	public class GebeurtenisdefinitieController : ControllerBase {

		[HttpGet("[controller]")]
		public async Task<IEnumerable<IGebeurtenisdefinitieViewModel>> GetAll(
			[FromServices] IGetDefinities getGebeurtenisdefinities) {

			var gebeurtenisdefinities = await getGebeurtenisdefinities.GetAllGebeurtenisdefinities();

			return gebeurtenisdefinities.Select(_ => _.AlsViewModel());
		}

		[HttpGet("[controller]/{id}")]
		public async Task<ActionResult<IGebeurtenisdefinitieViewModel>> GetById(
			[FromServices] IGetDefinities getGebeurtenisdefinities,
			[FromRoute]    Guid                                 id) {

			var gebeurtenisdefinitie = await getGebeurtenisdefinities.GetGebeurtenisdefinitieById(id);

			if (gebeurtenisdefinitie == null) {
				return NotFound(id);
			}

			return Ok(gebeurtenisdefinitie.AlsViewModel());
		}

		[HttpGet("[controller]/{id}/gegeven")]
		public async Task<ActionResult<IEnumerable<IGebeurtenisgegevendefinitieViewModel>>> GetGegevensById(
			[FromServices] IGetDefinities getGebeurtenisdefinities,
			[FromRoute]    Guid                                 id) {

			var gebeurtenisdefinitie = await getGebeurtenisdefinities.GetGebeurtenisdefinitieById(id);

			if (gebeurtenisdefinitie == null) {
				return NotFound(id);
			}

			return Ok(gebeurtenisdefinitie.GegevensdefinitieViewModels);
		}
	}
}