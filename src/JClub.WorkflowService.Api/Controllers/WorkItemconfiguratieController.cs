using System;
using System.Linq;
using System.Threading.Tasks;
using JClub.Shared.MessageBus;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Business.Contracts.ViewModels;
using JClub.WorkflowService.Data.Repositories;
using JClub.WorkflowService.Messages.Commands;
using Microsoft.AspNetCore.Mvc;
using NServiceBus;

namespace JClub.WorkflowService.Api.Controllers {
	public class WorkItemconfiguratieController : ControllerBase {

		[HttpGet("workflowconfiguratie/{workflowconfiguratieId}/[controller]")]
		public async Task<ActionResult<IWorkItemconfiguratieViewModel>> GetAllByWorkflowconfiguratieId(
			[FromServices] IGetWorkflowconfiguraties getWorkflowconfiguraties,
			[FromRoute]    Guid                      workflowconfiguratieId) {

			var workflowconfiguratie = await getWorkflowconfiguraties.ById(workflowconfiguratieId);

			if (workflowconfiguratie == null) {
				return NotFound(workflowconfiguratieId);
			}

			return Ok(workflowconfiguratie.SortedWorkItemconfiguraties.Select(_ => _.ToViewModel()));
		}

		[HttpGet("workflowconfiguratie/{workflowconfiguratieId}/[controller]/{id}")]
		public async Task<ActionResult<IWorkItemconfiguratieViewModel>> GetById(
			[FromServices] IGetWorkflowconfiguraties getWorkflowconfiguraties,
			[FromRoute]    Guid                      workflowconfiguratieId,
			[FromRoute]    Guid                      id) {

			var workflowconfiguratie = await getWorkflowconfiguraties.ById(workflowconfiguratieId);

			if (workflowconfiguratie == null) {
				return NotFound(workflowconfiguratieId);
			}

			if (!workflowconfiguratie.ZoekWorkItemconfiguratie(id,out var workitemconfiguratie)) {
				return NotFound(workflowconfiguratieId);
			}

			return Ok(workitemconfiguratie.ToViewModel());
		}

		[HttpGet("[controller]/{id}/gebeurtenisdefinitiegegeven")]
		public async Task<ActionResult<IGebeurtenisgegevendefinitieViewModel>> BepaalGebeurtenisgegevendefinities(
			[FromServices] IGetWorkflowconfiguraties getWorkflowconfiguraties,
			[FromRoute]    Guid                      id) {

			var workflowconfiguratie = await getWorkflowconfiguraties.ByWorkItemconfiguratieId(id);

			if (workflowconfiguratie == null) {
				return NotFound(id);
			}

			return Ok(workflowconfiguratie.BepaalGebeurtenisgegevendefinities(id).Select(_ => _.AlsViewModel()));
		}

		[HttpPost("/[controller]/registreerNieuweWorkItemconfiguratiecommand")]
		public Task VoerOpdrachtUit(
			[FromBody]     RegistreerNieuweWorkItemconfiguratieCommand command,
			[FromServices] IMessageBus                                 messageSession) {

			return messageSession.SendCommandLocal(new RegistreerNieuweWorkItemconfiguratieUiCommand() {
				Data = command
			});
		}
	}
}