using System.Linq;
using System.Threading.Tasks;
using Elastic.Apm.AspNetCore;
using JClub.Shared.MessageBus;
using JClub.Shared.Initialization;
using JClub.Shared.Initialization.EntityFrameworkCore;
using JClub.Shared.Security;
using JClub.WorkflowService.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.HttpOverrides;
using JClub.Shared.MessageBus.EntityFrameworkCore;
using JClub.WorkflowService.Application.Contracts;
using JClub.WorkflowService.Application.Utils;
using JClub.WorkflowService.Data.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Serilog;

namespace JClub.WorkflowService.Api {
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
	public class Startup {
		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services) {

			// Configureer basisservices
			services.AddControllersWithViews();
			services.AddMessageBus();
			services.AddDbContextAccessor<WorkflowServiceDataContext>(Configuration.GetConnectionString("Database"));

			//services.AddDbContextAccessor<WorkflowServiceDataContext>(Configuration.GetConnectionString("Database"));
			services.AddHealthChecks().AddDbContextCheck<WorkflowServiceDataContext>();
			services.AddTransient<IUnitOfWork>(s => s.GetRequiredService<DbContextAccessor<WorkflowServiceDataContext>>());

			// Configureer CORS
			var corsOrigins = Configuration.GetSection("CorsOrigins").Get<string[]>();
			services.AddCors(options => options
				.AddDefaultPolicy(p => p
					.AllowAnyHeader()
					.AllowAnyMethod()
					.AllowCredentials()
					.WithOrigins(corsOrigins)
				));

			services.AddSwaggerGen();

			// Configureer Security stuff
			services.AddJClubSecurityComplete(Configuration["Jwt:SigningSecret"], cfg => cfg.SigningSecret = Configuration["Jwt:ClientSigningSecret"]);

			services.AddTransient<ISaveWorkflowconfiguratie, WorkflowconfiguratieRepository>();
			services.AddTransient<IGetDefinities, OpdrachtEnGebeurtenisRepository>();
			services.AddTransient<WorkflowconfiguratieRepository>();
			services.AddTransient<WorkflowRepository>();
			services.AddTransient<ISaveWorkflow, WorkflowRepository>();
			services.AddTransient<IGetWorkflowconfiguraties, WorkflowconfiguratieRepository>();
			services.AddTransient<IGetWorkflows, WorkflowRepository>();
			services.AddSingleton<ISendOpdrachtGenerator>(new SendOpdrachtGenerator());

			//Configureer applicatie startup taken
			services.AddApplicationInitializationTasks(config => config
				.DbContextMigrateApplicationInitialization<WorkflowServiceDataContext>());
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app) {

			app.UseSwagger();

			app.UseSwaggerUI(c => {
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Workflow API V1");
				//c.RoutePrefix = string.Empty;
			});

			var opts = new ForwardedHeadersOptions {
				ForwardLimit = 3,
				ForwardedHeaders = ForwardedHeaders.All,
			};
			app.UseXForwardedHeaders(opts);

			app.UseSerilogRequestLogging();

			if (!string.IsNullOrWhiteSpace(Configuration["ElasticApm:ServerUrls"]))
				app.UseElasticApm(Configuration);

			app.UseRouting();
			app.UseAuthentication();
			app.UseAuthorization();
			app.UseCors();

			app.UseEndpoints(endpoints => {
				endpoints.MapHealthChecks("/health", new HealthCheckOptions {
					ResponseWriter = WriteHealthCheckResponse
				});
				endpoints.MapControllers();
			});
		}
		
		private static Task WriteHealthCheckResponse(HttpContext context, HealthReport result) {
			context.Response.ContentType = "application/json";
			var json = new JObject(
				new JProperty("status", result.Status.ToString()),
				new JProperty("total_duration", result.TotalDuration),
				new JProperty("results", new JObject(result.Entries.Select(pair =>
					new JProperty(pair.Key, new JObject(
						new JProperty("status", pair.Value.Status.ToString()),
						new JProperty("description", pair.Value.Description),
						new JProperty("duration", pair.Value.Duration),
						new JProperty("exception", pair.Value.Exception?.Message),
						new JProperty("tags", pair.Value.Tags),
						new JProperty("data", JToken.FromObject(pair.Value.Data))))))));

			return context.Response.WriteAsync(json.ToString(Formatting.Indented));
		}
	}
}
